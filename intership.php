<!doctype html>
<html lang=en-gb class=no-js>
<head>
<title>Work with OPTRON | Join Team OPTRON</title>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge" />
<meta name=keywords content />
<meta name=description content="Work, Learn, Earn and Grow with OPTRON - We invite creative minds to work with optron and grow together." />
<?php include "includes/common-css.php" ?>
   
</head>
<body>
<div class=site_wrapper>
<?php include "includes/menu-home.php" ?>
<div class=clearfix></div>

<div class="feature_section904">
            <div class="container">
                <h1 class="less10">Intership</h1>
                <div class="clearfix margin_bottom4"></div>
               <a href="/inquiry.html?urm_source=home-button" class="button eight">Apply Now</a> </div>
        </div>

<div class=clearfix></div>

<div class="feature_section89">
            <div class="container">
                <div class="content">
                    <h2>Intership</h2>
                    <p class="bigtfont">Jumpstart your career with Optron, grow your skills with learning by doing.</p><br>
                    <p class="bigtfont">Learning new things by doing is best way to improve knowledge and enhance skills. By working at Optron, you will be given an opportunity to learn new things, improve your knowledge and skills while working on real assignments.</p><br>
                    <p class="bigtfont">You will be working with team of experienced people, you will be mentored by leaders. You will learn many new things at Optron.</p><br>
                    <p class="bigtfont">We offer L1 and L2 internship to passionate students who are ready to learn new things and grow their career. </p><br>

                    <p class="bigtfont">For more details about our internship programs, share your contact details on <a href="mailto:careers@optron.in">careers@optron.in</a> </p><br>

                    <p class="bigtfont">You may fill this form to apply at OPTRON</p>

                </div>
              
            </div>
        </div>


<div class=clearfix></div>


<?php include "includes/footer.php" ?>
<a href=# class=scrollup>Scroll</a>
</div>
<?php include "includes/common-js.php" ?>


</body>
</html>