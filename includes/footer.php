<footer class="footer">

<div class="container">

    <div class="one_fourth">
         <div class="qlinks">
     <h4 class="white">Digital Marketing</h4>
        <ul class="foolist">
        <li><a href="/seo.html">SEO Service</a></li>
        <li><a href="/social-media-marketing.html">Social Media Marketing</a></li>
        <li><a href="/lead-generation.html">Lead Generation</a></li>
        <li><a href="/google-adwords.html">Google AdWords</a></li>
        <li><a href="/search-marketing.html">Search Marketing</a></li>
        <li><a href="/seo-plans.html">Seo Plans</a></li>
          
        </ul>
    </div>
	</div><!-- end links -->
    
    <div class="one_fourth">
    <div class="qlinks">
    
    	<h4 class="white">Web Solutions</h4>
        <ul class="foolist">
        <li><a href="/website/index.html">Website Development</a></li>
        <li><a href="/website/e-commerce-website.html">E-Commerce Website</a></li>
        <li><a href="/website/wordpress-website.html">Wordpress Website</a></li>
        <li><a href="/website/startup-website.html">Startup Development</a></li>
         <li><a href="/website/dynamic-website.html">Dynamic Website</a></li>
        <li><a href="/website/business-website.html">Business Website</a></li>
            </ul>
        
    </div>
	</div><!-- end address -->
    
        
    <div class="one_fourth">
    
    	<div class="qlinks">
    
    	<h4 class="white">Company</h4>
        <ul class="foolist">
                <li><a href="/about.html">About Us</a></li>
                <li><a href="/career.html">Jobs &amp; Career</a></li>
                <li><a href="/newsandupdates.html">News / Updates</a></li>
                 <li><a href="/blogs.html">Blogs</a></li>
                <li><a href="/portfolio.html">Success Stories</a></li>
                <li><a href="/partner.html">Become a Partner</a></li>
                </ul>
        
    </div>
        
    </div><!-- end site info -->
    
    <div class="one_fourth last">
        
    <div class="siteinfo">
     <h4>Address</h4>
        
        <ul class="faddress">
            <li><i class="fa fa-map-marker fa-lg"></i>&nbsp; 217,  Accord Classic, <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Goregaon East, Mumbai, 400063</li>
            <li><i class="fa fa-phone"></i>&nbsp; +91 8652 360 360</li>
           
            <li>Landmark: Above Anupam Store</li>
        </ul>
	</div>
        
        <div class="clearfix margin_top2"></div>
        
    </div><!-- end about -->

 </div><!-- end footer -->

<div class="clearfix"></div>

<div class="copyright_info">
<div class="container">

	<div class="clearfix"></div>
    
    <div class="one_half">
    
        Copyright © 2018 Optron Digital. All rights reserved. <a href="/terms.html" rel="nofollow">Terms of Use</a> | <a href="/privacy-policy.html" rel="nofollow">Privacy Policy</a>
        
    </div>
    
    <div class="one_half last">
        
        <ul class="footer_social_links">
            
            <li class="animate" data-anim-type="zoomIn"><a href="http://facebook.com/optronindia" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="http://www.twitter.com/optronindia" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
            
        </ul>
            
    </div>
    
</div>
</div><!-- end copyright info -->


</footer>