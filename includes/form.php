<div id="new-message-popup" class="form-popup new-message mfp-hide">
    <!-- FORM POPUP CONTENT -->
    <div class="form-popup-content">
      <h4 class="popup-title">Get Response in less than 1 minute</h4>
      <!-- LINE SEPARATOR -->
      <hr class="line-separator">
      <!-- /LINE SEPARATOR -->
      <form action="/form-enquiry.php" class="new-message-form" method="post">
        <!-- INPUT CONTAINER -->

        <input type="hidden" value="<?php echo $productName ?>" id="productName" name="productName">
      
        <div class="input-container">
          <label for="subject" class="rl-label b-label required">Contact Name:</label>
          <input type="text" id="contactName" name="contactName" placeholder="Enter Contact Name here..." required>
        </div>

        <div class="input-container">
          <label for="subject" class="rl-label b-label required">Contact E-mail:</label>
          <input type="text" id="contactEmail" name="contactEmail" placeholder="Enter Email ID here..." required>
        </div>


        <div class="input-container">
          <label for="subject" class="rl-label b-label required">Mobile Number (Whatsapp):</label>
          <input type="text" id="contactMobile" name="contactMobile" placeholder="Enter Contact Number here..." required>
        </div>

        <div class="input-container">
          <label for="message" class="rl-label b-label required">Your Message:</label>
          <textarea id="Message" name="Message" placeholder="Write your message here...">Yes, I am Interested in </textarea>
        </div>
        <!-- INPUT CONTAINER -->
        <span><a id="showError" class="showerror"></a></span>
      <!--   <button type="button" id="submit"" name="submit" class="button mid dark">Send Enquiry</button> -->

      <input type="submit" class="button mid dark" name="submit">
         
      </form>
    </div>
    <!-- /FORM POPUP CONTENT -->
  </div>