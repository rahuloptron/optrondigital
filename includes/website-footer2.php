<footer>

<div class="footer">

<div class="clearfix"></div>

<div class="secarea">
<div class="container">
    
    <div class="one_fourth">
        <h4 class="white">Hosting Packages</h4>
        <ul class="foolist">
            <li><a href="#">Web Hosting</a></li>
            <li><a href="#">Reseller Hosting</a></li>
            <li><a href="#">VPS Hosting</a></li>
            <li><a href="#">Dedicated Servers</a></li>
            <li><a href="#">Windows Hosting</a></li>
            <li><a href="#">Cloud Hosting</a></li>
            <li><a href="#">Linux Servers</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Our Products</h4>
        <ul class="foolist">
            <li><a href="#">Website Builder</a></li>
            <li><a href="#">Web Design</a></li>
            <li><a href="#">Logo Design</a></li>
            <li><a href="#">Register Domains</a></li>
            <li><a href="#">Traffic Booster</a></li>
            <li><a href="#">Search Advertising</a></li>
            <li><a href="#">Email Marketing</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Company</h4>
        <ul class="foolist">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Press &amp; Media</a></li>
            <li><a href="#">News / Blogs</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Awards &amp; Reviews</a></li>
            <li><a href="#">Testimonials</a></li>
            <li><a href="#">Affiliate Program</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth last aliright">
        <div class="address">
        
           <h3 class="white">Optron Technologies</h3>
            217, Accord Classic, Station Road, Goregaon East 400063
            <div class="clearfix margin_bottom1"></div>
            <strong>Phone:</strong> <b>9833189090</b>
            <br />
            <strong>Mail:</strong> <a href="mailto:info@arkahost.com">info@optron.in</a>
            <br />
           <br />
           
            
        </div>
        
    </div><!-- end section -->
    
    
    <div class="clearfix margin_bottom3"></div>
    
    
    <div class="one_fourth">
        <h4 class="white">Follow Us</h4>
        <ul class="foosocial">
            <li class="faceboox"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
            <li class="linkdin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Resources</h4>
        <ul class="foolist">
            <li><a href="#">How to Create a Website</a></li>
            <li><a href="#">How to Transfer a Website</a></li>
            <li><a href="#">Start a Web Hosting Business</a></li>
            <li><a href="#">How to Start a Blog</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Support</h4>
        <ul class="foolist">
            <li><a href="#">Product Support</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Knowledge Base</a></li>
            <li><a href="#">Tutorials</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth last aliright">
        <p class="clearfix margin_bottom1"></p>
        <img src="http://placehold.it/120x50" alt="" /> &nbsp; <img src="http://placehold.it/120x50" alt="" />
    </div><!-- end section -->
    
</div>
</div>
    

<div class="clearfix"></div>


<div class="copyrights">
<div class="container">

    <div class="one_half">Copyright © 2018 Optron Digital. All rights reserved.</div>
    <div class="one_half last aliright"><a href="/about/terms.html">Terms of Service</a>|<a href="/about/privacy.html">Privacy Policy</a>|<a href="/about/sitemap.html">Site Map</a></div>

</div>
</div><!-- end copyrights -->


</div>

</footer>