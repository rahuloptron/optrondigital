<footer>

<div class="footer">

<div class="clearfix"></div>

<div class="secarea">
<div class="container">
    
    <div class="one_fourth">
        <h4 class="white">Digital Marketing</h4>
        <ul class="foolist">
        <li><a href="/seo.html">SEO Service</a></li>
        <li><a href="/social-media-marketing.html">Social Media Marketing</a></li>
        <li><a href="/lead-generation.html">Lead Generation</a></li>
        <li><a href="/google-adwords.html">Google AdWords</a></li>
        <li><a href="/search-marketing.html">Search Marketing</a></li>
        <li><a href="/seo-plans.html">Seo Plans</a></li>
          
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Web Solutions</h4>
        <ul class="foolist">
        <li><a href="/website/index.html">Website Development</a></li>
        <li><a href="/website/e-commerce-website.html">E-Commerce Website</a></li>
        <li><a href="/website/wordpress-website.html">Wordpress Website</a></li>
        <li><a href="/website/startup-website.html">Startup Development</a></li>
         <li><a href="/website/dynamic-website.html">Dynamic Website</a></li>
        <li><a href="/website/business-website.html">Business Website</a></li>
            </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Company</h4>
        <ul class="foolist">
                <li><a href="/about.html">About Us</a></li>
                <li><a href="/career.html">Jobs &amp; Career</a></li>
                <li><a href="/newsandupdates.html">News / Updates</a></li>
                 <li><a href="/blogs.html">Blogs</a></li>
                <li><a href="/portfolio.html">Success Stories</a></li>
                <li><a href="/partner.html">Become a Partner</a></li>
                </ul>
    </div><!-- end section -->
    
    <div class="one_fourth last aliright">
        <div class="address">
        
           <h3 class="white">Optron Technologies</h3>
            217, Accord Classic, Station Road, Goregaon East 400063
            <div class="clearfix margin_bottom1"></div>
            <strong>Phone:</strong> <b>9833189090</b>
            <br />
            <strong>Mail:</strong> <a href="mailto:info@arkahost.com">info@optron.in</a>
            <br />
           <br />
           
            
        </div>
        
    </div><!-- end section -->
    
    
</div>
</div>
    

<div class="clearfix"></div>


<div class="copyrights">
<div class="container">

    <div class="one_half">Copyright © 2018 Optron Digital. All rights reserved.</div>
    <div class="one_half last aliright"><a href="/terms.html">Terms of Service</a>|<a href="/privacy-policy.html">Privacy Policy</a></div>

</div>
</div><!-- end copyrights -->


</div>

</footer>