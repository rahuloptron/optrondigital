<script type="text/javascript" src="js/universal/jquery.js"></script>
<script src="js/animations/js/animations.min.js" type="text/javascript"></script>
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 

<script src="../js/module-popup/jquery.magnific-popup.min.js"></script>
<script src="../js/module-popup/dashboard-header.js"></script>
<script src="../js/module-popup/dashboard-inbox.js"></script>
<script src="../js/module-popup/inbox-messages.js"></script>
<script src="../js/module-popup/side-menu.js"></script>
<script src="../js/module-popup/jquery.xmpiechart.min.js"></script>
<script src="/form.js"></script>

<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>
<script src="js/masterslider/jquery.easing.min.js"></script>

<script src="js/scrolltotop/totop.js" type="text/javascript"></script>

<script src="js/carouselowl/owl.carousel.js"></script> 
<script type="text/javascript" src="js/universal/custom.js"></script>
<script type="text/javascript" src="js/carouselowl/custom.js"></script> 

<script type="text/javascript" src="js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="js/accordion/custom.js"></script>

<script src="js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>