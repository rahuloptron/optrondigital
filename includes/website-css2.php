<link rel="stylesheet" href="../../css/reset1.css" type="text/css" />
    <link rel="stylesheet" href="../../css/style1.css" type="text/css" />
    
    <!-- font awesome icons -->
    <link rel="stylesheet" href="../../css/font-awesome/css/font-awesome.min.css">


    <link rel="stylesheet" type="text/css" href="../../css/magnific-popup.css">

    
    <!-- simple line icons -->
    <link rel="stylesheet" type="text/css" href="../../css/simpleline-icons/simple-line-icons.css" media="screen" />
        
    <!-- animations -->
    <link href="../../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
    <link rel="stylesheet" media="screen" href="../../css/responsive-leyouts1.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="../../css/shortcodes1.css" type="text/css" /> 

    <!-- mega menu -->
    <link href="../../js/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="../../js/mainmenu/demo.css" rel="stylesheet">
    <link href="../../js/mainmenu/menu3.css" rel="stylesheet">
    
    <!-- MasterSlider -->
    <link rel="stylesheet" href="../../js/masterslider/style/masterslider.css" />
    <link rel="stylesheet" href="../../js/masterslider/skins/default/style.css" />
    
    <!-- owl carousel -->
    <link href="../../js/carouselowl/owl.transitions.css" rel="stylesheet">
    <link href="../../js/carouselowl/owl.carousel.css" rel="stylesheet">
    
    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="../../js/accordion/style.css" />
    
    <!-- tabs 2 -->
    <link href="../../js/tabs2/tabacc.css" rel="stylesheet" />
    <link href="../../js/tabs2/detached.css" rel="stylesheet" />
    
    <!-- loop slider -->
    <link type="text/css" rel="stylesheet" href="../../js/loopslider/style.css">