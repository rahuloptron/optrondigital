<header class="header">
 
	<div class="container_full_menu">
    
    <!-- Logo -->
<div class="logo"><a href="/index.html" id="logo"></a></div>
	<!-- Navigation Menu -->
    <div class="menu_main">
    
      <div class="navbar yamm navbar-default">
        
          <div class="navbar-header">
            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1" > <span></span>
              <button type="button" > <i class="fa fa-bars"></i></button>
            </div>
          </div>
          
          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
          
            <nav>
            
              <ul class="nav navbar-nav">
              
                <li> <a href="/index.html">Home</a></li>

                 <li> <a href="/about.html">About</a></li>
                
                <li> <a href="/marketing/index.html">Digital Marketing </a> </li>   

                <li class="dropdown"> <a href="/website/index.html" class="dropdown-toggle">Web Designing</a>
                    <ul class="dropdown-menu multilevel" role="menu">
                       
                        <li><a href="/website/e-commerce-website.html">Ecommerce Website</a></li>
                        <li><a href="/website/startup-website.html">Startup Website</a></li>
                        <li><a href="/website/business-website.html">Business Website</a></li>
                        <li><a href="/website/dynamic-website.html">Dynamic Website</a></li>
                      
                  </ul>
                </li>          

                <li> <a href="/portfolio.html">Portfolio</a> </li>

                <li> <a href="/clients.html">Clients </a> </li>

                <li><a href="/contact.html">Contact</a> </li>
                
                
              </ul>
              
            </nav>
            
          </div>
        
      </div>
    </div>
        
	</div>
    
</header>