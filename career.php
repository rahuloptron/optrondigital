<!doctype html>
<html lang=en-gb class=no-js>
<head>
<title>Work with OPTRON | Join Team OPTRON</title>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge" />
<meta name=keywords content />
<meta name=description content="Work, Learn, Earn and Grow with OPTRON - We invite creative minds to work with optron and grow together." />
<?php include "includes/common-css.php" ?>
   
</head>
<body>
<div class=site_wrapper>
<?php include "includes/menu-home.php" ?>
<div class=clearfix></div>

<div class="feature_section904">
            <div class="container">
                <h1 class="less10">BUILD THE FUTURE</h1>
                <div class="clearfix margin_bottom2"></div>
                <h3 class="white">To showcase what OPTRON has to offer when candidate work here. How to apply for job, culture and other things. </h3>
                </div>
        </div>

<div class=clearfix></div>

<div class="feature_section89">
            <div class="container">
                <div class="one_half">
                    <p class="bigtfont">At Optron, our mission is to empower growth. </p><br>
                    <p class="bigtfont">We are more than a technology company. We are team of innovative, creative and passionate people who are working together towards common goals. </p><br>
                    <p class="bigtfont">It’s Empowering Growth and becoming a growth partner. </p><br>
                    <p class="bigtfont">We are one of the fastest growing technology and education company, we are constantly looking for passionate people to join our team. </p><br>
                </div>
                <div class="one_half last">
                    <img src="../images/career-3.jpg">
                </div>
            </div>
        </div>


<div class=clearfix></div>

<div class=feature_section69>
<div class=container>

<h2>Why Work at Optron</h2>
<div class="clearfix margin_bottom5"></div>

<div class=one_third_less>
<div class="box">
<h5>Engaging Culture</h5><div class=bgline></div>
<p>We have build culture that encourages you to engage with your team members and make new friends. We are not company, we are family.</p>
</div>
</div>
<div class=one_third_less>
<div class="box">
<h5>Team Meetings</h5><div class=bgline></div>
<p>Regular team meetings to understand and review tasks, responsibilities and progress on assignments.</p>
</div>
</div>

<div class="one_third_less last">
<div class="box">
<h5>Open Feedback</h5><div class=bgline></div>
<p>We have open feedback culture. Share your ideas, comments and feedback about what you like, what you don’t like.</p>
</div>
</div>
<div class="clearfix margin_bottom5"></div>
<div class=one_third_less>
<div class="box">
<h5>Regular Training & Classes</h5><div class=bgline></div>
<p>Regular training sessions, workshops and skills enhancement classes to upgrade knowledge, improve skills and develop healthy competition.</p>
</div>
</div>
<div class=one_third_less>
<div class="box"> 
<h5>Freedom to work, believe in results</h5><div class=bgline></div>
<p>We don’t have boss. We encourage flexible working style with responsibilities and results.</p>
</div>
</div>
<div class="one_third_less last">
<div class="box"> 
<h5>New Opportunities</h5><div class=bgline></div>
<p>Optron offers you to grow your career by giving you more challenging assignments and opportunities. Ultimately it’s about growing together.</p>
</div>
</div>
</div>
</div>

<div class=clearfix></div>

<div class="feature_section80">
<div class="container">
<div class="arrow_box">
<h1>Our Culture</h1>
<p class="bigtfont less10">Over a decade ago, Optron was founded with a culture that encourages people to do what they love, learn, earn and grow together.At optron we strongly believe in transparency, trust, respect and knowledge.</p>
</div>
</div>
</div>

<div class=clearfix></div>

<div class="feature_section81">
<div class="container">
<h2>Current Openings at Optron</h2>
<div class="clearfix margin_bottom2"></div>
<div class="one_third_less">
<img src="/images/salesmarketing.jpg" class="rimg" alt="">
<div class="clearfix margin_bottom2"></div>
<h3>SALES & MARKETING</h3>
<div class="clearfix margin_bottom2"></div>
<a href="#" class="button eleven">Read More</a>
</div>
<div class="one_third_less">
<img src="/images/creative.jpg" class="rimg" alt="">
<div class="clearfix margin_bottom2"></div>
<h3>CREATIVE</h3>
<div class="clearfix margin_bottom2"></div>
<a href="#" class="button eleven">Read More</a>
</div>
<div class="one_third_less last">
<img src="/images/customer-support.jpg" class="rimg" alt="">
<div class="clearfix margin_bottom2"></div>
<h3>CUSTOMER SUPPORT</h3>
<div class="clearfix margin_bottom2"></div>
<a href="#" class="button eleven">Read More</a>
</div>

<div class="clearfix margin_bottom4"></div>
<div class="one_third_less">
<img src="/images/smm.jpg" class="rimg" alt="">
<div class="clearfix margin_bottom2"></div>
<h3>OPERATIONS</h3>
<div class="clearfix margin_bottom2"></div>
<a href="#" class="button eleven">Read More</a>
</div>
<div class="one_third_less">
<img src="/images/internship-and-student.jpg" class="rimg" alt="">
<div class="clearfix margin_bottom2"></div>
<h3>INTERNSHIP</h3>
<div class="clearfix margin_bottom2"></div>
<a href="intership.html" class="button eleven">Read More</a>
</div>
<div class="one_third_less last">
<img src="/images/research-and-development-process.jpg" class="rimg" alt="">
<div class="clearfix margin_bottom2"></div>
<h3>DEVELOPMENT</h3>
<div class="clearfix margin_bottom2"></div>
<a href="#" class="button eleven">Read More</a>
</div>


</div>
</div>

<div class=clearfix></div>
<?php include "includes/footer.php" ?>
<a href=# class=scrollup>Scroll</a>
</div>
<?php include "includes/common-js.php" ?>


</body>
</html>