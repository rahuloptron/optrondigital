
$(document).ready(function(){

var showError = $("#showError");

$('#submitenquiry').click(function(){

var contactName = $('#contactName').val();
var contactMobile = $('#contactMobile').val();
var contactEmail = $('#contactEmail').val();
var cMessage = $('#cMessage').val();
var productName = $('#productName').val();

var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var add = /^[a-zA-Z0-9-\/] ?([a-zA-Z0-9-\/]|[a-zA-Z0-9-\/] )*[a-zA-Z0-9-\/]$/;

	if(contactName == ''){

		
		showError.text("Please Enter Your Name");

	}else if(!(re.test(contactEmail))){

		showError.text("Please Enter Valid Email Id");


	}else if(contactMobile.length < 10 || contactMobile.length > 10 || contactMobile == ''){

		showError.text("Please Enter Your Mobile Number");

	}else if(cMessage == ''){

		showError.text("Please Enter Your Message");

	}else{

		$("span").remove();

		$.post("/enquiry-back.php",
				{
					 contactName : contactName,
					 contactMobile: contactMobile,
					 contactEmail:contactEmail,
					 cMessage: cMessage,
					 productName : productName,

					 	}, function(data){

					 	$('#submitenquiry').attr("disabled", "disabled");
						$('#submitenquiry').css("color","white");
						$('#submitenquiry').text("Please wait...");

						if(data == 1){

							setTimeout(function(){

								$('#submitenquiry').attr("disabled", "disabled");
								$('#submitenquiry').css("background","Green");
								$('#submitenquiry').css("color","Yellow");
								$('#submitenquiry').text("Enquiry Submitted! Thank You");
								/*$("#showSuccess").show();*/

							}, 5000);



							setTimeout(function(){

								window.location = "/inquiry.php" ;

							}, 8000);


						}else if(data == 0) { 

					
							setTimeout(function(){

							$('#submitenquiry').attr("disabled", "disabled");
							$('#submitenquiry').css("background","Black");
							$('#submitenquiry').css("color","Red");
							$('#submitenquiry').text("Enquiry Not Submitted Please Try Again");


						}, 2000);

							setTimeout(function(){

								location.reload();

							}, 6000);

						}else{

							setTimeout(function(){

							$('#submitenquiry').attr("disabled", "disabled");
							$('#submitenquiry').css("background","Black");
							$('#submitenquiry').css("color","Red");
							$('#submitenquiry').text("Error! Please Try Again");

							}, 2000);

							setTimeout(function(){

								location.reload();

							}, 6000);
						}

					}
			);

	}

});

});
