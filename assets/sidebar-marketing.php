<div class="right_sidebar">

	<div class="sidebar_widget">
    	<div class="sidebar_title"><h4>Our Services</h4></div>
		<ul class="arrows_list1">		
            <li><a href="/digital-marketing"><i class="fa fa-angle-right"></i> Digital Marketing</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Google AdWords</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Social Media Marketing</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Search Engine Optimization</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Google AdWords & PPC</a></li>
             <li><a href="#"><i class="fa fa-angle-right"></i> How to Start a Blog</a></li>
            
		</ul>
	</div><!-- end section -->
    
    <div class="clearfix margin_top4"></div>
    
    <div class="sidebar_widget">
      <div class="project_details"> 
            <h5>Course Details</h5>
            <span><strong>Total Duration</strong> <em>3 months (3 days per week)</em></span>
            <span><strong>Timing</strong> <em>Morning | Afternoon </em></span> 
            <span><strong>Total Sessions</strong> <em>36 sessions (72 hrs) of 2 hrs. Each 3 days per week</em></span>
            <span><strong>Venue</strong> <em>Goregaon Center 
217, Accord Classic, Station Road, Above Anupam Stationery Store, 
Goregaon East, Mumbai 400063</em></span>
            <div class="clearfix margin_top5"></div>
            <a href="#" class="but_medium1"><i class="fa fa-hand-o-right fa-lg"></i>&nbsp; Download Brochure</a>
            <div class="clearfix margin_top5"></div>
        </div>
    
    	<div class="sidebar_title"><h4>Recent Posts</h4></div>
        
			<ul class="recent_posts_list">
                
                <li>
                <span><a href="#"><img src="images/side1.jpg" alt="" /></a></span>
                <a href="#">How to Register a Domain Name on Siteground ? </a>
                
                </li>
                
                <li>
               <span><a href="#"><img src="images/side1.jpg" alt="" /></a></span>
                <a href="#">How to get 500+ customers using SEO & AdWords ?</a>
               
                </li>
                        
                <li class="last">
                <span><a href="#"><img src="images/side1.jpg" alt="" /></a></span>
                <a href="#">How to setup MailChimp Email Marketing (Free Account) ? </a>
                </li>

            </ul>
                
	</div><!-- end section -->
    
    
   
    
    <div class="clearfix margin_top4"></div>
    
	<div class="sidebar_widget"> 
    	
        <div class="sidebar_title"><h4>Text Widget</h4></div>
        <p>Going to use a passage of lorem lpsum you need to be sure there anything embarrassin hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend the repeat predefined chunks as thenecessary making this the first true generator.</p>      
           
	</div><!-- end section -->
	
</div>