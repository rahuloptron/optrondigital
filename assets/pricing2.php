<div class="feature_section62">
<div class="container">
	
	<h2>Pricing 2</h2>
    
    <div class="one_third">
    <div class="title">
      <h4>STANDARD</h4><h2>$4 <b>/month</b></h2></div>
    <ul>
      <li><i class="fa  fa-check"></i>Etiam quis purus orci posuere</li>
      <li><i class="fa  fa-check"></i>Ut tincidunt sapien ut faucibus</li>
      <li><i class="fa  fa-check"></i>Donec imperdiet ornare pretium</li>
      <li><i class="fa  fa-check"></i>Sed et sem se ipsum vitae metus</li>
      <li><i class="fa  fa-check"></i>Cras necleo vel tristique uorci</li>
    </ul>
    <div class="clearfix margin_bottom2"></div><a href="#" class="button_2">Sign Up Now</a>
    </div>
    
    <div class="one_third highlight ">
    <div class="title"><h4>BUSINESS</h4><h2>$18 <b>/month</b></h2></div>
    <ul>
      <li><i class="fa  fa-check"></i>Etiam quis purus orci posuere</li>
      <li><i class="fa  fa-check"></i>Ut tincidunt sapien ut faucibus</li>
      <li><i class="fa  fa-check"></i>Donec imperdiet ornare pretium</li>
      <li><i class="fa  fa-check"></i>Sed et sem se ipsum vitae metus</li>
      <li><i class="fa  fa-check"></i>Cras necleo vel tristique uorci</li>
    </ul>
    <div class="clearfix margin_bottom2"></div><a href="#" class="button_2">Sign Up Now</a>
    </div>
    
    <div class="one_third last">
    <div class="title"><h4>UNLIMITED</h4><h2>$99<b>/month</b></h2></div>
    <ul>
      <li><i class="fa  fa-check"></i>Etiam quis purus orci posuere</li>
      <li><i class="fa  fa-check"></i>Ut tincidunt sapien ut faucibus</li>
      <li><i class="fa  fa-check"></i>Donec imperdiet ornare pretium</li>
      <li><i class="fa  fa-check"></i>Sed et sem se ipsum vitae metus</li>
      <li><i class="fa  fa-check"></i>Cras necleo vel tristique uorci</li>
    </ul>
    <div class="clearfix margin_bottom2"></div><a href="#" class="button_2">Sign Up Now</a>
    </div>
    </div>
</div>