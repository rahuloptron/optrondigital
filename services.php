<!doctype html>
<html lang="en-gb" class="no-js">
<head>
<title>Full service Digital Marketing company in Goregaon</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="Full service digital agency with 10+ years of experience" />

<?php include "includes/common-css.php" ?>

</head>

<body>
<div class="site_wrapper">

<?php include "includes/menu-home.php" ?>

<div class="clearfix"></div>


<div class="feature_section10">
<div class="container">

    <h1 class="less6">Innovative Digital Marketing Services<em>We are a full-service digital marketing agency having more than 10 years of industry experience and 100+ clients, 200+ projects. With our detail-oriented and ROI focused digital marketing services we help businesses to improve their online visibility and increase sales</em></h1>
    
     <!-- <a href="contact.html">Get Started Now</a> -->
    
</div>
</div>


<div class="clearfix"></div>






<div class="feature_section64">
<div class="container">

    <h2>Our Expertise</h2>
    <b>Full-service Digital Marketing and web development company
</b>
    
    <div class="one_fourth">
    <img src="images/180x180.png" alt="" />
    <h4>Digital Marketing <b>Use Digital Marketing to grow your business faster </b></h4>
    </div>
    
    <div class="one_fourth">
    <img src="images/training.png" alt="" />
    <h4>Training<b>Search engine optimizatoin and Google Adwords Campaigns </b></h4>
    </div>
    
    <div class="one_fourth">
    <img src="images/web-design180.png" alt="" />
    <h4>Website Development <b>Mobile friendly and conversion ready website development</b></h4>
    </div>
    
    <div class="one_fourth last">
    <img src="images/kr180.png" alt="" />
    <h4>Marketing Strategy <b>We build result oriented marketing strategy </b></h4>
    </div>

</div>
</div>






<div class="feature_section662">
<div class="container">
<h1>What we do?</h1>
    <div class="one_third">
     <a href="website-designing/index"><div class="box1">
    <h4>Web Designing <b>Get static website, dynamic website, e-commerce website for business growth</b></h4>
   
    </div>
</a>
    </div>
    
    <div class="one_third">
    <a href="marketing/social-media-marketing"><div class="box2">
    
    <h4>Social Media Marketing<b>Use Social Media Marketing to get more inquiries and increase brand awareness</b></h4>
    </div>
</a>
    </div>
    
    <div class="one_third last">
     <a href="marketing/search-engine-optimization"><div class="box3">
  
         <h4>Search Engine Optimization<b>Get your website on the first page of Google with our result driven SEO services</b></h4>
    </div>
</a>
    </div>
<div class="clearfix margin_bottom3"></div>

<div class="one_third">
     <a href="marketing/lead-generation"><div class="box4">
   
    <h4>
        Lead Generation<b>Get more leads for your business by using optron lead generation services</b></h4>
    </div>
</a>
    </div>
    
    <div class="one_third">
     <a href="marketing/google-adwords"><div class="box5">
    <!--<div class="img"><img src="images/icon-32.png" alt=""/></div>-->
        <h4>Google Adwords <b>Promote your business on first page of Google using Google AdWords marketing</b></h4>

    </div>
</a>
    </div>
    
    <div class="one_third last">
      <a href="marketing/email-marketing"><div class="box6">
    <!--<div class="img"><img src="images/icon-33.png" alt=""/></div>-->
        <h4>Email Marketing <b>Integrated E-mail solutions to reach your target audience via email marketing</b></h4>
    </div>
</a>
    </div>

<div class="clearfix margin_bottom3"></div>


<div class="one_third">
    <div class="box7">
   
        <h4>E-Commerce website <b>Complete e-commerce solutions featuring order hsitory, shopping carts, secure transactions, etc</b></h4>
    </div>
    </div>
    
    <div class="one_third">
    <div class="box8">
    <!--<div class="img"><img src="images/icon-32.png" alt=""/></div>-->
    <h4>Wordpress & Blogging <b>Wordpress is the most popular way to bulid website and create blog within limited time.</b></h4>
    </div>
    </div>
    
    <div class="one_third last">
    <div class="box9">
    <!--<div class="img"><img src="images/icon-33.png" alt=""/></div>-->
    <h4>Content writing <b>Unique and Professional Content Writing Services and get your website on 1st page of google</b></h4>
    </div>
    </div>


</div>
</div>


<div class="clearfix margin_bottom3"></div>


<div class="feature_section3">

    <div class="section_title1_1">
        <h2 class="caps white">More than 10 years of experience</h2>
        <div class="linebg"></div>
        <p class="fontdefaulf less6">Since 2007, We have helped many small to medium companies increase sales & reduce expense. We have helped more than 500 individuals learn new skills to achieve goals in their life.</p>
    </div>
</div>


<div class="clearfix"></div>


<div class="feature_section78">
<div class="container">

    <div class="one_half">
     <h3>Why choose OPTRON - Your digital partner</h3>
    <br>
        <ul class="list_divlines">		
            <li> <i class="fa fa-check "></i> Complete service under one roof</li>  
                <li> <i class="fa fa-check "></i> Team of experienced & certified professionals</li>
                <li> <i class="fa fa-check"></i> We use latest tools and techniques</li>  
                <li> <i class="fa fa-check"></i> Data Driven & Result oriented approach</li>
             <li> <i class="fa fa-check"></i> We are focused on quality, value and results</li>
             <li> <i class="fa fa-check"></i> Proven Track Record of Success in Digital Marketing</li>
		</ul>
        
    </div>


	<div class="one_half last">
    	
    



      <script type="text/javascript" src="https://static.mailerlite.com/data/webforms/604568/r9z0o5.js?v9"></script>
  
    </div>

</div>
</div>


<div class="clearfix"></div>



<?php include "includes/test.php" ?>


<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
  
    
    <h1 style="text-align: center;"> Some of Our Clients</h1>
    
    

    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
  <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
              <img src="images/clients1.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Enjay</a></strong></h5>
                <p>Empowering enterprise with ennovation</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients2.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_third last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients3.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Aashray</a></strong></h5>
                <p>Leading IT service provider company</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients4.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Rohm Computers</a></strong></h5>
                <p>Rohm computers offers comprehensive range of IT Products</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients5.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Shakti Enterprise</a></strong></h5>
                <p>Reliable &amp; Affordable Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_third last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients6.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Revital Trichology</a></strong></h5>
                <p>Best Trichology Clinic in Mumbai for Hair fall treatment</p>

            </div>
            
        </div>
    
    </div>
    
    
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients7.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Agdigitas</a></strong></h5>
                <p>RELIABLE, AFFORDABLE AND SCALABLE IT Infrastructure</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients8.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Dental Avenue</a></strong></h5>
                <p>Multi-specialty Dental clinic in Mumbai</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_third last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients9.jpg" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Grecells</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div>


      <div class="margin_top3"></div><div class="clearfix"></div>

    

 </div>
</div>
<div class="clearfix"></div>



<?php include "includes/partner.php" ?>



<div class="clearfix"></div>


<div class="feature_section629">
    <div class="container">
      <h3 class="white caps">BOOST YOUR BUSINESS WITH OUR DIGITAL MARKETING SERVICES </h3>
      <a href="inquiry" class="button one">Contact Us</a> </div>
  </div>

<div class="clearfix"></div>


<div class="feature_section_contact">
<div class="container">

    <div class="box1"> <i class="fa fa-mobile-phone"></i>
    <h4 class="caps">Phone Number<b>+91 9833189090</b></h4></div>
    
    <div class="box2"> <i class="fa fa-envelope-o"></i>
    <h4 class="caps">Email Address<b>bhavesh@optron.in</b></h4></div>
    
    <div class="box3"> <i class="fa fa-map-marker"></i>
    <h4 class="caps">Location Address<b>217, Accord Classics, Station Road,
     Goregaon East, Mumbai, 400063</b></h4></div>
    
   <!--  <div class="box4">
      <div class="logo"><a href="/index.html" id="logo"></a></div>
   </div> -->

</div>    
</div>

<div class="copyright_info2">
<div class="container">

  <div class="clearfix"></div>
    
    <div class="one_half">
    
        Copyright © 2017 OPTRON All rights reserved.  <a href="/terms.html" rel="nofollow">Terms of Use</a> | <a href="/privacy-policy.html" rel="nofollow">Privacy Policy</a>
        
    </div>
    
    <div class="one_half last">
        
       <!--  <ul class="footer_social_links">
           <li class="animate" data-anim-type="zoomIn"><a href="http://facebook.com/optronindia" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
           <li class="animate" data-anim-type="zoomIn"><a href="http://www.twitter.com/optronindia" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
           
       </ul>
            -->
    </div>
    
</div>
</div>




<a href="#" class="scrollup">Scroll</a>


    
</div>


<?php include "includes/common-js.php" ?>

    <?php include "includes/ga.php" ?>






</body>



</html>
