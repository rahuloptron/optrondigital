<?php

$productName = "Social Media Marketing";

?>

<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]--><head>
	<title>#1 For Social Media Marketing in Mumbai</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

    <!-- Favicon --> 
    <link rel="shortcut icon" href="images/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
	
<?php include 'includes/website-css.php' ?>

<?php include 'includes/ga.php' ?>
    
</head>

<body>

<?php include "includes/form.php" ?>

<div class="site_wrapper">

<?php include "includes/menu-home.php" ?>

<div class="clearfix"></div>

<div class="page_title1 sty7">

    <div class="container">

    <h1>Welcome to Optron Digital – Social Media Marketing</h1>
    <h2>Generate More Traffic, Leads & Sales</h2>
    
      <br>
     <a href="#new-message-popup" class="but_large4 transp2 open-new-message">Get Started</a>

     </div>
    
</div>

<div class="clearfix margin_bottom4"></div>

<div class="feature_section12-1">
<div class="container">
    
    <div class="one_half">

    
    <p class="bigtfont dark">Optron Digital offers best SEO Services in Mumbai and helps you to increase business with online presence and increase ROI. With smart online strategy we implement our SEO Search Engine Optimization is a natural way (Organic ranking) to compete for a ranking on first page of Search Engines.  </p>

    <div class="clearfix margin_bottom2"></div>
     <p class="bigtfont dark">With the fast-paced growth and development of the virtual platform, every business is opting for an online enterprises are finding it highly competitive to get digital presence. If your brand is not competing online then we are sure that you are missing out golden opportunities which will bring you more business and maximize your revenues.</p>
      <div class="clearfix margin_bottom2"></div>
     <p class="bigtfont dark">We are providing an affordable SEO Services in Mumbai since 5+ years. Our targeted approach for online presence includes best techniques of Off Page & ON Page Optimization on your website to make sure your brand lands up on first page of Search Engines.</p>
    	
	</div><!-- end section -->
    
    <div class="one_half last">

        <img src="images/seo-services.jpg" alt="">
    
        
	</div><!-- end section -->
    
</div>
</div><!-- end featured section 12 -->

<div class="clearfix"></div>

<div class="feature_section401">
<div class="container">

    <div class="one_third">
    
        <span aria-hidden="true" class="icon-trophy"></span>
        <h4 class="white caps">MONITOR</h4>
        <p>We constantly monitor the performance of website to develop improved strategies.</p>
    
    </div><!-- end section -->
    
    <div class="one_third">
    
        <span aria-hidden="true" class="icon-present"></span>
        <h4 class="white caps">MANAGE</h4>
        <p>We undertake complete management of your online strategy covering the online presence.</p>
    
    </div><!-- end section -->
    
    <div class="one_third last">
    
        <span aria-hidden="true" class="icon-settings"></span>
        <h4 class="white caps">MEASURE</h4>
        <p>We measure and report against a number of metrics to analyse your effectiveness online.</p>
    
    </div><!-- end section -->

</div>
</div>

<div class="clearfix"></div>

<div class="feature_section17-1">
<div class="container">

      <h1 class="caps"><strong>Our Commitment</strong>

         <div class="clearfix margin_bottom7"></div>

    <div class="one_third">
        <img src="images/service-search.png" alt="">
        <h3 class="caps"><strong>Proven Methodology</strong></h3>
        <p class="light">Our process driven approach allows us to complete every project on time.</p>
       
    </div><!-- end section -->
    
    <div class="one_third">
        <img src="images/service-layers.png" alt="">
        <h3 class="caps"><strong>Affordable Pricing</strong></h3>
        <p class="light">We are not cheap, we are reasonable and delivering value for money.</p>
    </div><!-- end section -->
    
    <div class="one_third last">
        <img src="images/service-devices.png" alt="">
        <h3 class="caps"><strong>Skilled Developers</strong></h3>
        <p class="light">Team of skilled and developers with multiple years of experience.</p>
        
    </div><!-- end section -->
    
    
</div>
</div>

<div class="clearfix"></div>

<div class="parallax_section2">
<div class="container">

    <h4 class="caps">More than 2,000 websites hosted</h4>

    <h1 class="caps"><strong>get your website online today</strong></h1>

    <div class="clearfix margin_bottom2"></div>
    
    <div class="counters12">
    
        <div class="one_fifth_less"> <h5 class="light">Customers</h5> <h1 id="target31"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Projects</h5> <h1 id="target32"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Developers</h5> <h1 id="target33"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Awards</h5> <h1 id="target34"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less last"> <h5 class="light">Years</h5> <h1 id="target35"></h1> </div><!-- end section -->
     
    </div>

</div>
</div><!-- end parallax section 2 -->

<div class="clearfix"></div>

<div class="feature_section17">
<div class="container">

     <h1 class="caps"><strong>SEO Services</strong>

         <div class="clearfix margin_bottom7"></div>

    <div class="one_third">
        <i class="fa fa-wordpress"></i>
        <h3 class="caps"><strong>Social Media Marketing</strong></h3>
        
    </div><!-- end section -->
    
    <div class="one_third">
        <i class="fa fa-download"></i>
        <h3 class="caps"><strong>Pay Per <br>Click</strong></h3>
       
    </div><!-- end section -->
    
    <div class="one_third last">
        <i class="fa fa-database"></i>
        <h3 class="caps"><strong>Search Engine Oprimization</strong></h3>
        
    </div><!-- end section -->
    
    
    <div class="clearfix margin_bottom5"></div>
    
    
    <div class="one_third">
        <i class="fa fa-wrench"></i>
        <h3 class="caps"><strong>Social Media Marketing </strong></h3>
       
    </div><!-- end section -->
    
    <div class="one_third">
        <i class="fa fa-connectdevelop"></i>
        <h3 class="caps"><strong>SEO <br>Outsourcing</strong></h3>
        
    </div><!-- end section -->
    
    <div class="one_third last">
        <i class="fa fa-support"></i>
        <h3 class="caps"><strong>Wordpress<br> Support</strong></h3>
        
    </div><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>

<div class="feature_section1">
<div class="container">

    <h1 class="caps"><strong>Why Choose Optrondigital</strong></h1>

    <div class="clearfix margin_bottom3"></div>
    
    <div class="one_third">
       
        <h4>EXPERIENCE</h4>
        <p>Our 15 years of experience has made us competent enough to take your business to the next level online.</p>
        
        <div class="clearfix margin_bottom5"></div>
        
       
        <h4>INNOVATION</h4>
        <p>We believe in providing our clients with new innovative ideas that help promote the brand online.</p>
      
        
    </div><!-- end section -->
    
    
    <div class="one_third">

        <h4>CONVENIENCE</h4>
        <p>Our work approach is designed in a structured format that helps us deliver services in a convenient manner.</p>
       
        <div class="clearfix margin_bottom5"></div>
        
        <h4>CORE TEAM</h4>
        <p>We have a dedicated team for each service that helps in successfully delivering all business requirements</p>
       
        
    </div><!-- end section -->
    
    
    <div class="one_third last">
       
        <h4>GLOBAL OUTREACH</h4>
        <p>We have worked with diverse clientele across the globe, thus making us well-poised to cater international clients</p>
        
        <div class="clearfix margin_bottom5"></div>
        
       
        <h4>PROVEN TRACK RECORD</h4>
        <p>We have achieved significant digital marketing milestones that have helped businesses reach its online goals.</p>
       
        
    </div><!-- end section -->

</div>
</div>


<div class="clearfix"></div>

<div class="feature_section3">

<div class="container alicent">
    
    <h2 class="caps"><strong>How We Work</strong></h2>
    
    <div class="clearfix margin_bottom2"></div>
    
   
    <ul class="pop-wrapper">
    
        <li><img src="images/icon-16.png" alt=""> <h6>Understand</h6> </li>
        
        <li> <img src="images/icon-16.png" alt=""> <h6>Analysis</h6> </li>
        
        <li> <img src="images/icon-16.png" alt=""> <h6>Competitors</h6> </li>
        
        <li> <img src="images/icon-16.png" alt=""> <h6>Strategy</h6> </li>
        
        <li> <img src="images/icon-16.png" alt=""> <h6>Result</h6> </li>
        
        <li> <img src="images/icon-16.png" alt=""> <h6>Support</h6> </li>
        
    </ul>
    
</div>

</div>

<div class="clearfix"></div>


<div class="feature_section6">
<div class="container">
    
    <h1 class="caps"><strong>why customers <i class="fa fa-heart sitecolor"></i> us!</strong></h1>
    
    <div class="clearfix margin_bottom1"></div>
    
    <div class="less6">
    <div id="owl-demo20" class="owl-carousel">
    
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Michile Johnson</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Desirae Karla</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Franklin Brice</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
                
    </div>
    </div>

</div>
</div>

<div class="clearfix"></div>


<div class="content_fullwidth">
<div class="container">
    
     <h1 class="caps"><strong>Some Esteemed Clients</strong>
    </h1>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/adam-fabriwerk.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                 <h5><strong><a href="#">Adam Fabriwerk</a></strong></h5>
            <p>Manufactures of processing systems for pharma</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
       
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/ajfilms.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Aj Films</a></strong></h5>
            <p>Best Photography in Mumbai</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/agdigitas.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ag Digitas</a></strong></h5>
                <p>IT Solutions Company</p>
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/blacpeper.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Black Peper</a></strong></h5>
                <p>Black Pepper is an Exhibition Stall Design company</p>
            </div>
            
        </div>
    
    </div>
    
    
     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/enjay.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Enjay Solution</a></strong></h5>
                <p>Expert in CRM and Telephony Solutions across verticals</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/genesis.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Genesis Telesecure</a></strong></h5>
                <p>Genesis Telesecure LLP offers complete range of solutions</p>
            </div>
            
        </div>
    
    </div>
                      
    <div class="one_fourth">

    <div class="flips4">
    
        <div class="flips4_front flipscont4">
       <img src="images/clients/grecelllogo.png" alt="" >
        </div>
        
        <div class="flips4_back flipscont4">
            <h5><strong><a href="#">Grecells</a></strong></h5>
            <p>Disaster recovery solutions for Windows server</p>
        </div>
        
    </div>

    </div>      
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/heminfotech.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hem Infotech</a></strong></h5>
                <p>IT Solutions and Services Company</p>

            </div>
            
        </div>
    
    </div>
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/hi-will-logo.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hi-Will Education</a></strong></h5>
                <p>Engineers classes</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/neotech.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/prarambh.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Prarambh Security</a></strong></h5>
                <p>Security Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/pratrikshainterior.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Pratiksha Interior</a></strong></h5>
                <p>Modular kitchen, Office Furniture and Roofing</p>

            </div>
            
        </div>
    
    </div>

 </div>
</div>

<div class="clearfix"></div>

<div class="page_title1 sty7">

    <div class="container">

    <h1>#1 For SEO Services in Mumbai</h1>
    <h3>Optron digital is a leading Search Engine Optimization (SEO) Company. Our foray into the Internet marketing world has been an astounding success, and we are rapidly growing. Web SEO Services have passion and enthusiasm for our work, and we are unabashed about the fact that we are technology freak.</h3>

     <br><a href="#" class="but_medium1"><strong>&nbsp; Get Started</strong></a>

     </div>
    
</div>

<div class="clearfix"></div>

<div class="host_plans">
<div class="container">

    <h1 class="caps"><strong>Seo Packages</strong>
    </h1>
    
    <div class="clearfix margin_bottom3"></div>
    
    <div class="one_third_less">
    <div class="planbox">
        
        <div class="title"><h4 class="caps"><strong>Silver</strong></h4></div>
        
        <div class="prices">
            <strong>2.99<i>/M</i></strong> <b>Regularly <em>4.99</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
           
             <li><strong>5</strong> Keywords</li>
            <li>Site Analysis</li>
             <li>Rank report</li>
             <li>Keyword research</li>
              <li>Competition Analysis</li>
             <li>back link analysis</li>
             <li>Google Penalty Check</li>
             <li>Backlink Analysis</li>
            
        </ul>
        
    </div>
    </div><!-- end plan -->
    
    
    
    <div class="one_third_less">
    <div class="planbox">
        
        <div class="title"><h4 class="caps"><strong>Gold
</strong></h4></div>
        
        <div class="prices">
            <strong>45.50<i>/M</i></strong> <b>Regularly <em>90.50</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
          
            <li><strong>10</strong> Keywords</li>
             <li>Site Analysis</li>
            <li>Rank report</li>
            <li>Keyword research</li>
              <li>Competition Analysis</li>
             <li>back link analysis</li>
             <li>Google Penalty Check</li>
             <li>Backlink Analysis</li>
            
        </ul>
        
    </div>
    </div><!-- end plan -->
    
    <div class="one_third_less last">
    <div class="planbox">
        
        <div class="title"><h4 class="caps"><strong>Platinum
</strong></h4></div>
        
        <div class="prices">
            <strong>100<i>/M</i></strong> <b>Regularly <em>200</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
          
            <li><strong>20</strong> Keywords</li>
            <li>Site Analysis</li>
             <li>Rank report</li>
              <li>Keyword research</li>
              <li>Competition Analysis</li>
             <li>back link analysis</li>
             <li>Google Penalty Check</li>
             <li>Backlink Analysis</li>
           
        </ul>
        
    </div>
    </div><!-- end plan -->
    
</div>
</div><!-- end hosting plans -->

<div class="clearfix"></div>




<div class="container feature_section107"><br><h1 class="caps light">NEED HELP? <a href="#">Request Quote</a></h1></div>

<div class="clearfix"></div>


<?php include 'includes/website-footer.php' ?>

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
 
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<?php include 'includes/website-js.php' ?>

</body>
</html>
