<footer class="footer">

<div class="container">

    <div class="one_fourth">
     <h4>Address</h4>
        
        <ul class="faddress">
            <li><i class="fa fa-map-marker fa-lg"></i>&nbsp; 217,  Accord Classics, Station Road,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Goregaon East, Mumbai, 400063</li>
            <li><i class="fa fa-phone"></i>&nbsp; +91 8652 360 360</li>
           
            <li></i>Landmark: Above Anupam Store</a></li>
        </ul>
	</div><!-- end links -->
    
    <div class="one_fourth">
    <div class="qlinks">
    
    	<h4>Our Services</h4>
        
        <ul>
            <li><a href="#"><i class="fa fa-angle-right"></i>Why choose optron</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i>Advcanced SEO</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i>Workshop</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Corporate Trainings</a></li>
           
        </ul>
        
    </div>
	</div><!-- end address -->
    
        
    <div class="one_fourth">
    
    	<div class="qlinks">
    
    	<h4>Latest Courses</h4>
        
        <ul>
            <li><a href="#"><i class="fa fa-angle-right"></i>Why choose optron</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i>Advcanced SEO</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i>Workshop</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Corporate Trainings</a></li>
           
        </ul>
        
    </div>
        
    </div><!-- end site info -->
    
    <div class="one_fourth last">
        
       
        
        <div class="siteinfo">
    
    	<h4>About Us</h4>
        
        We aim to become #1 Digital Marketing and Technology Training Company. 
               
	</div>
        
        <div class="clearfix margin_top2"></div>
        


    </div><!-- end about -->

 </div><!-- end footer -->

<div class="clearfix"></div>

<div class="copyright_info">
<div class="container">

	<div class="clearfix"></div>
    
    <div class="one_half">
    
        Copyright © 2017 OPTRON All rights reserved.  <a href="/register/terms.html" rel="nofollow">Terms of Use</a> | <a href="/register/privacy-policy.html" rel="nofollow">Privacy Policy</a>
        
    </div> 
    
    <div class="one_half last">
        
        <ul class="footer_social_links">
            <li class="animate" data-anim-type="zoomIn"><a href="http://facebook.com/optronindia" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="http://www.twitter.com/optronindia" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
            
        </ul>
            
    </div>
    
</div>
</div><!-- end copyright info -->


</footer>