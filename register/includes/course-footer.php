<div class="copyright_info inner">
<div class="container">

	<div class="clearfix"></div>
    
    <div class="one_half">
    
        Copyright © 2017 Optron Technologies All rights reserved.  <a href="/terms.html">Terms of Use</a> | <a href="/privacy-policy.html">Privacy Policy</a>
        
    </div>
    
    <div class="one_half last">
        
       <ul class="footer_social_links">
            <li class="animate" data-anim-type="zoomIn"><a href="http://www.facebook.com/optronindia"><i class="fa fa-facebook"></i></a></li>
        
            
        </ul>
            
    </div>
    
</div>
</div>