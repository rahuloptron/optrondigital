<div class="feature_section13">
<div class="container">

    <h2>What people says</h2>
    <div class="linebg_3"></div>
	<br /><br />
    
    <div class="less6">
    <div id="owl-demo2" class="owl-carousel">
    
        <div class="item">
        	<div class="climg"><img src="../images/ajinkya.jpg" alt="" /></div>
        	<p class="bigtfont">If you are looking for Digital Marketing Course, you don't need to hunt. You will hardly find better than OPTRON in Mumbai. They are simple but practical. In-depth knowledge, practical sessions and you also get placement.</p>
			<br />
        	<strong>Ajinkya</strong> &nbsp;<em>Digital Marketing Manager</em>
        </div><!-- end slide -->
        
        <div class="item">
        	<div class="climg"><img src="../images/mayur-patil.jpg" alt="" /></div>
        	<p class="bigtfont">I will always recommend OPTRON Academy for Digital Marketing Training. Practical, in-depth, easy and fun. They will cover things in detail and on real-projects. Don't think much. Just join OPTRON Academy to learn digital marketing.</p>
			<br />
        	<strong>Mayur Patil</strong> &nbsp;<em>SEO Executive</em>
        </div><!-- end slide -->
        
        <div class="item">
        	<div class="climg"><img src="../images/anurag.jpg" alt="" /></div>
        	<p class="bigtfont">Optron is the best academy for Digital Marketing & SEO course. You will get real knowledge only from OPTRON. After visiting 10+ institute, I selected optron because of their culture, learning methods and in-depth training</p>
			<br />
        	<strong>Anurag</strong> &nbsp;<em>Software Developer</em>
        </div><!-- end slide -->
                
    </div>
    </div>
</div>
</div>