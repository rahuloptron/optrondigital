<header class="header">
 
	<div class="container_full_menu">
    
    <!-- Logo -->
<div class="logo"><a href="/index.html" id="logo"></a></div>
	<!-- Navigation Menu -->
    <div class="menu_main">
    
      <div class="navbar yamm navbar-default">
        
          <div class="navbar-header">
            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1" > <span></span>
              <button type="button" > <i class="fa fa-bars"></i></button>
            </div>
          </div>
          
          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
          
            <nav>
            
              <ul class="nav navbar-nav">
              
                <li class="dropdown yamm-fw"> <a href="index.html" class="dropdown-toggle active">Home</a>
                
                    <ul class="dropdown-menu">
                        <li> 
                        <!-- Content container to add padding -->
                            <div class="yamm-content">
                                <div class="row">
                                
                                <ul class="col-sm-6 col-md-3 list-unstyled">
                                <li>
                                    <p>Multi Use Demos</p>
                                </li>
                                <li><a href="index1.html"><i class="fa fa-caret-right"></i> Demo Version 1</a></li>
                                <li><a href="index2.html"><i class="fa fa-caret-right"></i> Demo Version 2</a></li>
                                <li><a href="index3.html"><i class="fa fa-caret-right"></i> Demo Version 3</a></li>
                                <li><a href="index4.html"><i class="fa fa-caret-right"></i> Demo Version 4</a></li>
                                <li><a href="index5.html"><i class="fa fa-caret-right"></i> Demo Version 5</a></li>
                                <li><a href="index6.html"><i class="fa fa-caret-right"></i> Demo Version 6</a></li>
                                <li><a href="index7.html"><i class="fa fa-caret-right"></i> Demo Version 7</a></li>
                                <li><a href="index8.html"><i class="fa fa-caret-right"></i> Demo Version 8</a></li>
                                <li><a href="index9.html"><i class="fa fa-caret-right"></i> Demo Version 9</a></li>
                                <li><a href="index10.html"><i class="fa fa-caret-right"></i> Demo Version 10</a></li>
                                </ul>
                                
                                <ul class="col-sm-6 col-md-3 list-unstyled">
                                <li>
                                    <p>Category Demos</p>
                                </li>
                                <li><a href="index11.html"><i class="fa fa-caret-right"></i> Demo Version 11</a></li>
                                <li><a href="index12.html"><i class="fa fa-caret-right"></i> Demo Version 12</a></li>
                                <li><a href="index13.html"><i class="fa fa-caret-right"></i> Demo Version 13</a></li>
                                <li><a href="index14.html"><i class="fa fa-caret-right"></i> Demo Version 14</a></li>
                                <li><a href="index.html"><i class="fa fa-caret-right"></i> Demo Version 15</a></li>
                                <li><a href="index-construction.html"><i class="fa fa-caret-right"></i> Construction</a></li>
                                <li><a href="index-restaurant.html"><i class="fa fa-caret-right"></i> Restaurant</a></li>
                                <li><a href="index-political.html"><i class="fa fa-caret-right"></i> Political</a></li>
                                <li><a href="index-realestate.html"><i class="fa fa-caret-right"></i> Real Estate</a></li>
                                <li><a href="index-education.html"><i class="fa fa-caret-right"></i> Education</a></li>
                                </ul>
                                
                                <ul class="col-sm-6 col-md-3 last list-unstyled">
                                <li>
                                    <p>Category Demos</p>
                                </li>
                                <li><a href="index-portfolio.html"><i class="fa fa-caret-right"></i> Portfolio</a></li>
                                <li><a href="index-musicband.html"><i class="fa fa-caret-right"></i> Music &amp; Band</a></li>
                                <li><a href="index-wedding.html"><i class="fa fa-caret-right"></i> Wedding</a></li>
                                <li><a href="index-nonprofit.html"><i class="fa fa-caret-right"></i> Non Profit</a></li>
                                <li><a href="index-medical.html"><i class="fa fa-caret-right"></i> Medical</a></li>
                                <li><a href="index-travel.html"><i class="fa fa-caret-right"></i> Travel</a></li>
                                <li><a href="index-law.html"><i class="fa fa-caret-right"></i> Law</a></li>
                                <li><a href="index-hosting.html"><i class="fa fa-caret-right"></i> Hosting </a></li>
                                <li><a href="index-shopping.html"><i class="fa fa-caret-right"></i> Shopping 1</a></li>
                                <li><a href="index-shopping2.html"><i class="fa fa-caret-right"></i> Shopping 2</a></li>                                
                                </ul>
                                
                                </div>
                            </div>
                        </li>
                    </ul>
                    
                </li>
                
                <li class="dropdown"><a href="about.html" class="dropdown-toggle">Pages</a>
                    <ul class="dropdown-menu" role="menu">
                    <li><a href="about.html">About Style 1</a></li>
                    <li><a href="about2.html">About Style 2</a></li>
                    <li><a href="about3.html">About Style 3</a></li>
                    <li><a href="about4.html">About Style 4</a></li>
                    <li><a href="about5.html">About Style 5</a></li>                    
                    <li><a href="services.html">Services Style 1</a></li>
                    <li><a href="services2.html">Services Style 2</a></li>
                    <li><a href="services3.html">Services Style 3</a></li>
                    <li><a href="services4.html">Services Style 4</a></li>
                    <li><a href="services5.html">Services Style 5</a></li>
                    <li><a href="team.html">Our Team Style 1</a></li>
                    <li><a href="team2.html">Our Team Style 2</a></li>
                    
                    <li class="dropdown-submenu mul"> <a tabindex="-1" href="#">Multi Level Submenu +</a>
                        <ul class="dropdown-menu">
                            <li><a href="#">Menu Item 1</a></li>
                            <li><a href="#">Menu Item 2</a></li>
                            <li><a href="#">Menu Item 3</a></li>
                        </ul>
                    </li>
                    </ul>
                </li>
        
                <li class="dropdown yamm-fw"> <a href="left-sidebar.html" class="dropdown-toggle">Features</a>
                    <ul class="dropdown-menu">
                    <li> 
                    <div class="yamm-content">
                      <div class="row">
                                          
                        <ul class="col-sm-6 col-md-3 list-unstyled">
                            <li>
                                <p>Useful Pages 1</p>
                            </li>
                            <li><a href="left-sidebar.html"><i class="fa fa-angle-right"></i> Left Sidebar</a></li>
                            <li><a href="right-sidebar.html"><i class="fa fa-angle-right"></i> Right Sidebar</a></li>
                            <li><a href="left-nav.html"><i class="fa fa-angle-right"></i> Left Navigation</a></li>
                            <li><a href="right-nav.html"><i class="fa fa-angle-right"></i> Right Navigation</a></li>
                            <li><a href="login.html"><i class="fa fa-angle-right"></i> Login Form</a></li>
                            <li><a href="register.html"><i class="fa fa-angle-right"></i> Registration Form</a></li>
                            <li><a href="404.html"><i class="fa fa-angle-right"></i> 404 Error Page</a></li>
                            <li><a href="faq.html"><i class="fa fa-angle-right"></i> FAQs Page</a></li>
                            <li><a href="video-bg.html"><i class="fa fa-angle-right"></i> Video Backgrounds</a></li>
                        </ul>
                        
                        <ul class="col-sm-6 col-md-3 list-unstyled">
                            <li>
                               <p>Useful Pages 2</p>
                            </li>
                            <li><a href="coming-soon.html" target="_blank"><i class="fa fa-angle-right"></i> Coming Soon</a></li>
                            <li><a href="history.html"><i class="fa fa-angle-right"></i> History Timeline</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Video BG Slider</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Header Styles</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Footer Styles</a></li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Masonry Gallerys</a> </li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Parallax Backgrounds</a> </li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Background Videos</a> </li>
                            <li><a href="#"><i class="fa fa-angle-right"></i> Create your Own</a> </li>
                        </ul>
                    
                        <ul class="col-sm-6 col-md-3 last list-unstyled">
                        <li>
                           <p>More Features</p>
                        </li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Mega Menu</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Diffrent Websites</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Cross Browser Check</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Premium Sliders</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Diffrent Slide Shows</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Video BG Effects</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> 100+ Feature Sections</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Use for any Website</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Lifetime Updates</a></li>
                        </ul>
                        
                      </div>
                    </div>
                    </li>
                    </ul>
                </li>
                
                <li class="dropdown"><a href="portfolio-5.html" class="dropdown-toggle">Portfolio</a>
                    <ul class="dropdown-menu" role="menu">
                    <li> <a href="portfolio-1.html">Single Item</a> </li>
                    <li> <a href="portfolio-5.html">Portfolio Masonry</a> </li>
                    <li> <a href="portfolio-4.html">Portfolio Columns 4</a> </li>
                    <li> <a href="portfolio-3.html">Portfolio Columns 3</a> </li>
                    <li> <a href="portfolio-2.html">Portfolio Columns 2</a> </li>
                    <li> <a href="portfolio-6.html">Portfolio + Sidebar</a> </li>
                    <li> <a href="portfolio-7.html">Portfolio Full Width</a> </li>
                    <li> <a href="portfolio-8.html">Image Gallery</a> </li>
                    </ul>
                </li>
                
                <li class="dropdown yamm-fw"> <a href="template1.html" class="dropdown-toggle">Shortcodes</a>
                    <ul class="dropdown-menu">
                    <li> 
                    <div class="yamm-content">
                      <div class="row">
                      
                        <ul class="col-sm-6 col-md-3 list-unstyled two">
                            <li><a href="template1.html"><i class="fa fa-plus-square"></i> Accordion &amp; Toggle</a></li>
                            <li><a href="template2.html"><i class="fa fa-leaf"></i> Title Styles</a></li>
                            <li><a href="template3.html"><i class="fa fa-bars"></i> List of Dividers</a></li>
                            <li><a href="template4.html"><i class="fa fa-exclamation-triangle"></i> Boxes Alert</a></li>
                            <li><a href="template5.html"><i class="fa fa-hand-o-up"></i> List of Buttons</a></li>
                            <li><a href="template6.html"><i class="fa fa-cog"></i> Carousel Sliders</a></li>
                            <li><a href="template7.html"><i class="fa fa-file-text"></i> Page Columns</a></li>
                            <li><a href="template8.html"><i class="fa fa-rocket"></i> Animated Counters</a></li>
                        </ul>
                        
                        <ul class="col-sm-6 col-md-3 list-unstyled two">
                            <li><a href="template9.html"><i class="fa fa-pie-chart"></i> Pie Charts</a></li>
                            <li><a href="template10.html"><i class="fa fa-flag"></i> Font Icons</a></li>
                            <li><a href="template11.html"><i class="fa fa-umbrella"></i> Flip Boxes</a></li>
                            <li><a href="template12.html"><i class="fa fa-picture-o"></i> Image Frames</a></li>
                            <li><a href="template13.html"><i class="fa fa-table"></i> Pricing Tables</a></li>
                            <li><a href="template14.html"><i class="fa fa-line-chart"></i> Progress Bars</a></li>
                            <li><a href="template15.html"><i class="fa fa-toggle-on"></i> List of Tabs</a></li>
                            <li><a href="template16.html"><i class="fa fa-paper-plane"></i> Popover &amp; Tooltip</a></li>
                        </ul>
                        
                        <ul class="col-sm-6 col-md-3 last list-unstyled">
                            <li>
                                <p>About Website</p>
                            </li>
                            <li class="dart">
                                <img src="http://placehold.it/288x145" alt="" class="rimg" />
                                There many variations passages available majority have alteration in some form by injected humoue on randomised words. 
                            </li>
                        </ul>
                         
                      </div>
                    </div>
                    </li>
                    </ul>
                </li>

                <li class="dropdown"> <a href="blog-3.html" class="dropdown-toggle">Blog </a>
                    <ul class="dropdown-menu multilevel" role="menu">
                    <li> <a href="blog-4.html">With Masonry</a> </li>
                    <li> <a href="blog.html">With Large Image</a> </li>
                    <li> <a href="blog-2.html">With Medium Image</a> </li>
                    <li> <a href="blog-3.html">With Small Image</a> </li>
                    <li> <a href="blog-post.html">Single Post</a> </li>
                    </ul>
                </li>
                
                <li class="dropdown"><a href="contact.html" class="dropdown-toggle">Contact</a>
                    <ul class="dropdown-menu" role="menu">
                    <li> <a href="contact.html">Contact Style 1</a> </li>
                    <li> <a href="contact2.html">Contact Style 2</a> </li>
                    <li> <a href="contact3.html">Contact Style 3</a> </li>
                    </ul>
                </li>
                
                
              </ul>
              
            </nav>
            
          </div>
        
      </div>
    </div>
        
	</div>
    
</header>