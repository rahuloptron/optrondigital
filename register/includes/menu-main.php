<header class="header innerpage">
 
	<div class="container_full_menu">
    
    <!-- Logo -->
    <div class="logo"><a href="/index.html" id="logo"></a></div>
		
	<!-- Navigation Menu -->
    <div class="menu_main">
    
      <div class="navbar yamm navbar-default">
        
          <div class="navbar-header">
            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1" > <span></span>
              <button type="button" > <i class="fa fa-bars"></i></button>
            </div>
          </div>
          
          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
          
            <nav>
            
              <ul class="nav navbar-nav">
              
                <li> <a href="/index.html" class="dropdown-toggle">Home</a>
                
                    
                    
                </li>
                
                <li class="dropdown"><a href="about.html" class="dropdown-toggle">Full Time Courses</a>
                    <ul class="dropdown-menu" role="menu">
                    <li><a href="about.html">Digital Marketing</a></li>
                    <li><a href="about2.html">SEO + SEM</a></li>
                    <li><a href="about3.html">Social Media Marketing</a></li>
                    <li><a href="about4.html">Wordpress Blogging</a></li>
                   <li><a href="about4.html">View All ..</a></li>
                    
                    
                    </ul>
                </li>
        
                
                
                <!--<li class="dropdown"><a href="portfolio-5.html" class="dropdown-toggle">Portfolio</a>
                    <ul class="dropdown-menu" role="menu">
                    <li> <a href="portfolio-1.html"> Single Item</a> </li>
                    <li> <a href="portfolio-5.html">Portfolio Masonry</a> </li>
                    <li> <a href="portfolio-4.html">Portfolio Columns 4</a> </li>
                    <li> <a href="portfolio-3.html">Portfolio Columns 3</a> </li>
                    <li> <a href="portfolio-2.html">Portfolio Columns 2</a> </li>
                    <li> <a href="portfolio-6.html">Portfolio + Sidebar</a> </li>
                    <li> <a href="portfolio-7.html">Portfolio Full Width</a> </li>
                    <li> <a href="portfolio-8.html">Image Gallery</a> </li>
                    </ul>
                </li>-->
                
                <li class="dropdown yamm-fw"> <a href="template1.html" class="dropdown-toggle">Next Batch</a>
                    <ul class="dropdown-menu">
                    <li> 
                    <div class="yamm-content">
                      <div class="row">
                      
                        <ul class="col-sm-6 col-md-3 list-unstyled two">
                            <li><a href="template1.html"><i class="fa fa-plus-square"></i> Accordion &amp; Toggle</a></li>
                            <li><a href="template2.html"><i class="fa fa-leaf"></i> Title Styles</a></li>
                            <li><a href="template3.html"><i class="fa fa-bars"></i> List of Dividers</a></li>
                            <li><a href="template4.html"><i class="fa fa-exclamation-triangle"></i> Boxes Alert</a></li>
                            <li><a href="template5.html"><i class="fa fa-hand-o-up"></i> List of Buttons</a></li>
                            <li><a href="template6.html"><i class="fa fa-cog"></i> Carousel Sliders</a></li>
                            <li><a href="template7.html"><i class="fa fa-file-text"></i> Page Columns</a></li>
                            <li><a href="template8.html"><i class="fa fa-rocket"></i> Animated Counters</a></li>
                        </ul>
                        
                        <ul class="col-sm-6 col-md-3 list-unstyled two">
                            <li><a href="template9.html"><i class="fa fa-pie-chart"></i> Pie Charts</a></li>
                            <li><a href="template10.html"><i class="fa fa-flag"></i> Font Icons</a></li>
                            <li><a href="template11.html"><i class="fa fa-umbrella"></i> Flip Boxes</a></li>
                            <li><a href="template12.html"><i class="fa fa-picture-o"></i> Image Frames</a></li>
                            <li><a href="template13.html"><i class="fa fa-table"></i> Pricing Tables</a></li>
                            <li><a href="template14.html"><i class="fa fa-line-chart"></i> Progress Bars</a></li>
                            <li><a href="template15.html"><i class="fa fa-toggle-on"></i> List of Tabs</a></li>
                            <li><a href="template16.html"><i class="fa fa-paper-plane"></i> Popover &amp; Tooltip</a></li>
                        </ul>
                        
                        <ul class="col-sm-6 col-md-3 last list-unstyled">
                            <li>
                                <p>About Website</p>
                            </li>
                            <li class="dart">
                                <img src="http://placehold.it/288x145" alt="" class="rimg" />
                                There many variations passages available majority have alteration in some form by injected humoue on randomised words. 
                            </li>
                        </ul>
                         
                      </div>
                    </div>
                    </li>
                    </ul>
                </li>

                <li class="dropdown"> <a href="/blog" class="dropdown-toggle">Blog </a>
                   
                </li>
                
                <li><a href="contact.html" class="dropdown-toggle active">Contact</a>
                    
                </li>
                
              
                
                
              </ul>
              
            </nav>
            
          </div>
        
      </div>
    </div>
        
	</div>
    
</header>