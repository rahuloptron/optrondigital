<footer class="footer">

<div class="container">

    <div class="one_fourth">
    <div class="qlinks">
    
    	<h4>Full Time Courses</h4>
        
        <ul>
            <li><a href="/training/digital-marketing-training-mumbai.html"><i class="fa fa-angle-right"></i> Digital Marketing Training</a></li>
            <li><a href="/training/social-media-training-mumbai.html"><i class="fa fa-angle-right"></i> Social Media Marketing</a></li>
            <li><a href="/training/digital-marketing-courses-mumbai.html"><i class="fa fa-angle-right"></i> Certified Digital Marketing</a></li>
            <li><a href="/seo-training/seo-for-beginners.html"><i class="fa fa-angle-right"></i> SEO Course for Beginners</a></li>
            
            
        </ul>
        
    </div>
	</div><!-- end links -->
    
    <div class="one_fourth">
    
  <div class="qlinks">
    
    	<h4>Resources</h4>
        
        <ul>
            <li><a href="/en/digital-marketing"><i class="fa fa-angle-right"></i> What is Digital Marketing</a></li>
            <li><a href="/training/for-business-owners.html"><i class="fa fa-angle-right"></i> Facebook Advertising</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> E-Mail Marketing</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Bloging & Wordpress</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Google Analytics</a></li>
            
            
        </ul>
        
    </div>
        	    
	</div><!-- end address -->
    
        
    <div class="one_fourth">
    <div class="siteinfo">
    
    	<h4>About Us</h4>
        
       Optron Academy is one of the best Digital Marketing institute in Mumbai. <br>We offer SEO Training, SMO Training, Google Adwords Training, Wordpress, Blogging, Lead generation training. <br>300+ Students, Small Batch Size, In-depth training by experts, focus on skills development than just knowledge
               
	</div>
    	    
    </div><!-- end site info -->
    
    <div class="one_fourth last">
        
        <h4>Address Info</h4>
        
        <ul class="faddress">
            <li><i class="fa fa-map-marker fa-lg"></i>&nbsp; 217, Accord Classic, Opp. Goregaon Station,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Goregaon East, Mumbai 4000063</li>
            <li><i class="fa fa-phone"></i>&nbsp; 8652360360</li>
            <li><i class="fa fa-print"></i>&nbsp; 9833189090</li>
            <li><a href="mailto:trainings@optroninfo.com"><i class="fa fa-envelope"></i>&nbsp; trainings@optroninfo.com</a></li>
        </ul>
        

    </div><!-- end about -->

 </div><!-- end footer -->

<div class="clearfix"></div>

<div class="copyright_info">
<div class="container">

	<div class="clearfix"></div>
    
    <div class="one_half">
    
        Copyright © 2017 Optron Technologies All rights reserved.  <a href="/terms.html">Terms of Use</a> | <a href="/privacy-policy.html">Privacy Policy</a>
        
    </div>
    
    <div class="one_half last">
        
        <ul class="footer_social_links">
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            
        </ul>
            
    </div>
    
</div>
</div><!-- end copyright info -->


</footer>