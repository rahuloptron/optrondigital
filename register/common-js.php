<script type="text/javascript" src="js/universal/jquery.js"></script>

<script src="js/animations/js/animations.min.js" type="text/javascript"></script>
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 
<script type="text/javascript" src="js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>
<script src="js/masterslider/jquery.easing.min.js"></script>

<script src="js/scrolltotop/totop.js" type="text/javascript"></script>

<!-- cubeportfolio --> 
<script type="text/javascript" src="js/cubeportfolio/js/jquery.cubeportfolio.min.js"></script> 
<script type="text/javascript" src="js/cubeportfolio/main.js"></script>

<!-- aninum --> 
<script src="js/aninum/jquery.animateNumber.min.js"></script>

<!-- tabs --> 
<script type="text/javascript" src="js/tabs3/tabulous.js"></script>
<script type="text/javascript" src="js/tabs3/js.js"></script>

<!-- carouselowl --> 
<script src="js/carouselowl/owl.carousel.js"></script> 
<script type="text/javascript" src="js/universal/custom.js"></script>
<script type="text/javascript" src="js/carouselowl/custom.js"></script> 

<!-- search box --> 
<script src="js/searchbox/overlay.js"></script>
<script>
  $(document).ready(function() {
	$('.overlay').overlay();
  });
</script>

<script type="text/javascript" src="js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="js/accordion/custom.js"></script>

<script src="js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>