 $(document).ready(function(){
   
      var sendBtn = $("#sendMessage");

      	$("#mobile").keypress(function() {
			var value = $(this).val();
			value = value.replace(/[^0-9]+/g, '');
			$(this).val(value);
		});


      	$("#course").change(function(){

      		var courseValue = $("#course").val();

      		if(courseValue == "Other"){
				$("#other").css("display", "block");
			}else {
				$("#other").css("display", "none");
			}

      	});		


      	$("input, select").focus(function(){
      		$(".message").empty();
      	});

      sendBtn.click(function(){
      	var name = $("#name").val();
      	var email = $("#email").val();
      	var mobile = $("#mobile").val();
      	var course = $("#course").val();
      	var otherCourse = $("#otherCourse").val();

      	var errorMessage = $(".message");


      	var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

      	if(!(/^[A-z ]+$/.test(name)) || name == ''){
      		errorMessage.text("Provide your name");
      	}else if(!(re.test(email)) || email == ''){
      		errorMessage.text("Provide your valid email");
      	}else if(mobile.length < 10 || mobile.length > 10 || mobile == ""){
      		errorMessage.text("Provide your valid number");
      	}else if(course == "0"){
      		errorMessage.text("Select course");
      	}else if($("#other").is(":visible") && otherCourse == ''){
      		errorMessage.css("color", "red");
      		errorMessage.text("Provide other course name");
      	}else if(otherCourse.length > 0){
      		course = otherCourse;

      		$.post("api-contact.php", {
			      			name: name,
			      			email: email,
			      			mobile: mobile,
			      			course: course
			      		}, function(data){
			      			if(data == "0"){
			      				errorMessage.text("Resubmit form");
			      			}else{
			      				errorMessage.css("color", "green");
			      				setTimeout(function(){

								function redirect()
								{window.location="http://www.optron.in";
								
								}
								redirect();

								}, 3000);
			      				errorMessage.text("Your request has been submitted");
			      			}
			      		});
      	}else {
      		$.post("api-contact.php", {
			      			name: name,
			      			email: email,
			      			mobile: mobile,
			      			course: course
			      		}, function(data){
			      			if(data == "0"){
			      				errorMessage.text("Resubmit form");
			      			}else{
			      				errorMessage.css("color", "green");
			      				setTimeout(function(){

								function redirect()
								{window.location="http://www.optron.in";
								}
								redirect();

								}, 30);
			      				errorMessage.text("Your request has been submitted");
			      			}
			      		});
      	}





      });

  })
