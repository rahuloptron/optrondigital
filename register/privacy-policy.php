﻿<!doctype html>
<html lang="en-gb" class="no-js">
<head>
	<title>Optron Privacy Policy</title>
	<meta name="keywords" content="" />
	<meta name="description" content="" />
  <?php include "/includes/course-css.php" ?>
</head>
<body>
<div class="wrapper_boxed">
<div class="site_wrapper">
<?php include "/includes/course-menu.php" ?>
<div class="clearfix"></div>
<div class="margin_top5"></div>
<div class="feature_sec5">
<div class="container">
<div class="one_full">
        <h2 class="small">Privacy <strong>Policy</strong></h2>
        
        <p>This Privacy Policy governs the manner in which Optron Technologies collects, uses, maintains and discloses information collected from users (each, a "User") of the http://www.optron.in website ("Site"). This privacy policy applies to the Site and all products and services offered by Optron Technologies.</p>
		<br />
        <h4> Personal identification information</h4>
		<p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, register on the site, subscribe to the newsletter, respond to a survey, fill out a form, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name, email address, phone number. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>
        <br />
       
       <h4>Non-personal identification information</h4>
      <p> We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>
      <br />
       <h4>Web browser cookies</h4>
      <p>Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>
      <h3>Non-personal identification information</h3>
        <p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>
        <br />
        <h3>Web browser cookies</h3>
        <p>Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p><br>
        <h3>How we use collected information</h3>
        <h5>Optron Technologies may collect and use Users personal information for the following purposes:</h5>
        <ul>
<li><i>- To improve customer service</i><br>
	Information you provide helps us respond to your customer service requests and support needs more efficiently.</li>
<li><i>- To personalize user experience</i><br>
	We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.</li>
<li><i>- To improve our Site</i><br>
	We may use feedback you provide to improve our products and services.</li>
<li><i>- To process payments</i><br>
	We may use the information Users provide about themselves when placing an order only to provide service to that order. We do not share this information with outside parties except to the extent necessary to provide the service.</li>
<li><i>- To run a promotion, contest, survey or other Site feature</i><br>
	To send Users information they agreed to receive about topics we think will be of interest to them.</li>
<li><i>- To send periodic emails</i><br>
We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.</li>
</ul>
<h3>How we protect your information</h3><br>

<p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p><br><br>

<h3>Sharing your personal information</h3><br>

<p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.We may use third party service providers to help us operate our business and the Site or administer activities on our behalf, such as sending out newsletters or surveys. We may share your information with these third parties for those limited purposes provided that you have given us your permission.</p><br>

<h3>Changes to this privacy policy</h3><br>

<p>Optron Technologies has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page and send you an email. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p><br>

<h3>Your acceptance of these terms</h3><br>

<p>By using this Site network of web sites, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.
If we decide to change our privacy policy, we will post those changes on this page so that you are always aware of what information we collect, how we use it, and under what circumstances we disclose it.
</p><br>

<h3>Contacting us</h3><br>

<p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br>
<a href="http://www.optron.in">Optron Technologies</a><br>
<a href="http://www.optron.in">http://www.optron.in</a><br>
217, Accord Classic, 5 minutes distance from railway station, Goregoan East<br>
9833189090<br>
bhavesh@optronifo.com<br>
<br></p>
</div><!-- end section -->
</div>
</div>
<?php include "/includes/footer.php" ?>
</div>
</div>
<?php include "/includes/common-js.php" ?>
</body>
</html>
