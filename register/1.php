﻿<!doctype html>
<html lang="en-gb" class="no-js">
<!--<![endif]-->
<head>
<title>Register for Free</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="Free Seminar" />
<meta name="description" content="Free Registration Bhavesh Gudhka" />
<?php include "../includes/course-css.php" ?>
</head>
<body>
<div class="site_wrapper">
<div class="clearfix"></div>


<div class="clearfix"></div>
    <div class="feature_section89">
<div class="container">

    <div class="one_half">
    
     <h2>Our Popular Trainings </h2>   
 <div class="clearfix margin_top2"></div>
 <p class="bigtfont">We offer trainings on various topics to help you learn new things. Our trainings are for entrepreneurs, business owners, students and working professionals.    </p>
 <br>
 <h3>Popular Trainings & Workshops</h3>
  <ul class="list_divlines">		
            <li> <i class="fa fa-check "></i> Free workshop for business owners about how to make website</li>  
                <li> <i class="fa fa-check "></i> Linkedin for entrepreneurs and marketnig professionals</li>
                <li> <i class="fa fa-check"></i> Mailchimp automation and advanced features</li>  
               <li> <i class="fa fa-check"></i> SMS Marketing for promotion and lead generation</li>  
               
		</ul>
            
      </div>


	<div class="one_half last">
    	<script type="text/javascript" src="//static.mailerlite.com/data/webforms/418857/h3r3o7.js?v1"></script>
    <br>
        
        
    </div>

</div>
</div>

<div class="clearfix"></div>

<div class="feature_section8">
<div class="container">
 <div class="left">
    	<h1 class="caps"><strong>Experience the BEST</strong> <br/>Learning Experience</h1>
	</div>  
    <div class="right">
        <h4 class="caps white">Professional, Practical & Reasonable</h4>
        
        <ul>
            <li><i class="fa  fa-check"></i>Experienced Trainers who have passion for training</li>
            <li><i class="fa  fa-check"></i>Training certificate is provided to participants</li>
            <li><i class="fa  fa-check"></i>We will never compromise with quality </li>
    	</ul>
    </div>
    
</div>
<div class="clearfix margin_bottom8"></div>
</div><!-- end feature section 8 -->
 <div class="clearfix"></div>

 <?php include "../includes/training-reviews.php" ?>

<div class="clearfix"></div>

<div class="clearfix"></div>

<?php include "../includes/footer.php" ?>
</div>
<?php include "../includes/course-js.php" ?>
<?php include "../includes/ga.php" ?>

</body>
</html>
