<!doctype html>

<html lang=en-gb class=no-js>

<head>
<title>Optron Digital Clients</title>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge" />
<meta name=keywords content />
<meta name=description content="Optron clients  " />
<?php include "includes/common-css.php" ?>
 <?php include "includes/ga.php" ?>
</head>
<body>
<div class=site_wrapper>
<?php include "includes/menu-home.php" ?>
<div class=clearfix></div>


<div class=clearfix></div>


    
 <div class="page_title2">
<div class="container">
    
    <h1>Clients</h1>
    <div class="pagenation">&nbsp;<a href="/index.html">Home</a> <i>/</i>Clients</div>
     
</div>
</div>
<div class="content_fullwidth">
<div class="container">
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="images/clients/adam-fabriwerk.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Adam Fabriwerk</a></strong></h5>
            <p>Manufactures of processing systems for pharma</p>
            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/ajfilms.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Aj Films</a></strong></h5>
            <p>Best Photography in Mumbai</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/agdigitas.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ag Digitas</a></strong></h5>
                <p>IT Solutions Company</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/amita-desai.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Amita desai</a></strong></h5>
                <p>Professional services from renown Company Secretaries firm</p>
            </div>
            
        </div>
    
    </div>

        <div class="margin_top3"></div><div class="clearfix"></div>

        <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/ashraya.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ashryay Infotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/blacpeper.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Black Peper</a></strong></h5>
                <p>Black Pepper is an Exhibition Stall Design company</p>
            </div>
            
        </div>
    
    </div>
    
    
        <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/enjay.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Enjay Solution</a></strong></h5>
                <p>Expert in CRM and Telephony Solutions across verticals</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/genesis.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Genesis Telesecure</a></strong></h5>
                <p>Genesis Telesecure LLP offers complete range of solutions</p>
            </div>
            
        </div>
    
    </div>

        <div class="margin_top3"></div><div class="clearfix"></div>
              
    <div class="one_fourth">

    <div class="flips4">
    
        <div class="flips4_front flipscont4">
       <img src="images/clients/grecelllogo.png" alt="" >
        </div>
        
        <div class="flips4_back flipscont4">
            <h5><strong><a href="#">Grecells</a></strong></h5>
            <p>Disaster recovery solutions for Windows server</p>
        </div>
        
    </div>

    </div>      

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/heminfotech.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Hem Infotech</a></strong></h5>
                <p>IT Solutions and Services Company</p>

            </div>
            
        </div>
    
    </div>
    
        <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/hi-will-logo.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Hi-Will Education</a></strong></h5>
                <p>Engineers classes</p>

            </div>
            
        </div>
    
    </div>

     <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/impetix.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#"></a></strong></h5>
                <p></p>

            </div>
            
        </div>
    
    </div>

     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/neotech.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/prarambh.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Prarambh Security</a></strong></h5>
                <p>Security Solutions</p>

            </div>
            
        </div>
    
    </div>
       
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/pratrikshainterior.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Pratiksha Interior</a></strong></h5>
                <p>Modular kitchen, Office Furniture and Roofing</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/revital.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Revital Tricology</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>

     <div class="margin_top3"></div><div class="clearfix"></div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/rohmcomputers.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Rohm Computers</a></strong></h5>
                <p>Multi-specialty Dental clinic in Mumbai</p>

            </div>
            
        </div>
    
    </div>

      <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/rsbl.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">RSBL</a></strong></h5>
                <p>RiddiSiddhi Bullions Limited (RSBL)</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/revelation.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Revelation</a></strong></h5>
                <p>Leading Event Industry</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/setutech-new.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Setutech</a></strong></h5>
                <p>Providing 100% authentic licensed softwares</p>

            </div>
            
        </div>
    
    </div>
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/supportplus.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Support Plus</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div>

        <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/studioroopkala.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Studio Roopkala</a></strong></h5>
                <p>All kind of Photography & Videography since 1980</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/shaktienterprises.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Shakti Enterprises</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                     
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/sastadeals.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Sasta Deals</a></strong></h5>
                <p>Buy Online shopping</p>

            </div>
            
        </div>
    
    </div>

     <div class="margin_top3"></div><div class="clearfix"></div>

      <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/techs.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Techsofya</a></strong></h5>
                <p>Yardi software solution</p>

            </div>
            
        </div>
    
    </div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/viraldental.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Dr. Viral Dental</a></strong></h5>
                <p>Multi-specialty Dental clinic in Mumbai</p>

            </div>
            
        </div>
    
    </div>

        <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/welcomecineplex.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Welcome Cineplex</a></strong></h5>
                <p>Cineplex</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/wits.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Wits Interactive</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>

                   
   
    
 </div>
</div>
<div class=divider_line23></div>



<?php include "includes/test.php" ?>

<div class=clearfix></div>





<div class=clearfix></div>
<?php include "includes/footer.php" ?>
<a href=# class=scrollup>Scroll</a>
</div>

<script type="text/javascript" src="js/universal/jquery.js"></script>
<script src="js/animations/js/animations.min.js" type="text/javascript"></script>
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 

<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>
<script src="js/scrolltotop/totop.js" type="text/javascript"></script>

<script type="text/javascript" src="js/universal/custom.js"></script>

<?php include "includes/ga.php" ?>

</body>
</html>