<!doctype html>
<html lang=en-gb class=no-js>
<!--<![endif]-->
<head>
<title>Optron Digital Marketing Agency in Mumbai | Digital Marketing Training Institute</title>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge" />
<meta name=keywords content />
<meta name=description content="One of the most innovative Digital Marketing Agency in Mumbai for SEO Services, Digital Marketing Consulting, Social Media Consulting, Email Marketing, Lead Generation, Online promotion, Google Adwords, Facebook Advertising, PPC services " />

<link rel="shortcut icon" href="images/favicon.png">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />
<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/simpleline-icons/simple-line-icons.css" media="screen" />
<link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />
<link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" /> 
<link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
<link href="js/mainmenu/demo.css" rel="stylesheet">
<link href="js/mainmenu/menu3.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="js/accordion/style.css" />
<link href=js/cubeportfolio/cubeportfolio.min.css rel=stylesheet type=text/css>
<link href=js/carouselowl/owl.transitions.css rel=stylesheet>
<link href=js/carouselowl/owl.carousel.css rel=stylesheet>
<link href=js/carouselowl/owl.theme.css rel=stylesheet>
<link rel="stylesheet" type="text/css" href="js/accordion/style.css" />
    
    
    
</head>
<body>
<div class=site_wrapper>
<?php include "includes/menu-home.php" ?>
<div class=clearfix></div>



<div class="feature_section_12">
<div class="container">



<h1>Digital Marketing Services</h1>

<p>We are a full-service digital marketing agency. With our detail-oriented and ROI focused digital marketing services we help businesses to improve their online visibility and increase sales.</p>

  <img src="images/e1.png" alt="description" class="">
 

    
</div>
</div>

<div class="clearfix"></div>






<div class="feature_section_13">
<div class="container">

<h2>Why Digital Marketing</h2>

<h4>We offer more than 15 digital marketing services for small companies to large organizations. Our digital marketing services include Digital Marketing Consulting, SEO Consulting etc.</h4>


<div class="onecol_forty">
 
    
     <img src="images/d2.png" alt="description" class="">
    </div>



    <div class="onecol_sixty last">
    
   
   
    
      <h3>Plan your content strategy and build search authority
       
       </h3>
        
       
  
    
 
  <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved
</p>


 <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved
</p>


<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved
</p>
<div class="clearfix"></div>
<!--  <div class="one_full stcode_title7">
   
     <h3>Solutions
      
       <span class="line"></span></h3>
     
   </div>
   

 <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved
</p> -->


    </div>
 <div class="clearfix margin_top5"></div>

    <div class="onecol_sixty">
 
    
      <h3>Plan your content strategy and build search authority
       
       </h3>
        
       
   
    
 
  <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved
</p>


 <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved
</p>


<p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved
</p>
    
    
    </div>



    <div class="onecol_forty last">
    
    <img src="images/d2.png" alt="description" class="">
    



<div class="clearfix"></div>
<!--  <div class="one_full stcode_title7">
   
     <h3>Solutions
      
       <span class="line"></span></h3>
     
   </div>
   

 <p>Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy. Various versions have evolved
</p> -->


    </div>



    
</div>
</div>


<div class="feature_section191">
                    <div class="container">
                        <h2 class="caps"><strong>We build website for:</strong></h2>
                        <div class="clearfix margin_bottom3"></div>
                        <div class="one_third"> <i class="fa fa-user-md "></i>
                            <h4>Clothing / Apparel</h4>
                            <p>Website for doctors, clinics, hospitals and pharmaceutical companies</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-shopping-cart"></i>
                            <h4>Sports Equipment</h4>
                            <p>Start selling online using our e-commerce website development services</p>
                            <div class="clearfix margin_bottom5"></div> 
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third"> <i class="fa fa-home"></i>
                            <h4>Fashion Store</h4>
                            <p>Website development &amp; Digital marketing for real estate industry</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-bus"></i>
                            <h4> Grocery Store</h4>
                            <p>Static or dynamic website for travel agency and tours &amp; travel business</p>
                            <div class="clearfix margin_bottom5"></div> 
                           
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third last"> <i class="fa fa-graduation-cap"></i>
                            <h4>Furniture</h4>
                            <p>Website for Schools, Colleges, Training institutes, coaching classes etc. </p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-desktop"></i>
                            <h4>Watches</h4>
                            <p>Mobile friendly website, marketing solution for IT Industry</p>
                            <div class="clearfix margin_bottom5"></div> 
                            
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                    </div>
                </div>




<div class="feature_section334">
<div class="container">

 <div class="one_full stcode_title7">
    
      <h2>Our Projects<br><span class="line"></span> </h2>
        
       
    
    </div>

  <div class="one_third">
<a href="#">
<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="images/enjay-1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Enjay World <br> Sugar CRM Partner</h6>
                </div>
                </a>
</div>


<div class="one_third">
<a href="#">
<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="images/enjay-1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Investigationes demonstraverunt legere</h6>
                </div>
              </a>
</div>


<div class="one_third last">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="images/enjay-1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Investigationes demonstraverunt legere</h6>
                </div>
</div>




</div>
</div>


<div class="clearfix"></div>
<div class="feature_section_tech">
<div class="container">

  <div class="one_full">
       
   
    <h2 class="caps"><strong>Technologies We Use</strong></h2>
        <div class="clearfix margin_bottom2"></div>
        <ul>
          <li><img src="../images/php.png" alt=""></li>
            <li><img src="../images/scripts-logo2.jpg" alt=""></li>
            <li><img src="../images/scripts-logo3.jpg" alt=""></li>
            <li><img src="../images/scripts-logo4.jpg" alt=""></li>
           <li><img src="../images/scripts-logo7.jpg" alt=""></li>
            <li><img src="../images/scripts-logo6.jpg" alt=""></li>
           
          
        </ul>

        <ul>
          <li><img src="../images/scripts-logo1.jpg" alt=""></li>
            <li><img src="../images/scripts-logo2.jpg" alt=""></li>
            <li><img src="../images/scripts-logo3.jpg" alt=""></li>
            <li><img src="../images/scripts-logo4.jpg" alt=""></li>
           <li><img src="../images/scripts-logo7.jpg" alt=""></li>
            <li><img src="../images/scripts-logo6.jpg" alt=""></li>
           
          
        </ul>
        
    </div>
    

</div>
</div>
<div class="clearfix"></div>


                <div class="feature_section265">
<div class="container">

    <div class="one_half">
    
     <h2>Static Website is suitable for Small and Medium size Business</h2>   <p class="bigtfont">We take care of the smallest details while making your website and writing content to ensure that the above mentioned objectives are done with perfection. objectives are done with perfection.<br> </p>
 <div class="clearfix margin_top2"></div>
  <img src="images/t2.png" alt=""/> 
        </div>

<div class="one_half last">
      
<script type="text/javascript" src="//static.mailerlite.com/data/webforms/446663/u0p2r9.js?v3"></script>

    </div>
  

</div>
</div>

<!-- <div class="feature_section_14">
<div class="container">

<div class="one_third">
  

 
 


</div>
</div>

<div class="one_third">
  
  
</div>


<div class="one_third last">
  
  
</div>



    
</div>
</div>
 -->

<!-- <div class="feature_section335">
<div class="container">


<div class="onecol_forty">
 <div class="one_full stcode_title7">
    
      <h2>The Happy Client <br>About Us</h2>
        
       
    
    </div>
</div>




<div class="onecol_sixty last">

  <div class="box">

  <p>We got 70% increase in organic traffic in 3 months, that's amazing. I would definitely recommend them to anyone interested in Search Engine Optimization. I would definitely recommend them </p>
  <br>

  <b>Limesh Parekh</b>
    <em> ~ Enjay CEO</em>




<div class="avatar" >
                                            <img src="images/test01.png" alt="avatar">
                                        </div> 
</div>
  </div>



</div>
</div>
 -->

<div class="clearfix"></div>


<!-- <div class="feature_section334">
<div class="container">

 <div class="one_full stcode_title7">
    
      <h2>View More Projects<br><span class="line"></span> </h2>
        
       
    
    </div>

  <div class="one_third">
<a href="#">
<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="images/enjay-1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Enjay World <br> Sugar CRM Partner</h6>
                </div>
                </a>
</div>


<div class="one_third">
<a href="#">
<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="images/enjay-1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Investigationes demonstraverunt legere</h6>
                </div>
              </a>
</div>


<div class="one_third last">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="images/enjay-1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Investigationes demonstraverunt legere</h6>
                </div>
</div>




</div>
</div> -->



<div class=divider_line23></div>


<div class="clearfix"></div>


<div class="feature_sec93">
<div class="container">
  <div class="one_third">
	<div>
      	<div class="peoplesays">
  	We got 70% increase in organic traffic in 3 months, that's amazing. I would definitely recommend them to anyone interested in Search Engine Optimization.
                </div>
    <div class="peoimg"><img src="../images/site-img1.jpg" alt="" /> <strong>- Limesh Parekh</strong></div>
    </div>
    </div>
    <div class="one_third">
    <div>
 	<div class="peoplesays">
 	One of the best training institute if you want to learn SEO or Digital Marketing. I would always recomment OPTRON for SEO or any other trainings
                </div>
     <div class="peoimg"><img src="../images/anurag.jpg" alt="" /> <strong>- Anurag Sharma</strong></div>
    </div>
   </div>
   <div class="one_third last">
   <div>
  <div class="peoplesays">
                
                	I wanted SEO training for my team and I found OPTRON to be perfect match. Their training methodology is one of the best in industry compared to any other institute. 
                </div>
        <div class="peoimg"><img src="../images/vishal.jpg" alt="" /> <strong>- Vishal Waghmare</strong></div>
   </div>
   </div>
   </div>
   </div>
   <div class=clearfix></div>


<div class=divider_line23></div>
<div class=clearfix></div>
<div class="feature_section629">
    <div class="container">
      <h3 class="white caps">Grow  Your Business to next Level  </h3>
      <a href="inquiry" class="button one">Let's Start</a> </div>
  </div>
<div class=clearfix></div>
<div class="feature_section_contact">
<div class="container">

    <div class="box1"> <i class="fa fa-mobile-phone"></i>
    <h4 class="caps">Phone Number<b>+91 9833189090</b></h4></div>
    
    <div class="box2"> <i class="fa fa-envelope-o"></i>
    <h4 class="caps">Email Address<b>bhavesh@optron.in</b></h4></div>
    
    <div class="box3"> <i class="fa fa-map-marker"></i>
    <h4 class="caps">Location Address<b>217, Accord Classics, Station Road,
     Goregaon East, Mumbai, 400063</b></h4></div>
    
   <!--  <div class="box4">
      <div class="logo"><a href="/index.html" id="logo"></a></div>
   </div> -->

</div>    
</div>
<div class="copyright_info2">
<div class="container">

  <div class="clearfix"></div>
    
    <div class="one_half">
    
        Copyright © 2017 OPTRON All rights reserved.  <a href="/terms.html" rel="nofollow">Terms of Use</a> | <a href="/privacy-policy.html" rel="nofollow">Privacy Policy</a>
        
    </div>
    
    <div class="one_half last">
        
       <!--  <ul class="footer_social_links">
           <li class="animate" data-anim-type="zoomIn"><a href="http://facebook.com/optronindia" rel="nofollow"><i class="fa fa-facebook"></i></a></li>
           <li class="animate" data-anim-type="zoomIn"><a href="http://www.twitter.com/optronindia" rel="nofollow"><i class="fa fa-twitter"></i></a></li>
           
       </ul>
            -->
    </div>
    
</div>
</div>
<a href=# class=scrollup>Scroll</a>
</div>
<script type="text/javascript" src="js/universal/jquery.js"></script>
<script src="js/animations/js/animations.min.js" type="text/javascript"></script>
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 
<script type="text/javascript" src="js/mainmenu/sticky.js"></script>
<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>
<script src="js/masterslider/jquery.easing.min.js"></script>
<script src="js/scrolltotop/totop.js" type="text/javascript"></script>
<script type="text/javascript" src="js/tabs3/tabulous.js"></script>
<script type="text/javascript" src="js/tabs3/js.js"></script>
<script src="js/carouselowl/owl.carousel.js"></script> 
<script type="text/javascript" src="js/universal/custom.js"></script>
<script type="text/javascript" src="js/carouselowl/custom.js"></script> 
<script type="text/javascript" src="js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="js/accordion/custom.js"></script>
<script src="js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>
<?php include "includes/ga.php" ?>
<!-- MailMunch for Optron Main Website -->
<!-- Paste this code right before the </head> tag on every page of your site. -->

</body>
</html>