<!doctype html>

<html lang=en-gb class=no-js>

<head>
<title>Optron Digital Clients</title>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge" />
<meta name=keywords content />
<meta name=description content="Optron clients  " />
<?php include "includes/common-css.php" ?>

</head>
<body>
<div class=site_wrapper>
<?php include "includes/menu-home.php" ?>
<div class=clearfix></div>


<div class=clearfix></div>


    
 <div class="page_title2">
<div class="container">
	
    <h1>Clients</h1>
    <div class="pagenation">&nbsp;<a href="/index.html">Home</a> <i>/</i>Clients</div>
     
</div>
</div>
<div class="content_fullwidth">
<div class="container">
	

    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
	<div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            	<img src="images/clients/adam.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
            	<h5><strong><a href="#">Enjay</a></strong></h5>
                <p>Empowering enterprise with ennovation</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/ajfilms2.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
            	<h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_third last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/clients7.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
            	<h5><strong><a href="#">Aashray</a></strong></h5>
                <p>Leading IT service provider company</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
		<div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/blackpeper2.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
            	<h5><strong><a href="#">Rohm Computers</a></strong></h5>
                <p>Rohm computers offers comprehensive range of IT Products</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/clients1.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
            	<h5><strong><a href="#">Shakti Enterprise</a></strong></h5>
                <p>Reliable & Affordable Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_third last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/genesis2.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
            	<h5><strong><a href="#">Revital Trichology</a></strong></h5>
                <p>Best Trichology Clinic in Mumbai for Hair fall treatment</p>

            </div>
            
        </div>
    
    </div>
    
    
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
		<div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/clients9.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
            	<h5><strong><a href="#">Agdigitas</a></strong></h5>
                <p>RELIABLE, AFFORDABLE AND SCALABLE IT Infrastructure</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/heminfotech2.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
            	<h5><strong><a href="#">Dental Avenue</a></strong></h5>
                <p>Multi-specialty Dental clinic in Mumbai</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_third last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/clients9.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
            	<h5><strong><a href="#">Grecells</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div>


      <div class="margin_top3"></div><div class="clearfix"></div>
    
        <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="images/clients/wits.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Agdigitas</a></strong></h5>
                <p>RELIABLE, AFFORDABLE AND SCALABLE IT Infrastructure</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_third">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/adam.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Dental Avenue</a></strong></h5>
                <p>Multi-specialty Dental clinic in Mumbai</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_third last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="images/clients/techs.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Grecells</a></strong></h5>
                <p>Infancy versions have evolved over years.</p>

            </div>
            
        </div>
    
    </div>
    

 </div>
</div>
<div class=divider_line23></div>



<?php include "includes/test.php" ?>

<div class=clearfix></div>





<div class=clearfix></div>
<?php include "includes/footer.php" ?>
<a href=# class=scrollup>Scroll</a>
</div>

<?php include "includes/common-js.php" ?>

<?php include "includes/ga.php" ?>


</body>
</html>