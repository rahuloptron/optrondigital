<!doctype html>
<html lang=en-gb class=no-js>
<head>
<title>B2B lead generation services in Mumbai</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="b2b lead generation, b2c lead generation" />
<meta name="description" content="Generate qualified leads for your business. We are one of the top lead generation companies in Mumbai. We offer B2B and B2C lead generation service for real estate, healthcare, fitness, ecommerce, automobile companies" />
<?php include "../includes/common-css.php" ?>
</head>
<body>
<div class="site_wrapper">
<?php include "../includes/menu-home.php" ?>
<div class="clearfix"></div>
<div class="feature_section83">
<div class="container">
    <div class="content"><h1>LEARN DIGITAL MARKETING</h1>    
    <h4 class="less6">Learn Digital Marketing online from digigurukul. Digigurukul covers all important topics about digital marketing, blogging, website designing, business promotion etc.
</h4>
    <br />
     <a href="/register" class="button five">Register for Free</a>
    </div>

</div>
</div>
<div class="clearfix"></div>
<div class="content_fullwidth less2">
<div class="container">
<div class="content_left">
  <h2>Digital Marketing Basics</h2>
       <p class="bigtfont">Many people think digital marketing means using social media or SEO or Google AdWords for marketing. That's not true. Digital marketing is a concept of using digital channels, degital devices, websites, mobile apps, email, sms and internet to target right people with right message</p>
 <div class="clearfix margin_top3"></div>
 <div class="big_text1">Digital Marketing is the future of marketing and this is going to stay for another 15+ years </div>
        <div class="clearfix margin_top3"></div>
        
       <h2>Let's understand digital marketing in detail</h2>
       <p class="bigtfont">Understanding digital marketing is not very difficult if you know fundamentals of marketing and internet. For those who are not from marketing and technical background,</p>
 <div class="project_details"> 
            <h3>Digital Marketing Course Details (Classroom Training)</h3>
            <span><strong>Total Duration</strong> <em>3 months (3 days per week)</em></span>
            <span><strong>Timing</strong> <em>Morning | Afternoon </em></span> 
            <span><strong>Training Type</strong> <em>Practical Training (Learning by Doing)</em></span> 
            <span><strong>Total Sessions</strong> <em>36 sessions (72 hrs) of 2 hrs. Each 3 days per week</em></span>
            
            <span><strong>Venue</strong> <em>Goregaon Center 
217, Accord Classic, Station Road, Above Anupam Stationery Store, 
Goregaon East, Mumbai 400063</em></span>
            <div class="clearfix margin_top5"></div>
           <a href="/contact.html" class="but_full"><i class="fa fa-hand-o-right"></i>&nbsp; Download Brochure</a>
            <div class="clearfix margin_top5"></div>
        </div>
    
     <p class="f18">Digital marketing is term which has become popular in recent days, before digital marketing people used to call it online marketing or internet marketing. Digital marketing is not very new concept, digital marketing was started from the day internet and email started.</p>
     
    <div class="big_text1">Digital marketing is new name given to Internet marketing + E-mail marketing + mobile marketing all combined </div>
        <div class="clearfix margin_top3"></div>
     <h2>History of Digital Marketing</h2>
    
     <p class="f18">As I told you, digital marketing is not new. Digital marketing started when Internet was started and people started creating websites, people started using emails for marketing and promotions. Internet became popular very soon and companies realised that this can be a good medium for marketing.</p>
      <p class="f18">By the time Google was introduced, Yahoo and Altavista were used for searching the Internet. Then came Google in 1996 and became popular quickly due to their algorith and relevant results. Google changed the way SEO was done and soon became #1 search engine in market</p>
     <h2>Beginning of the Digital Marketing</h2>
    
     <p class="f18">After Google, Facebook and Youtube started becoming popular. Internet was available on mobile devices and people started using mobile phones and smart phones for social networking, browsing and checking emails. E-Commerce became popular and now people started buying products online.</p>
      <p class="f18">Very soon, people started using Internet on Desktops, Laptops, Mobiles, Tabs and also on TV's. This is how people started using more digital devices to access content using Internet and Internet marketing was transformed into Digital Marketing.</p>
    <div class="big_text1">This is test</div>
         
         <ul>
            <li>
              <h4>Mega Menu</h4>
                <p class="f18">Lorem Ipsum is simply dummy text of theprinting and typesetting it has the randomised words.</p>
            </li>
            <li>
              <h4>Mega Menu</h4>
                <p>Lorem Ipsum is simply dummy text of theprinting and typesetting it has the randomised words.</p>
            </li>
        </ul>
    <div class="toc1">
     <div class="t1">
            <ul>
            
            <li>Unlimited Online Video Courses</li>
            <li>Expert Trainers with Experience</li>
            <li>Unlimited Free Webinars</li>
            <li>Access to Unlimited E-books</li>
            <li>Get Certificate after Trainings</li>
            <li>Monthly Tips & Tricks Emails</li>
            </ul>
        </div>
   </div>
   <div class="margin_top5"></div>
        <div class="clearfix"></div>
    
</div>
<?php include "../assets/sidebar-marketing.php" ?>
</div>
</div>
<div class="clearfix"></div>
<?php include "../assets/news.php" ?>
<div class="clearfix"></div>
<?php include "../includes/test.php" ?>
<div class="clearfix"></div>
<?php include "../includes/footer.php" ?>
</div>
<?php include "../includes/common-js.php" ?>
<?php include "../includes/ga.php" ?>
</body>
</html>