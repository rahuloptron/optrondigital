<!doctype html>

<html lang=en-gb class=no-js>

<head>
<title>Social Media marketing company in Mumabai</title>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge" />
<meta name=keywords content />
<meta name=description content="One of the most innovative Digital Marketing Agency in Mumbai for SEO Services, Digital Marketing Consulting, Social Media Consulting, Email Marketing, Lead Generation, Online promotion, Google Adwords, Facebook Advertising, PPC services " />
<?php include "../includes/common-css.php" ?>

</head>
<body>
<div class=site_wrapper>
<?php include "../includes/menu-home.php" ?>
<div class=clearfix></div>



<div class=clearfix></div>
<div class="feature_section444">
<div class="container">

    <h1 class="less6">BUILD YOUR BRAND USING <br>SOCIAL MEDIA! <em>Social media marketing allows your business to enhance its brand and reach both new and existing customers on media channels such as Facebook, Instagram, Twitter, LinkedIn, Pinterest, and Snapchat.</em></h1>
    
    <a href="/contact.html?utm_source=click&utm_medium=button-click&utm_campaign=social_media_quote">Request Quote</a> 
    
</div>
</div>
    
 <div class="feature_section80">
<div  class="container">

    <div class="arrow_box">
        <h1>WHAT IS SOCIAL MEDIA MARKETING?</h1>
        <p class="big_text1 less1">Social Media Markeing is the best way to increase brand visibility and improve customer engagement.</p><br>
        <p class="bigtfont less10">
This form of digital marketing focuses on the social sharing of content, images, and video with the intent of serving your brand’s marketing goals. Bullseye’s highly trained team of social media experts will use our marketing stack to leverage data and insights to determine the types of content your audience will respond to and the channels in which they are most likely to engage.</p>
          <div class="margin_top5"></div>
  
    </div>
  
</div>
</div>

<div class=clearfix></div>

  




<div class=clearfix></div>

<div class=divider_line23></div>

  <div class="feature_section64">
<div class="container">

    <h2>Social Media Marketing</h2>
    <b>Our Social Media Marketing Services include
</b>
    
    <div class="one_fourth">
    <img src="/images/social2.jpg" alt="" />
    <h4>Social Media Planning</h4>
    </div>
    
    <div class="one_fourth"> <img src="/images/fb.jpg" alt="" />
    <h4>Facebook Marketing</h4>
    </div>
    
    <div class="one_fourth">
    <img src="/images/fb-paid.jpg" alt="" />
    <h4>Paid ads on Facebook</h4>
    </div>
    
      <div class="one_fourth last">
    <img src="/images/link.jpg" alt="" />
    <h4>LinkedIn Marketing</h4>
    </div>
    
   

</div>
</div>

<div class="clearfix"></div>
<div class="feature_section89">
<div class="container">

    <div class="one_half">
    
     <h2>Social Media Overview</h2>   <p class="bigtfont">Social media marketing is not about creating Facebook business page and Twitter account. Social media marketing requires careful planning and implementation.</p>
 <div class="clearfix margin_top2"></div>
 <p class="bigtfont">Our social media marketing strategy will come to life through innovative content, imagery, and creative storytelling. Then we’ll deploy it and listen for social media mentions using sophisticated tools. </p>
            
        </div>


	<div class="one_half last">
    	
        <ul class="list_divlines">		
        
        <h3>Our Socia Media Marketing Services</h3>
            <li> <i class="fa fa-check "></i> Social Media Consulting</li>  
                <li> <i class="fa fa-check "></i> SMO Project Management</li>
                <li> <i class="fa fa-check"></i> Socia Media Marketing Audit</li>  
                <li> <i class="fa fa-check"></i> Social Media Planning & Setup</li>
             <li> <i class="fa fa-check"></i> Socia Media Campaign Management</li>
             <li> <i class="fa fa-check"></i> Social Media Marketing Training</li>
		</ul>
        
    </div>

</div>
</div>
<div class=clearfix></div>
    
<?php include "../includes/test.php" ?>
<div class=clearfix></div>
<?php include "../includes/footer.php" ?>
<a href=# class=scrollup>Scroll</a>
</div>
<?php include "../includes/common-js.php" ?>
<?php include "../includes/ga.php" ?>


</body>
</html>