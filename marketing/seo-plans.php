﻿<!doctype html>

<html lang="en-gb" class="no-js">

<head>
<title>SEO service plans | How much does SEO cost in India?</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php include "../includes/common-css.php" ?>

</head>

<body>
<div class="site_wrapper">

<?php include "../includes/menu-seo.php" ?>

<div class="clearfix"></div>

<div class="page_title2">
<div class="container">
	
    <h1>SEO Plans</h1>
    <div class="pagenation">&nbsp;<a href="/index.html">Home</a> <i>/</i>   <a href="/seo/index.html">SEO</a></div>
     
</div>
</div>

<div class="clearfix"></div>


<div class="feature_section80">
<div  class="container">

    <div class="arrow_box">
        <h1 class="white less9"><strong>Latest SEO Plans </strong></h1>
        <p class="bigtfont less10">We offer unique SEO plans depending on your business needs. We have created 3 major classification of SEO plans to meet your business goals and budget</p>
    </div>
  
</div>
</div>


<div class="clearfix"></div>



  

  <div class="feature_section62">
<div class="container">
	
	<h2>our pricing</h2>
    
    <div class="one_third">
    <div class="title">
      <h4>Standard SEO</h4></div>
    <ul>
      <li><i class="fa  fa-check"></i>Suitable for Small website</li>
       <li><i class="fa  fa-check"></i>SEO Audit : Basic</li>
      <li><i class="fa  fa-check"></i>Keywords : upto 5</li>
      <li><i class="fa  fa-check"></i>Reporting : Basic</li>
      <li><i class="fa   fa-check"></i>On Page Optimization</li>
      <li><i class="fa   fa-check"></i>Off-page Optimizatoin</li>
      <li><i class="fa  fa-check"></i>Landing page : 3</li>
      <li><i class="fa  fa-check"></i>Reporting : Basic</li>
      <li><i class="fa   fa-check"></i>On Page Optimization</li>
      <li><i class="fa   fa-check"></i>Off-page Optimizatoin</li>
      <li><i class="fa  fa-close"></i>Content Writing</li>
       <li><i class="fa  fa-close"></i>Conversion Tracking</li>
      <li><i class="fa  fa-close"></i>Advanced Link Building</li>
      <li><i class="fa  fa-close"></i>Blogging</li>
      <li><i class="fa   fa-close"></i>Keywords Tracking Report</li>
      <li><i class="fa   fa-close"></i>Dedicated Manager</li>
      
    </ul>
    <div class="clearfix margin_bottom2"></div><a href="#" class="button_2">Sign Up Now</a>
    </div>
    
    <div class="one_third">
    <div class="title"><h4>Basic SEO</h4></div>
    <ul>
       <li><i class="fa  fa-check"></i>Suitable for Small website</li>
       <li><i class="fa  fa-check"></i>SEO Audit : Basic</li>
      <li><i class="fa  fa-check"></i>Keywords : upto 10</li>
      <li><i class="fa  fa-check"></i>Reporting : Basic</li>
      <li><i class="fa   fa-check"></i>On Page Optimization</li>
      <li><i class="fa   fa-check"></i>Off-page Optimizatoin</li>
      <li><i class="fa  fa-check"></i>Landing page : 4</li>
      <li><i class="fa  fa-check"></i>Reporting : Basic</li>
      <li><i class="fa   fa-check"></i>On Page Optimization</li>
      <li><i class="fa   fa-check"></i>Off-page Optimizatoin</li>
      <li><i class="fa  fa-close"></i>Content Writing</li>
       <li><i class="fa  fa-close"></i>Conversion Tracking</li>
      <li><i class="fa  fa-close"></i>Advanced Link Building</li>
      <li><i class="fa  fa-close"></i>Blogging</li>
      <li><i class="fa   fa-close"></i>Keywords Tracking Report</li>
      <li><i class="fa   fa-close"></i>Dedicated Manager</li>
    </ul>
    <div class="clearfix margin_bottom2"></div><a href="#" class="button_2">Sign Up Now</a>
    </div>
    
    <div class="one_third last">
    <div class="title"><h4>Advanced SEO</h4></div>
    <ul>
       <li><i class="fa  fa-check"></i>Suitable for Small website</li>
       <li><i class="fa  fa-check"></i>SEO Audit : Basic</li>
      <li><i class="fa  fa-check"></i>Keywords : upto 15</li>
      <li><i class="fa  fa-check"></i>Reporting : Basic</li>
      <li><i class="fa   fa-check"></i>On Page Optimization</li>
      <li><i class="fa   fa-check"></i>Off-page Optimizatoin</li>
      <li><i class="fa  fa-check"></i>Landing page : 3</li>
      <li><i class="fa  fa-check"></i>Reporting : Basic</li>
      <li><i class="fa   fa-check"></i>On Page Optimization</li>
      <li><i class="fa   fa-check"></i>Off-page Optimizatoin</li>
      <li><i class="fa  fa-check"></i>Content Writing</li>
       <li><i class="fa  fa-check"></i>Conversion Tracking</li>
      <li><i class="fa  fa-check"></i>Advanced Link Building</li>
      <li><i class="fa  fa-check"></i>Blogging</li>
      <li><i class="fa   fa-check"></i>Keywords Tracking Report</li>
      <li><i class="fa   fa-check"></i>Dedicated Manager</li>
    </ul>
    <div class="clearfix margin_bottom2"></div><a href="#" class="button_2">Sign Up Now</a>
    </div>

</div>
</div>

<div class="clearfix"></div>

<?php include "../includes/partner.php" ?>
  
  
<div class="clearfix"></div>
  
  
<div class="feature_section79">
<div class="container">

	
    
    <h1 class="white">We offer Free SEO Audit of your website</h1>
	
    
</div>
</div>



<div class="clearfix"></div>


<?php include"../includes/footer.php"; ?>

    
</div>


 
<?php include"../includes/common-js.php"; ?>
</body>
</html>
