<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-gb" class="no-js">
<!--<![endif]-->
<head>
<title>Thank you for Downloading Profile</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<?php include "includes/common-css.php" ?>

</head>

<body>
<div class="site_wrapper">

<!-- end Navigation Menu -->

<div class="clearfix"></div>

<!-- end page title -->

<div class="clearfix"></div>


<div class="content_fullwidth less2">
<div class="container">

	<div class="error_pagenotfound">
    	
        <!--<strong>404</strong>-->
        <br />
    	<b>Thank You for Downloading Profile</b>
        
        <em>Please follow the steps below to complete the process:</em>

        <!--<p>Try using the button below to go to main page of the site</p>-->
        
       
    	
       
        
    </div>


</div>
</div><!-- end content area -->


		
<div class="feature_section255">
<div class="container">

      <div class="clearfix margin_top3"></div>
    <div class="counters2">
    
        <div class="one_third">  <p> 1.</p> <h4>Check your e-mail inbox</h4> </div>
        
        <div class="one_third">  <p> 2.</p> <h4>Open the confirmation email</h4> </div>
        
      
        
        <div class="one_third last"> <p> 3.</p> <h4>Click the confirmation link</h4> </div>
        
       
    </div>
    
      
</div>
</div>
<div class="clearfix"></div>

    
</div>


<?php include "includes/common-js.php" ?>
<?php include "includes/ga.php" ?>

</body>
</html>
