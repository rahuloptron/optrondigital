<!doctype html>

<html lang=en-gb class=no-js>

<head>

<title>Latest Mobile Friendly & Responsive Website with SEO Features @ Rs.6,500.00 only</title>

<meta charset=utf-8>

<meta http-equiv=X-UA-Compatible content="IE=edge" />

<meta name=keywords content />

<meta name=description content="Get SEO Friendly & Mobile friendly website for your business @ Rs.6,500.00 only.  " />

<?php include "includes/common-css.php" ?>

</head>

<body>

<div class=site_wrapper>

<?php include "includes/menu-home.php" ?>
<div class=clearfix></div>



<div class=clearfix></div>

<div class="feature_section336">
<div class="container">

  
   <div class="one_half">
    <h2>Get Your<span style="color: #f49610"> Professional Website</span> in just Rs. 6,500/- </h2>
    <em> </em>
    <div class="clearfix margin_bottom2"></div>
    <p class="big_text1">Do you know that companies who don't have latest website miss more than 42% business opportunities</p>
   <div class="clearfix margin_bottom4"></div>
     <p class="bigtfont"> Gone are days when having a website was big thing. Now, not having a website is big thing. Not having a professional website can spoil your reputation and you will miss business opportunities.</p>
   
    </div>
       
   <div class="one_half last">
  
<img src="images/web7.png" alt=""/>

    </div>
    
   
    
</div>
</div>


<div class="clearfix"></div>
        <div class="feature_section89">
            <div class="container">
                <div class="one_half">
                    <h2>Best website design company in Mumbai</h2>
                    <div class="clearfix margin_top2"></div>
                    <p class="bigtfont">Now don't need to look for best & professional website development company. We have team of professional website developers, programmers who will work on your project.  </p>
                    <p class="big_text1">We have already delivered 100+ projects and counting. </p>
                </div>
                <div class="one_half last">
                    <h3>Website design features</h3> <br>
                    <ul class="list_divlines">
                        <li> <i class="fa fa-check "></i> 100% Mobile friendly & SEO ready</li>
                        <li> <i class="fa fa-check "></i>Fast loading websites</li>
                        <li> <i class="fa fa-check"></i> Responsive layout</li>
                        <li> <i class="fa fa-check"></i> Latest CSS and HTML code</li>
                        <li> <i class="fa fa-check"></i> Social media ready</li>
                        <li> <i class="fa fa-check"></i> Inquiry forms with auto reply</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
<div class="clearfix"></div>
        <div class="feature_section79">
            <div class="container">
                <h2 class="white light">Did you know?</h2>
                <h1 class="white">Websites can boost your business by 200% </h1>
            </div>
        </div>

<div class=divider_line23></div>

 <div class="feature_section191">
                    <div class="container">
                        <h2 class="caps"><strong>TRUSTED BY CUSTOMERS ACROSS INDUSTRIES</strong></h2>
                        <div class="clearfix margin_bottom3"></div>
                        <div class="one_third"> <i class="fa fa-user-md "></i>
                            <h4>HEALTHCARE</h4>
                            <p>Website for doctors, clinics, hospitals and pharmaceutical companies</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-shopping-cart"></i>
                            <h4>E-COMMERCE</h4>
                            <p>Start selling online using our e-commerce website development services</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-tachometer"></i>
                            <h4>MANUFACTURING</h4>
                            <p>Corporate website and B2B marketing strategy for manufacturing companies</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third"> <i class="fa fa-home"></i>
                            <h4>REAL ESTATE</h4>
                            <p>Website development &amp; Digital marketing for real estate industry</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-bus"></i>
                            <h4>TOURS AND TRAVELS</h4>
                            <p>Static or dynamic website for travel agency and tours &amp; travel business</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-car"></i>
                            <h4>AUTOMOBILE</h4>
                            <p>Portal for cars and bikes, website for selling used cars, bikes etc</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third last"> <i class="fa fa-graduation-cap"></i>
                            <h4>EDUCATION</h4>
                            <p>Website for Schools, Colleges, Training institutes, coaching classes etc. </p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-desktop"></i>
                            <h4>SOFTWARE &amp; IT</h4>
                            <p>Mobile friendly website, marketing solution for IT Industry</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-gift"></i>
                            <h4>FASHION AND GARMENT</h4>
                            <p>Website development &amp; promotion for fashion and garment industry</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                    </div>
                </div>



<div class="feature_section338">
<div class="container">

 <div class="one_full stcode_title7">
   <h2>Our Projects<br><span class="line"></span> </h2> 
        
       
    
    </div>

  <div class="one_third">

<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work1.png" alt="">
                </div>
                <h6 class="case-item__title">Revital Trichology </h6>
              </div>
               
</div>


<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/enjay-1.png" alt="">
                </div>
                <h6 class="case-item__title">Enjay World</h6>
              </div>
           
</div>


<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work2.png" alt="">
                </div>
                <h6 class="case-item__title">Rohm Computers</h6>
              </div>
            
</div>



<div class="clearfix margin_bottom4"></div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work3.png" alt="">
                </div>
                <h6 class="case-item__title">Grecells</h6>
              </div>
               
</div>


<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/bla.png" alt="">
                </div>
                <h6 class="case-item__title">Black Pepper Exhibitions</h6>
              </div>
             
</div>


<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work5.png" alt="">
                </div>
                <h6 class="case-item__title">Genesis Telecom</h6>
              </div>
            
</div>



<div class="clearfix margin_bottom4"></div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work6.png" alt="">
                </div>
                <h6 class="case-item__title">AG Digitas</h6>
              </div>
              
</div>


<div class="one_third">
 


<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/work10.png" alt="">
                  </div>
                  <h6 class="case-item__title">Sastadeals </h6>
                </div>
             
</div>


<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/vir.png" alt="">
                </div>
                <h6 class="case-item__title">Dr Viral Gada</h6>
              </div>
             
</div>
 

<div class="clearfix margin_bottom4"></div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="../images/aj.png" alt="">
                  </div>
                  <h6 class="case-item__title">AJ Films</h6>
                </div>
               
</div>


<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/tech.png" alt="">
                  </div>
                  <h6 class="case-item__title">Techsofya</h6>
                </div>
             
</div>


<div class="one_third last">

<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work7.png" alt="">
                </div>
                <h6 class="case-item__title">Shakti Enterprises</h6>
              </div>
           
</div>

<div class="clearfix margin_bottom4"></div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="../images/par1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Prarambh Securites</h6>
                </div>
                
</div>


<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/wc.png" alt="">
                  </div>
                  <h6 class="case-item__title">Welcome Cineplex</h6>
                </div>
              
</div>


<div class="one_third last">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/work8.png" alt="">
                  </div>
                  <h6 class="case-item__title">920pm</h6>
                </div>
             
</div>

<div class="clearfix margin_bottom4"></div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="../images/rev.png" alt="">
                  </div>
                  <h6 class="case-item__title">Revelation events</h6>
                </div>
                
</div>


<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/lap.png" alt="">
                  </div>
                  <h6 class="case-item__title">Latest Movies</h6>
                </div>
              
</div>


<div class="one_third last">
<a href="website-designing/inquiry">
<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/a.png" alt="">
                  </div>
                  <h6 class="case-item__title">Make your Website here</h6>
                </div>
              </a>
             
</div>








</div>
</div>


<div class="feature_sec93">
<div class="container">
  
    
       <div class="one_third">
    
      <div>
            
              <div class="peoplesays">
                
                  We got 70% increase in organic traffic in 3 months, that's amazing. I would definitely recommend them to anyone interested in Search Engine Optimization.
                </div>
                
                <div class="peoimg"><img src="../images/5.png" alt=""> <strong>- Limesh Parekh</strong></div>
                   
            </div>
        
    </div>
     <div class="one_third">
    
      <div>
            
              <div class="peoplesays">
                
                  we got our new website from optron and they also promoted our website so we got 17 inquiries in 3 days
                </div>
                
                <div class="peoimg"><img src="../images/5.png" alt=""> <strong>- Anurag Sharma</strong></div>
                   
            </div>
        
    </div>
    <div class="one_third last">
    
      <div>
            
              <div class="peoplesays">
                
                  We recently got our new mobile friendly website from optron, Now i am getting faster response for Inquiries. 
                </div>
                
                <div class="peoimg"><img src="../images/5.png" alt=""> <strong>- Vishal Waghmare</strong></div>
                   
            </div>
        
    </div>
     
</div>
</div>
<!-- <div class="feature_section629">
    <div class="container">
      <h3 class="white caps">BOOST YOUR BUSINESS WITH OUR DIGITAL MARKETING SERVICES </h3>
      <a href="inquiry" class="button one">Contact Us</a> </div>
  </div> -->
<div class="feature_section265">
<div class="container">

    <div class="one_half">
    
    <h3>More Servies by OPTRON Digital</h3> <br>
                    <ul class="list_divlines">
                        <li> <i class="fa fa-check "></i> E-Commerce Website Development</li>
                        <li> <i class="fa fa-check "></i>SEO Services and Google AdWords</li>
                        <li> <i class="fa fa-check"></i> Social Media Promotion & Branding</li>
                        <li> <i class="fa fa-check"></i> Facebook Marketing & Facebook Ads</li>
                        <li> <i class="fa fa-check"></i> SEO Friendly Content Writing </li>
                        <li> <i class="fa fa-check"></i> YouTube video Promotion</li>
                        <li> <i class="fa fa-check"></i> Lead Generation for B2B / B2C</li>
                    </ul>
 <div class="clearfix margin_top2"></div>
<!--   <img src="images/t2.png" alt=""/>  -->
        </div>

<div class="one_half last">
      
<script type="text/javascript" src="//static.mailerlite.com/data/webforms/446663/u0p2r9.js?v3"></script>

    </div>
  

</div>
</div>


 <div class="feature_section665">
 <div class="container">
 
 <h1>Welcome to Highstand</h1>
 <div class="linebg"></div>
 
    <div class="clearfix margin_bottom9"></div>
 
    <div class="one_fourth_less">
       <div class="box">
        <i class="fa fa-tablet"></i>
            <h2>Responsive Theme<b>perfect in any device.</b></h2>
        <div class="bgline"></div><p>Lorem Ipsum gen erators the Internet tend to repeat pre</p>
        <a class="btn linebtn one" href="#">Read more </a>
      </div>
    </div>
     
      
      
      <div class="one_fourth_less">
       <div class="box">
         <i class="fa fa-paper-plane-o"></i>
          <h2>30+ Templates<b>perfect in any device.</b></h2>
        <div class="bgline"></div><p>Lorem Ipsum gen erators the Internet tend to repeat pre</p>
        <a class="btn linebtn one" href="#">Read more</a>
      </div>
    </div>
      
      
     <div class="one_fourth_less">
       <div class="box">
         <i class="fa fa-tags"></i>
          <h2>Multi Purpose<b>perfect in any device.</b></h2>
        <div class="bgline"></div><p>Lorem Ipsum gen erators the Internet tend to repeat pre</p>
        <a class="btn linebtn one" href="#">Read more</a>
      </div>
    </div>
   
      
      
     <div class="one_fourth_less last">
       <div class="box">
         <i class="fa fa-star-o"></i>
          <h2>Tons of Features<b>perfect in any device.</b></h2>
        <div class="bgline"></div><p>Lorem Ipsum gen erators the Internet tend to repeat pre</p>
        <a class="btn linebtn one" href="#">Read more</a>
      </div>
    </div>
     
      
 </div>
 </div>   
   
<div class="clearfix"></div>



<div class=divider_line23></div>



<div class=clearfix></div>
<div class=feature_section14>
<div class=container>
<h2 class="caps">Our Trusted Partners</h2>
<div class="clearfix margin_bottom3"></div>
<ul>
<li> <img src=images/google2.png alt=""/></li>
<li> <img src=images/fb1.png alt=""/></li>
<li> <img src=images/youtube.png alt=""/></li>
<li> <img src=images/twitter.jpg alt=""/></li>
<li> <img src=images/google-analytics.png alt=""/></li>
<li class=last> <img src=images/ppc1.png alt=""/></li>
</ul>
</div>
</div>

<div class="feature_section629">
    <div class="container">
      <h3 class="white caps">BOOST YOUR BUSINESS WITH OUR DIGITAL MARKETING SERVICES </h3>
      <a href="inquiry" class="button one">Contact Us</a> </div>
  </div>


<div class=clearfix></div>
<div class="feature_section_contact">
<div class="container">

    <div class="box1"> <i class="fa fa-mobile-phone"></i>
    <h4 class="caps">Phone Number<b>+91 9833189090</b></h4></div>
    
    <div class="box2"> <i class="fa fa-envelope-o"></i>
    <h4 class="caps">Email Address<b>bhavesh@optron.in</b></h4></div>
    
    <div class="box3"> <i class="fa fa-map-marker"></i>
    <h4 class="caps">Location Address<b>217, Accord Classics, Station Road,
     Goregaon East, Mumbai, 400063</b></h4></div>
    
   <!--  <div class="box4">
      <div class="logo"><a href="/index.html" id="logo"></a></div>
   </div> -->

</div>    
</div>
<div class="copyright_info2">
<div class="container">

  <div class="clearfix"></div>
    
    <div class="one_half">
    
        Copyright © 2017 OPTRON All rights reserved.  <a href="/terms.html" rel="nofollow">Terms of Use</a> | <a href="/privacy-policy.html" rel="nofollow">Privacy Policy</a>
        
    </div>
    
    <div class="one_half last">
        
      
    </div>
    
</div>
</div>  
<a href=# class=scrollup>Scroll</a>
</div>
<?php include "includes/common-js.php" ?>

<?php include "includes/ga.php" ?>


</body>
</html>