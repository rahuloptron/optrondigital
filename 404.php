﻿<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-gb" class="no-js">
<!--<![endif]-->
<head>
<title>HighStand - Responsive MultiPurpose HTML5 Template</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />

<!-- Favicon -->
<link rel="shortcut icon" href="images/favicon.png">

<!-- this styles only adds some repairs on idevices  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- Google fonts - witch you want to use - (rest you can just remove) -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>

<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<!-- ######### CSS STYLES ######### -->

<link rel="stylesheet" href="css/reset.css" type="text/css" />
<link rel="stylesheet" href="css/style.css" type="text/css" />

<!-- font awesome icons -->
<link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">

<!-- simple line icons -->
<link rel="stylesheet" type="text/css" href="css/simpleline-icons/simple-line-icons.css" media="screen" />

<!-- animations -->
<link href="js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />

<!-- responsive devices styles -->
<link rel="stylesheet" media="screen" href="css/responsive-leyouts.css" type="text/css" />

<!-- shortcodes -->
<link rel="stylesheet" media="screen" href="css/shortcodes.css" type="text/css" />

<!-- style switcher -->
<link rel = "stylesheet" media = "screen" href = "js/style-switcher/color-switcher.css" />

<!-- mega menu -->
<link href="js/mainmenu/bootstrap.min.css" rel="stylesheet">
<link href="js/mainmenu/demo.css" rel="stylesheet">
<link href="js/mainmenu/menu.css" rel="stylesheet">

<link rel="stylesheet" href="js/searchbox/overlay.css">

<!-- tabs -->
<link rel="stylesheet" type="text/css" href="js/tabs/assets/css/responsive-tabs3.css">

</head>

<body>
<div class="site_wrapper">

<!-- end Navigation Menu -->

<div class="clearfix"></div>

<!-- end page title -->

<div class="clearfix"></div>


<div class="content_fullwidth less2">
<div class="container">

	<div class="error_pagenotfound">
    	
        <!--<strong>404</strong>-->
        <br />
    	<b>Thank you for Subscribe & Check your Email Inbox</b>
        
        <em>We have sent you details about course</em>

        <p>Try using the button below to go to main page of the site</p>
        
        <div class="clearfix margin_top3"></div>
    	
        <a href="index.html" class="but_medium1"><i class="fa fa-arrow-circle-left fa-lg"></i>&nbsp; Go to Back</a>
        
    </div>


</div>
</div><!-- end content area -->



<div class="clearfix"></div>


<footer class="footer">

<div class="container">

    <div class="one_fourth">
    <div class="qlinks">
    
    	<h4>Useful Links</h4>
        
        <ul>
            <li><a href="#"><i class="fa fa-angle-right"></i> Home Page Variations</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Awsome Slidershows</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Features and Typography</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Different &amp; Unique Pages</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Single and Portfolios</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Recent Blogs or News</a></li>
            <li><a href="#"><i class="fa fa-angle-right"></i> Layered PSD Files</a></li>
        </ul>
        
    </div>
	</div><!-- end links -->
    
    <div class="one_fourth">
    
    <h4>Blog Posts</h4>
    <div class="fbposts">
    
        <a href="#"><img src="http://placehold.it/65x45" alt="" /> Web page editors uses default model texts.</a>
        
        <a href="#"><img src="http://placehold.it/65x45" alt="" /> Web page editors uses default model texts.</a>
        
        <a href="#"><img src="http://placehold.it/65x45" alt="" /> Web page editors uses default model texts.</a>
           
    </div><!-- end footer blogs -->
            	    
	</div><!-- end address -->
    
        
    <div class="one_fourth">
        
    <h4>Latest Tweets</h4>
    
    	<ul class="twitter_feeds">
        
        	<li class="bhline"><i class="fa fa-twitter fa-lg"></i> <a href="https://twitter.com/gsrthemes9" target="_blank">gsrthemes9</a>: Hoxa - Responsive html5 Professional Theme
			<em>.9 days ago &nbsp;.<a href="#">reply</a> &nbsp;.<a href="#">retweet</a> &nbsp;.<a href="#">favorite</a></em></li>
            
            <li><i class="fa fa-twitter fa-lg"></i> <a href="https://twitter.com/gsrthemes9" target="_blank">gsrthemes9</a>: elos - Responsive HTML5 / CSS3, Simple, Clean and Professional Multipurpose Use.
			<em>.12 days ago &nbsp;.<a href="#">reply</a> &nbsp;.<a href="#">retweet</a> &nbsp;.<a href="#">favorite</a></em></li>
            
        </ul>
        
    </div><!-- end site info -->
    
    <div class="one_fourth last">
        
        <h4>Address Info</h4>
        
        <ul class="faddress">
            <li><i class="fa fa-map-marker fa-lg"></i>&nbsp; 2901 Marmora Road, Glassgow,<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Seattle, WA 98122-1090</li>
            <li><i class="fa fa-phone"></i>&nbsp; 1 -234 -456 -7890</li>
            <li><i class="fa fa-print"></i>&nbsp; 1 -234 -456 -7890</li>
            <li><a href="mailto:info@yourdomain.com"><i class="fa fa-envelope"></i>&nbsp; info@yourdomain.com</a></li>
        </ul>
        
    </div><!-- end about -->

 </div><!-- end footer -->

<div class="clearfix"></div>

<div class="copyright_info inner">
<div class="container">

	<div class="clearfix"></div>
    
    <div class="one_half">
    
        Copyright © 2016 HighStand.com. All rights reserved.  <a href="#">Terms of Use</a> | <a href="#">Privacy Policy</a>
        
    </div>
    
    <div class="one_half last">
        
        <ul class="footer_social_links">
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-linkedin"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-skype"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-flickr"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-html5"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-youtube"></i></a></li>
            <li class="animate" data-anim-type="zoomIn"><a href="#"><i class="fa fa-rss"></i></a></li>
        </ul>
            
    </div>
    
</div>
</div><!-- end copyright info -->


</footer>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->


    
</div>


<!-- ######### JS FILES ######### --> 
<!-- get jQuery used for the theme --> 
<script type="text/javascript" src="js/universal/jquery.js"></script>
<script src="js/style-switcher/styleselector.js"></script>
<script src="js/animations/js/animations.min.js" type="text/javascript"></script>
<script src="js/mainmenu/bootstrap.min.js"></script> 
<script src="js/mainmenu/customeUI.js"></script> 
 
<script type="text/javascript" src="js/mainmenu/modernizr.custom.75180.js"></script>
<script src="js/masterslider/jquery.easing.min.js"></script>

<!-- search box --> 
<script src="js/searchbox/overlay.js"></script>
<script>
  $(document).ready(function() {
	$('.overlay').overlay();
  });
</script>

<script src="js/tabs/assets/js/responsive-tabs.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/universal/custom.js"></script>

</body>
</html>
