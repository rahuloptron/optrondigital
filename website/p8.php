
<!doctype html>
<html lang="en-gb" class="no-js"> <!--<![endif]--><head>
	<title>Our clients</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

    <!-- Favicon --> 
	<link rel="shortcut icon" href="../images-3/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    

    <link rel="stylesheet" href="../css-3/reset.css" type="text/css" />
	<link rel="stylesheet" href="../css-3/style.css" type="text/css" />
    
    <!-- font awesome icons -->
    <link rel="stylesheet" href="../css-3/font-awesome/css/font-awesome.min.css">
	
    <!-- simple line icons -->
	<link rel="stylesheet" type="text/css" href="../css-3/simpleline-icons/simple-line-icons.css" media="screen" />
        
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="../css-3/responsive-leyouts.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="../css-3/shortcodes.css" type="text/css" /> 

    <!-- mega menu -->
    <link href="../js-3/mainmenu/bootstrap.min.css" rel="stylesheet">
	<link href="../js-3/mainmenu/demo.css" rel="stylesheet">
	<link href="../js-3/mainmenu/menu.css" rel="stylesheet">

</head>

<body>

<div class="site_wrapper">

<header class="header">
 
    <div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="index.html" id="logo"></a></div>
        
    <!-- Navigation Menu -->
    <div class="menu_main">
    
      <div class="navbar yamm navbar-default">
        
          <div class="navbar-header">
            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  >
              <button type="button" > <i class="fa fa-bars"></i></button>
            </div>
          </div>
          
          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
          
            <?php include '../includes-3/menu.php' ?>
            
          </div>
        
      </div>
    </div>
   
    </div>
    
</header>

<div class="clearfix"></div>
<div class="page_title1 sty8">
<div class="container">

    <h1>Our Clients</h1>
 
</div>      
</div>
      
<div class="client_logos">
<div class="container">

<div class="one_fourth">
    
    	<img src="../images-3/1.png" alt=""/>
       
    </div>
    
    <div class="one_fourth">
    
       <img src="../images-3/GRE@CELLS.jpg" alt=""/>
        
    </div>

    <div class="one_fourth">
    
       <img src="../images-3/1.png" alt=""/>
        
    </div>
    
    <div class="one_fourth last">
    
       <img src="../images-3/1.png" alt=""/>
    
    </div>
    
 <div class="clearfix margin_top3"></div>
     

     <div class="one_fourth">
    
    	<img src="../images-3/1.png" alt=""/>
       
    </div>
    
    <div class="one_fourth">
    
        <img src="../images-3/1.png" alt=""/>
        
    </div>

    <div class="one_fourth">
    
       <img src="../images-3/1.png" alt=""/>
        
    </div>
    
    <div class="one_fourth last">
    
        <img src="../images-3/1.png" alt=""/>
    
    </div>
    
     <div class="clearfix margin_top3"></div>
         
  <div class="one_fourth">
    
    	<img src="../images-3/1.png" alt=""/>
       
    </div>
    
    <div class="one_fourth">
    
        <img src="../images-3/1.png" alt=""/>
        
    </div>

    <div class="one_fourth">
    
       <img src="../images-3/1.png" alt=""/>
        
    </div>
    
    <div class="one_fourth last">
    
        <img src="../images-3/1.png" alt=""/>
    
    </div>

    <div class="clearfix margin_top3"></div>
     

     <div class="one_fourth">
    
        <img src="../images-3/1.png" alt=""/>
       
    </div>
    
    <div class="one_fourth">
    
        <img src="../images-3/1.png" alt=""/>
        
    </div>

    <div class="one_fourth">
    
       <img src="../images-3/1.png" alt=""/>
        
    </div>
    
    <div class="one_fourth last">
    
        <img src="../images-3/1.png" alt=""/>
    
    </div>

</div>
</div>
    
    <div class="content_fullwidth">

<div class="container">
    
	<div class="one_third">
		<div class="pricingtable3">
        	<ul>
        
                <li>Aashray Infratech Solutions</li>
                <li class="bg">Adam fabriwerk</li>
                <li>AG Digitas</li>
                <li class="bg">Blackpepper exhibitions</li>
                <li>Dr. Viral gada</li>
                <li class="bg">Enjay World </li>
                <li>Grecells </li>
                <li class="bg">Janitor data bombay pvt ltd</li>
                
            </ul>
    	</div>
	</div><!-- end section -->
    
	<div class="one_third">
		<div class="pricingtable3">
        	<ul>
        
                <li>Neotech</li>
                <li class="bg">Prarambh Securities</li>
                <li>Revelation events</li>
                <li class="bg">Revital Trichology</li>
                <li>Rohm Computers</li>
                <li class="bg">Shakti Enterprise</li>
                <li>Shopx</li>
                <li class="bg">Trans it</li>
               
            </ul>
    	</div>
	</div><!-- end section -->
    
    <div class="one_third last">
		<div class="pricingtable3">
        	<ul>
            
                <li>Transparent shares and securities</li>
                <li class="bg">Vijay engineering</li>
                <li>Wits interactive</li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
               
            </ul>
    	</div>
	</div>
    

</div>

</div>
    

<div class="clearfix"></div>


<?php include "../includes-3/footer.php" ?><!-- end footer -->


<div class="clearfix"></div>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->


</div>

<script type="text/javascript" src="../js-3/universal/jquery.js"></script>
<script src="../js-3/mainmenu/bootstrap.min.js"></script> 
<script src="../js-3/mainmenu/customeUI.js"></script>
<script src="../js-3/scrolltotop/totop.js" type="text/javascript"></script>
<script type="text/javascript" src="../js-3/mainmenu/sticky.js"></script>


<script type="text/javascript" src="../js-3/universal/custom.js"></script>

</body>
</html>
