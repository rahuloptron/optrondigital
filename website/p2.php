<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]--><head>
	<title>Optron Website Designing</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

    
    <!-- Favicon --> 
    <link rel="shortcut icon" href="../images-3/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
    
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
	
    <link rel="stylesheet" href="../css-3/reset.css" type="text/css" />
	<link rel="stylesheet" href="../css-3/style.css" type="text/css" />
    
    <!-- font awesome icons -->
    <link rel="stylesheet" href="../css-3/font-awesome/css/font-awesome.min.css">
	
    <!-- simple line icons -->
	<link rel="stylesheet" type="text/css" href="../css-3/simpleline-icons/simple-line-icons.css" media="screen" />
        
    <!-- animations -->
    <link href="../js-3/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="../css-3/responsive-leyouts.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="../css-3/shortcodes.css" type="text/css" /> 


<!-- just remove the below comments witch color skin you want to use -->
    <!--<link rel="stylesheet" href="css/colors/blue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/green.css" />-->
    <!--<link rel="stylesheet" href="css/colors/cyan.css" />-->
    <!--<link rel="stylesheet" href="css/colors/orange.css" />-->
    <!--<link rel="stylesheet" href="css/colors/lightblue.css" />-->
    <!--<link rel="stylesheet" href="css/colors/pink.css" />-->
    <!--<link rel="stylesheet" href="css/colors/purple.css" />-->
    <!--<link rel="stylesheet" href="css/colors/red.css" />-->
    <!--<link rel="stylesheet" href="css/colors/slate.css" />-->
    <!--<link rel="stylesheet" href="css/colors/yellow.css" />-->
    <!--<link rel="stylesheet" href="css/colors/darkred.css" />-->

<!-- just remove the below comments witch bg patterns you want to use --> 
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-default.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-one.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-two.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-three.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-four.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-five.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-six.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-seven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eight.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-nine.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-ten.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-eleven.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-twelve.css" />-->
    <!--<link rel="stylesheet" href="css/bg-patterns/pattern-thirteen.css" />-->

    
    <!-- mega menu -->
    <link href="../js-3/mainmenu/bootstrap.min.css" rel="stylesheet">
	<link href="../js-3/mainmenu/demo.css" rel="stylesheet">
	<link href="../js-3/mainmenu/menu.css" rel="stylesheet">
	
	<!-- owl carousel -->
    <link href="../js-3/carouselowl/owl.transitions.css" rel="stylesheet">
    <link href="../js-3/carouselowl/owl.carousel.css" rel="stylesheet">
    
    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="../js-3/accordion/style.css" />
    
    <!-- tabs 2 -->
    <link href="../js-3/tabs2/tabacc.css" rel="stylesheet" />
	<link href="../js-3/tabs2/detached.css" rel="stylesheet" />
    
</head>

<body>

<div class="site_wrapper">


<header class="header">
 
	<div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="index.html" id="logo"></a></div>
		
	<!-- Navigation Menu -->
    <div class="menu_main">
    
      <div class="navbar yamm navbar-default">
        
          <div class="navbar-header">
            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  >
              <button type="button" > <i class="fa fa-bars"></i></button>
            </div>
          </div>
          
          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
          
            <?php include '../includes-3/menu.php' ?>
            
          </div>
        
      </div>
    </div>
	<!-- end Navigation Menu -->
    
    
	</div>
    
</header>


<div class="clearfix"></div>

<div class="page_title1 sty7">

    <h1>Leading website development Company.</h1>
    <h3>Whether you are looking for a website for your company or digital marketing services to grow your business; we can provide you complete solutions from website development to business promotion, lead generation services under one roof.</h3>
     <br><a href="/services/index.html" class="but_large2"><strong>&nbsp; Get Started</strong></a>
    
</div>

<div class="clearfix"></div>

<div class="clearfix"></div>
<div class="feature_section10">
<div class="container">
	
    <div class="one_fifth_less">
    <div class="box">
        <h1 class="sitecolor"><strong>.biz</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$4.50</strong><em>/year</em></h1>
    </div>
    </div>
    
    <div class="one_fifth_less">
    <div class="box">
        <h1 class="sitecolor"><strong>.club</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$7.95</strong><em>/year</em></h1>
    </div>
    </div>
    
    <div class="one_fifth_less">
    <div class="box">
        <h1 class="sitecolor"><strong>.wales</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$9.75</strong><em>/year</em></h1>
    </div>
    </div>
    
    <div class="one_fifth_less">
    <div class="box">
        <h1 class="sitecolor"><strong>.co.uk</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$18.25</strong><em>/year</em></h1>
    </div>
    </div>
    
    <div class="one_fifth_less last">
    <div class="box">
        <h1 class="sitecolor"><strong>.mobi</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$12.99</strong><em>/year</em></h1>
    </div>
    </div>

</div>
</div><!-- end featured section 10 -->


<div class="clearfix"></div>

<div class="feature_section12-1">
<div class="container">
    
    <div class="one_half">
    
    	<img src="../images-3/site-img20.jpg" alt="">
	</div><!-- end section -->
    
    <div class="one_half last">
    <div class="one_half">
        <i class="fa fa-check"></i>
        <h4 class="light">Easy Domain Setup No Technical Skills</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        </div>
    <div class="one_half last">
    	<i class="fa fa-check"></i>
        <h4 class="light">FREE Domain Privacy Protection</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Unlimited Sub Domains</h4>

        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        </div>
	</div><!-- end section -->
    
</div>
</div><!-- end featured section 12 -->


<div class="clearfix"></div>
<div class="feature_section7">
<div class="container">

    <h1 class="caps"><strong>All Our plans include</strong></h1>
    
    <div class="clearfix margin_bottom2"></div>

    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-globe"></i> Free<br>domain</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-database"></i> Unlimited<br>Database</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-wordpress"></i> 1-click<br>install apps</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-rocket"></i> 99.9% Uptime<br>Guarantee</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less last">
        <h5 class="caps"><i class="fa fa-coffee"></i> easy-to-use<br>control panel</h5>
    </div><!-- end -->
    

    <div class="clearfix margin_bottom3"></div>
    
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-building-o"></i> Free<br>SiteBuilder</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-lock"></i> SSL<br>Certificates</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-paper-plane-o"></i> Transfer<br>Website &amp; Domain</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-money"></i> 30 Day<br>Money Back</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less last">
        <h5 class="caps"><i class="fa fa-headphones"></i> 24/7/365<br>Premium Support</h5>
    </div><!-- end -->
    
</div>
</div>

<div class="clearfix"></div>


<div class="client_logos">
                    <div class="container">
                        <h2 class="caps"><strong>Our portfolio</strong></h2>
                        <div class="margin_top1"></div>
                        <div class="clearfix"></div>
                        <div id="owl-demo" class="owl-carousel">
                            <div class="item"><img src="/images-3/sliders/newindexviral.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexgrecells.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexag.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexshakti.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexenjay.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexrohm.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexrevital.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexaashrayaa.png" alt="" /></div>
                        </div>
                        <!-- end section -->
                    </div>
</div>


<div class="clearfix"></div>

<div class="feature_section8">
<div class="container">
    
    <h2 class="caps white"><strong>what our customers say</strong></h2>
    
    <div class="clearfix margin_bottom2"></div>
    
    <div id="owl-demo13" class="owl-carousel">
                    
        <div class="slidesec">
            <div class="imgbox one"><img src="../images-3/site-img20.jpg" alt="" /></div>
            <h4>- Mark Jones Computer Services</h4>
            <p class="bigtfont">Purchase the Template a more recently pagemaker include versions tortorhas survived not only five centuries making this the first true randomised words which generator on the tend to repeat predefined chunks as predefined chunks as randomised necessary.</p>
            <br />
            <strong>website:</strong> www.markjones.com
            <br />
            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
        </div><!-- end a slide -->
        
        
        <div class="slidesec">
            <div class="imgbox one"><img src="../images-3/site-img20.jpg" alt="" /></div>
            <h4>- Henry Clarence</h4>
            <p class="bigtfont">Purchase the Template a more recently pagemaker include versions tortorhas survived not only five centuries making this the first true randomised words which generator on the tend to repeat predefined chunks as predefined chunks as randomised necessary.</p>
            <br />
            <strong>website:</strong> www.henryclarence.us
            <br />
            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
        </div><!-- end a slide -->
        
        
        <div class="slidesec">
            <div class="imgbox one"><img src="../images-3/site-img20.jpg" alt="" /></div>
            <h4>- Maurice Jamar</h4>
            <p class="bigtfont">Purchase the Template a more recently pagemaker include versions tortorhas survived not only five centuries making this the first true randomised words which generator on the tend to repeat predefined chunks as predefined chunks as randomised necessary.</p>
            <br />
            <strong>website:</strong> www.mauricejamar.co.uk
            <br />
            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
        </div><!-- end a slide -->
                            
    </div>

</div>
</div><!-- end featured section 8 -->

<div class="clearfix"></div>


<div class="feature_section9">
<div class="container">

	<h1 class="caps"><strong>FAQ - Your questions? We got answers!</strong></h1>
    
    <div class="clearfix margin_bottom3"></div>
	
    <div class="one_half">
    
        <div class="box">
        	<h4 class="caps"><strong>WHAT'S A DOMAIN NAME?</strong></h4>
        	<p class="bigtfont">Many desktop publishing packages and web page editors now use many web sites still in their versions have over the years.</p>
        </div>
    	
        <div class="box">
        	<h4 class="caps"><strong>What is a domain extension?</strong></h4>
        	<p class="bigtfont">Desktop publishing packages and web page editors now use Lorem Ipsum as their default model text and a search</p>
        </div>
        
        <div class="box">
        	<h4 class="caps"><strong>Can I use my domain for email?</strong></h4>
        	<p class="bigtfont">Lorem Ipsum as their default model text and a search for will uncover many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
        <div class="box">
        	<h4 class="caps"><strong>What can I do with a domain name?</strong></h4>
        	<p class="bigtfont">Web page editors now use Lorem Ipsum as their default model text and a search for will uncover many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
	</div><!-- end section -->
    
    <div class="one_half last">
    
        <div class="box">
        	<h4 class="caps"><strong>How long does it take to register my domain?</strong></h4>
        	<p class="bigtfont">Lorem Ipsum as their default model many web sites still in their infancy versions have evolved over the years.</p>
        </div>
    	
        <div class="box">
        	<h4 class="caps"><strong>What are your Terms &amp; Conditions?</strong></h4>
        	<p class="bigtfont">Desktop publishing packages and web page editors now use Lorem Ipsum as their default model text and a search</p>
        </div>
        
        <div class="box">
        	<h4 class="caps"><strong>WHAT PAYMENT OPTIONS DO YOU OFFER?</strong></h4>
        	<p class="bigtfont">Lorem Ipsum as their default model text and a search for will uncover many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
        <div class="box">
        	<h4 class="caps"><strong>Can I sell or transfer the domain?</strong></h4>
        	<p class="bigtfont">Web page editors now use Lorem Ipsum as web sites still in their infancy their default model text and a search for will uncover many versions have evolved over the years.</p>
        </div>
        
	</div><!-- end section -->

</div>
</div><!-- end featured section 9 -->


<div class="clearfix"></div>


<div class="feature_section5">
<div class="container">


    <center><h1 class="caps"><strong>TECHNOLOGIES WE USE</strong></h1></center>
    <div class="clearfix margin_bottom1"></div>

    <div class="content">
    
        <ul>
            <li><img src="../images-3/media/scripts-logo1.jpg" alt=""></li>
            <li><img src="../images-3/media/scripts-logo2.jpg" alt=""></li>
            <li><img src="../images-3/media/scripts-logo3.jpg" alt=""></li>
            <li><img src="../images-3/media/scripts-logo4.jpg" alt=""></li>
             <li><img src="../images-3/media/scripts-logo7.jpg" alt=""></li>
            <li><img src="../images-3/media/scripts-logo6.jpg" alt=""></li>
           
          
        </ul>
        
    </div>
    

</div>
</div><!-- end featured section 5-->
<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Some <strong>Our Esteemed</strong><span> Clients</span></span></h2>
     
    </div><!-- end section title heading -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="../images/clients/adam-fabriwerk.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                 <h5><strong><a href="#">Adam Fabriwerk</a></strong></h5>
            <p>Manufactures of processing systems for pharma</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
       
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/ajfilms.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Aj Films</a></strong></h5>
            <p>Best Photography in Mumbai</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/agdigitas.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ag Digitas</a></strong></h5>
                <p>IT Solutions Company</p>
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/blacpeper.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Black Peper</a></strong></h5>
                <p>Black Pepper is an Exhibition Stall Design company</p>
            </div>
            
        </div>
    
    </div>
    
    
     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/enjay.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Enjay Solution</a></strong></h5>
                <p>Expert in CRM and Telephony Solutions across verticals</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/genesis.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Genesis Telesecure</a></strong></h5>
                <p>Genesis Telesecure LLP offers complete range of solutions</p>
            </div>
            
        </div>
    
    </div>
                      
    <div class="one_fourth">

    <div class="flips4">
    
        <div class="flips4_front flipscont4">
       <img src="../images/clients/grecelllogo.png" alt="" >
        </div>
        
        <div class="flips4_back flipscont4">
            <h5><strong><a href="#">Grecells</a></strong></h5>
            <p>Disaster recovery solutions for Windows server</p>
        </div>
        
    </div>

    </div>      
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/heminfotech.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hem Infotech</a></strong></h5>
                <p>IT Solutions and Services Company</p>

            </div>
            
        </div>
    
    </div>
    
    
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/hi-will-logo.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hi-Will Education</a></strong></h5>
                <p>Engineers classes</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/neotech.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/prarambh.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Prarambh Security</a></strong></h5>
                <p>Security Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/pratrikshainterior.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Pratiksha Interior</a></strong></h5>
                <p>Modular kitchen, Office Furniture and Roofing</p>

            </div>
            
        </div>
    
    </div>


 </div>
</div>

<div class="clearfix"></div>


<?php include '../includes-3/footer.php' ?>


<div class="clearfix"></div>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<script type="text/javascript" src="../js-3/universal/jquery.js"></script>
<script src="../js-3/style-switcher/styleselector.js"></script>
<script src="../js-3/animations/js/animations.min.js" type="text/javascript"></script>
<script src="../js-3/mainmenu/bootstrap.min.js"></script> 
<script src="../js-3/mainmenu/customeUI.js"></script>
<script src="../js-3/masterslider/jquery.easing.min.js"></script>

<script src="../js-3/scrolltotop/totop.js" type="text/javascript"></script>
<script type="text/javascript" src="../js-3/mainmenu/sticky.js"></script>
<script type="text/javascript" src="../js-3/mainmenu/modernizr.custom.75180.js"></script>
<script type="text/javascript" src="../js-3/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="../js-3/cubeportfolio/main.js"></script>

<script src="../js-3/aninum/jquery.animateNumber.min.js"></script>
<script src="../js-3/carouselowl/owl.carousel.js"></script>

<script type="text/javascript" src="../js-3/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="../js-3/accordion/custom.js"></script>

<script type="text/javascript" src="../js-3/universal/custom.js"></script>

</body>
</html>
