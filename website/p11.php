<!doctype html>
<!--[if IE 7 ]>    <html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en-gb" class="no-js"> <!--<![endif]--><head>
    <title>Price Table - Responsive Hosting Template</title>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    
    <!-- Favicon --> 
    <link rel="shortcut icon" href="../images-3/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
    
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    
    <!-- ######### CSS STYLES ######### -->
    
    <link rel="stylesheet" href="../css-3/reset.css" type="text/css" />
    <link rel="stylesheet" href="../css-3/style.css" type="text/css" />
    
    <!-- font awesome icons -->
    <link rel="stylesheet" href="../css-3/font-awesome/css/font-awesome.min.css">
    
    <!-- simple line icons -->
    <link rel="stylesheet" type="text/css" href="../css-3/simpleline-icons/simple-line-icons.css" media="screen" />
        
    <!-- animations -->
    <link href="../js-3/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
    <link rel="stylesheet" media="screen" href="../css-3/responsive-leyouts.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="../css-3/shortcodes.css" type="text/css" /> 

    
    <!-- mega menu -->
    <link href="../js-3/mainmenu/bootstrap.min.css" rel="stylesheet">
    <link href="../js-3/mainmenu/demo.css" rel="stylesheet">
    <link href="../js-3/mainmenu/menu.css" rel="stylesheet">
    
    <!-- owl carousel -->
    <link href="../js-3/carouselowl/owl.transitions.css" rel="stylesheet">
    <link href="../js-3/carouselowl/owl.carousel.css" rel="stylesheet">
    
    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="../js-3/accordion/style.css" />
    
</head>

<body>

<div class="site_wrapper">


<div class="clearfix"></div>


<header class="header">
 
    <div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="index.html" id="logo"></a></div>
        
    <!-- Navigation Menu -->
    <div class="menu_main">
    
      <div class="navbar yamm navbar-default">
        
          <div class="navbar-header">
            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  >
              <button type="button" > <i class="fa fa-bars"></i></button>
            </div>
          </div>
          
          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
          
          <?php include '../includes-3/menu.php' ?>
            
          </div>
        
      </div>
    </div>
    <!-- end Navigation Menu -->
    
    
    </div>
    
</header>


<div class="clearfix"></div>

<div class="page_title1 sty8">
<div class="container">



    <h1>Pricing Table</h1>
 
</div>      
</div>

<div class="clearfix"></div>


<div class="parallax_section3">
<div class="container">

    <div class="one_third">
        <div class="pricingtable3">
            <ul>
            
                <li class="title"><h3 class="white">Basic</h3></li>
                <li class="price"><h1>$12.99 <em>/Per license</em></h1> </li>
                <li class="hecont">Making this the first true generator necessary on the Internet. <br /><br /> <strong>Try Free for 30 Days</strong></li>
                <li></li>
                <li><i class="fa fa-check"></i>&nbsp; Browser protection</li>
                <li><i class="fa fa-check"></i>&nbsp; Automatic backups</li>
                <li><i class="fa fa-check"></i>&nbsp; Parental controls</li>
                <li><i class="fa fa-check"></i>&nbsp; Facebook protection</li>
                <li><i class="fa fa-check"></i>&nbsp; secured online storage</li>
                <li></li>
                <li><a href="#" class="but_small1 gray">Read more</a> &nbsp; <a href="#" class="but_small1 gray">Get Now!</a></li>
                <li></li>
                <li></li>
            
            </ul>
        </div>
    </div><!-- end section -->
    
    <div class="one_third">
        <div class="pricingtable3">
            <ul>
            
                <li class="title act"><h3 class="white">Professional</h3></li>
                <li class="price act"><h1>$27.99 <em>/Per license</em></h1> </li>
                <li class="hecont act">Making this the first true generator necessary on the Internet. <br /><br /> <strong>Try Free for 30 Days</strong></li>
                <li></li>
                <li><i class="fa fa-check"></i>&nbsp; Parental controls</li>
                <li><i class="fa fa-check"></i>&nbsp; Facebook protection</li>
                <li><i class="fa fa-check"></i>&nbsp; secured online storage</li>
                <li><i class="fa fa-check"></i>&nbsp; Advanced online identity</li>
                <li><i class="fa fa-check"></i>&nbsp; Two-Way Firewall</li>
                <li></li>
                <li><a href="#" class="but_small1">Read more</a> &nbsp; <a href="#" class="but_small1">Get Now!</a></li>
                <li></li>
                <li></li>
            
            </ul>
        </div>
    </div><!-- end section -->
    
    <div class="one_third last">
        <div class="pricingtable3">
            <ul>
            
                <li class="title"><h3 class="white">Advanced</h3></li>
                <li class="price"><h1>$45.99 <em>/Per license</em></h1> </li>
                <li class="hecont">Making this the first true generator necessary on the Internet. <br /><br /> <strong>Try Free for 30 Days</strong></li>
                <li></li>
                <li><i class="fa fa-check"></i>&nbsp; Link Protection</li>
                <li><i class="fa fa-check"></i>&nbsp; Parental controls</li>
                <li><i class="fa fa-check"></i>&nbsp; Facebook protection</li>
                <li><i class="fa fa-check"></i>&nbsp; secured online storage</li>
                <li><i class="fa fa-check"></i>&nbsp; Advanced online identity</li>
                <li></li>
                <li><a href="#" class="but_small1 gray">Read more</a> &nbsp; <a href="#" class="but_small1 gray">Get Now!</a></li>
                <li></li>
                <li></li>
            
            </ul>
        </div>
    </div><!-- end section -->
    

</div>
</div><!-- end parallax section3 -->



<div class="clearfix"></div>


<?php include '../includes-3/footer.php' ?>


<div class="clearfix"></div>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->


</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<script type="text/javascript" src="../js-3/universal/jquery.js"></script>
<script src="../js-3/style-switcher/styleselector.js"></script>
<script src="../js-3/animations/js/animations.min.js" type="text/javascript"></script>
<script src="../js-3/mainmenu/bootstrap.min.js"></script> 
<script src="../js-3/mainmenu/customeUI.js"></script>
<script src="../js-3/masterslider/jquery.easing.min.js"></script>

<script src="../js-3/scrolltotop/totop.js" type="text/javascript"></script>
<script type="text/javascript" src="../js-3/mainmenu/sticky.js"></script>
<script type="text/javascript" src="../js-3/mainmenu/modernizr.custom.75180.js"></script>
<script type="text/javascript" src="../js-3/cubeportfolio/jquery.cubeportfolio.min.js"></script>
<script type="text/javascript" src="../js-3/cubeportfolio/main.js"></script>

<script src="../js-3/aninum/jquery.animateNumber.min.js"></script>
<script src="../js-3/carouselowl/owl.carousel.js"></script>

<script type="text/javascript" src="../js-3/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="../js-3/accordion/custom.js"></script>

<script type="text/javascript" src="../js-3/universal/custom.js"></script>

</body>
</html>
