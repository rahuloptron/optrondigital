<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]--><head>
	<title>Optron Website Designing</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

    <!-- Favicon --> 
    <link rel="shortcut icon" href="../images/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
	
  <?php include '../includes/website-css.php' ?>
    
</head>

<body>

<div class="site_wrapper">

<?php include "../includes/menu-home.php" ?>
<div class="clearfix"></div>

<div class="page_title1 sty7">

    <h1>Leading website development Company.</h1>
    <h3>Whether you are looking for a website for your company or digital marketing services to grow your business; we can provide you complete solutions from website development to business promotion, lead generation services under one roof.</h3>
     <br><a href="/services/index.html" class="but_large2"><strong>&nbsp; Get Started</strong></a>
    
</div>

<div class="clearfix"></div>

<div class="feature_section10">
<div class="container">
	
    <div class="one_fifth_less">
    <div class="box">
        <h1 class="sitecolor"><strong>.biz</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$4.50</strong><em>/year</em></h1>
    </div>
    </div>
    
    <div class="one_fifth_less">
    <div class="box">
        <h1 class="sitecolor"><strong>.club</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$7.95</strong><em>/year</em></h1>
    </div>
    </div>
    
    <div class="one_fifth_less">
    <div class="box">
        <h1 class="sitecolor"><strong>.wales</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$9.75</strong><em>/year</em></h1>
    </div>
    </div>
    
    <div class="one_fifth_less">
    <div class="box">
        <h1 class="sitecolor"><strong>.co.uk</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$18.25</strong><em>/year</em></h1>
    </div>
    </div>
    
    <div class="one_fifth_less last">
    <div class="box">
        <h1 class="sitecolor"><strong>.mobi</strong></h1>
        <h6>Now Avaliable</h6>
        <h1><strong>$12.99</strong><em>/year</em></h1>
    </div>
    </div>

</div>
</div><!-- end featured section 10 -->


<div class="clearfix"></div>

<div class="feature_section12-1">
<div class="container">
    
    <div class="one_half">
    
    	<img src="../images/site-img20.jpg" alt="">
	</div><!-- end section -->
    
    <div class="one_half last">
    <div class="one_half">
        <i class="fa fa-check"></i>
        <h4 class="light">Easy Domain Setup No Technical Skills</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        </div>
        <div class="one_half last">
    	<i class="fa fa-check"></i>
        <h4 class="light">FREE Domain Privacy Protection</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Unlimited Sub Domains</h4>

        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Free Email Address - Forwarding</h4>

        </div>
	</div><!-- end section -->
    
</div>
</div><!-- end featured section 12 -->

<div class="clearfix"></div>

<div class="feature_section7">
<div class="container">

    <h1 class="caps"><strong>All Our plans include</strong></h1>
    
    <div class="clearfix margin_bottom2"></div>

    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-globe"></i> Free<br>domain</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-database"></i> Unlimited<br>Database</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-wordpress"></i> 1-click<br>install apps</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-rocket"></i> 99.9% Uptime<br>Guarantee</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less last">
        <h5 class="caps"><i class="fa fa-coffee"></i> easy-to-use<br>control panel</h5>
    </div><!-- end -->
    
    <div class="clearfix margin_bottom3"></div>
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-building-o"></i> Free<br>SiteBuilder</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-lock"></i> SSL<br>Certificates</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-paper-plane-o"></i> Transfer<br>Website &amp; Domain</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-money"></i> 30 Day<br>Money Back</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less last">
        <h5 class="caps"><i class="fa fa-headphones"></i> 24/7/365<br>Premium Support</h5>
    </div><!-- end -->
    
</div>
</div>

<div class="clearfix"></div>

<div class="feature_section338">
<div class="container">

 <div class="one_full stcode_title7">
   <h2>Recent Projects<br><span class="line"></span> </h2> 
        
    </div>

     <div class="one_third">

<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/heminfotechportfolio.png" alt="">
                </div>
                <h6 class="case-item__title">Hem Infotech</h6>
              </div>
               
</div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/hiwillportfolio.png" alt="">
                </div>
                <h6 class="case-item__title">Hi-Will Education</h6>
              </div>
           
</div>

<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/ajfilms.png" alt="">
                </div>
                <h6 class="case-item__title">Aj Films</h6>
              </div>
            
</div>

<div class="clearfix margin_bottom4"></div>

  <div class="one_third">

<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work1.png" alt="">
                </div>
                <h6 class="case-item__title">Revital Trichology </h6>
              </div>
               
</div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/enjay-1.png" alt="">
                </div>
                <h6 class="case-item__title">Enjay World</h6>
              </div>
           
</div>

<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work2.png" alt="">
                </div>
                <h6 class="case-item__title">Rohm Computers</h6>
              </div>
            
</div>

<div class="clearfix margin_bottom4"></div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work3.png" alt="">
                </div>
                <h6 class="case-item__title">Grecells</h6>
              </div>
               
</div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/bla.png" alt="">
                </div>
                <h6 class="case-item__title">Black Pepper Exhibitions</h6>
              </div>
             
</div>

<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work5.png" alt="">
                </div>
                <h6 class="case-item__title">Genesis Telecom</h6>
              </div>
            
</div>

<div class="clearfix margin_bottom4"></div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work6.png" alt="">
                </div>
                <h6 class="case-item__title">AG Digitas</h6>
              </div>
              
</div>

<div class="one_third">
 


<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/work10.png" alt="">
                  </div>
                  <h6 class="case-item__title">Sastadeals </h6>
                </div>
             
</div>

<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/vir.png" alt="">
                </div>
                <h6 class="case-item__title">Dr Viral Gada</h6>
              </div>
             
</div>


<div class="clearfix margin_bottom4"></div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="../images/par1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Prarambh Securites</h6>
                </div>
                
</div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/wc.png" alt="">
                  </div>
                  <h6 class="case-item__title">Welcome Cineplex</h6>
                </div>
              
</div>

<div class="one_third last">

<a href="website-designing/inquiry">
<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/a.png" alt="">
                  </div>
                    <h6 class="case-item__title">Make your Website here</h6>
                  </div>
                    </a>
             
</div>

</div>
</div>

<div class="clearfix"></div>

<div class="feature_section1">
                    <div class="container">
                        <h2 class="caps"><strong class="white">Industry Specific Web Design Solutions</strong></h2>
                        <div class="clearfix margin_bottom3"></div>
                        <div class="one_third"> <i class="fa fa-user-md "></i>
                            <h4>HEALTH CARE</h4>
                            <p>Website for doctors, clinics, hospitals and pharmaceutical companies</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-shopping-cart"></i>
                            <h4>E-COMMERCE</h4>
                            <p>Start selling online using our e-commerce website development services</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-tachometer"></i>
                            <h4>MANUFACTURING</h4>
                            <p>Corporate website and B2B marketing strategy for manufacturing companies</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third"> <i class="fa fa-home"></i>
                            <h4>REAL ESTATE</h4>
                            <p >Website development &amp; Digital marketing for real estate industry</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-bus"></i>
                            <h4>TOURS AND TRAVELS</h4>
                            <p>Static or dynamic website for travel agency and tours &amp; travel business</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-car"></i>
                            <h4>AUTOMOBILE</h4>
                            <p>Portal for cars and bikes, website for selling used cars, bikes etc</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third last"> <i class="fa fa-graduation-cap"></i>
                            <h4>EDUCATION</h4>
                            <p>Website for Schools, Colleges, Training institutes, coaching classes etc. </p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-coffee"></i>
                            <h4>Software &amp; IT</h4>
                            <p>Mobile friendly website, marketing solution for IT Industry</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-gift"></i>
                            <h4>FASHION AND GARMENT</h4>
                            <p>Website development &amp; promotion for fashion and garment industry</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                    </div>
                </div>

<div class="clearfix"></div>


<div class="feature_section17-1">
<div class="container">

      <h1 class="caps"><strong>Website Features</strong>
    <em>There are so many reasons to choose us for your website solutions, here are a few... </em></h1>

    <div class="one_third">
        <img src="../images/service-search.png" alt="">
        <h3 class="caps"><strong>SEO Friendly</strong></h3>
        <h5 class="light">We Provide SEO Friendly site, that improves visibility on various search engines like Google, Yahoo & more.</h5>
       
    </div><!-- end section -->
    
    <div class="one_third">
        <img src="../images/service-layers.png" alt="">
        <h3 class="caps"><strong>Payment Gateways</strong></h3>
        <h5 class="light">We Integrate payment gateway for Free. Accept payments via Debit Cards, Credit Cards, Net Banking, Paypal & COD.<br>
    </div><!-- end section -->
    
    <div class="one_third last">
        <img src="../images/service-devices.png" alt="">
        <h3 class="caps"><strong>Reponsive Design</strong></h3>
        <h5 class="light">An integrated responsive function that gives a beautiful interface on any devices like mobiles and tablets.</h5>
        
    </div><!-- end section -->
    
    
    <div class="clearfix margin_bottom5"></div>
    
    
    <div class="one_third">
        <img src="../images/service-3d-printer.png" alt="">
        <h3 class="caps"><strong>Shipping Method</strong></h3>
        <h5 class="light">We Integrate few available Logistics provider into the website. You have to signup and provide credentials.</h5>
        
    </div><!-- end section -->
    
    <div class="one_third">
         <img src="../images/sevice-startup.png" alt="">
        <h3 class="caps"><strong>Performance</strong></h3>
        <h5 class="light">We develop the website with speed & performance so your customer gets free speed browsing with latest technology.</h5>
        
    </div><!-- end section -->
    
    <div class="one_third last">
        <img src="../images/shield.png" alt="">
        <h3 class="caps"><strong>Backend Admin Panel</strong></h3>
        <h5 class="light">You can manage store from admin panel like logo, sliders, menu, products & categories without coding knowledge.</h5>
       
    </div><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>

<div class="feature_section5">
<div class="container alicent">
    
    <h2 class="caps"><strong>HOW WE WORK</strong></h2>
    
    <div class="clearfix margin_bottom2"></div>
    
    
    <ul class="pop-wrapper">
    
        <li><a href="#"> <img src="../images/copy.png" alt=""> <h6>Meeting</h6></a></li>
        
        <li><a href="#"> <img src="../images/planning.png" alt=""> <h6>Planning</h6> </a></li>
        
        <li><a href="#"> <img src="../images/web-design-1.png" alt=""> <h6>Designing</h6> </a></li>
        
        <li><a href="#"> <img src="../images/web-develop.png" alt=""> <h6>Developing</h6> </a></li>
        
        <li><a href="#"> <img src="../images/testing.png" alt=""> <h6>Testing</h6> </a></li>
        
        <li><a href="#"> <img src="../images/delivery.png" alt=""> <h6>Delivery</h6> </a></li>
        
    </ul>
    
</div>

</div><!-- end featured section 5-->


<div class="clearfix"></div>

<div class="price_compare">
<div class="container">

<h1 class="caps"><strong>Packages</strong></h1>
<div class="clearfix margin_bottom5"></div>
    
<table>
  <tbody><tr>
    <td class="first rowfirst">
        <div class="title">
            <div class="arrow_box">
         
            <h3 class="caps">Choose Packages</h3>
            </div>
        </div>
    </td>
    <td class="rowsremain">    
        <div class="prices">
            <h4 class="caps">Silver</h4>
           
        </div>
    </td>
    <td class="rowsremain">
        <div class="prices">
           
            <h4 class="caps">Gold</h4>
           
        </div>
    </td>
    <td class="rowsremain">
        <div class="prices">
            <h4 class="caps">Platinum</h4>
          
        </div>
    </td>
  </tr>
  <tr>
    <th>INITIAL REVIEW & ANALYSIS</th>
    <th></th>
    <th></th>
    <th></th>
  </tr>
  <tr>
    <th class="alileft">Size of Website</th>
    <th>5 Pages</th>
    <th>10 Pages</th>
    <th>20 Pages</th>
  </tr>
  <tr>
    <td class="alileft">Keywords Targeted</td>
    <th>Max 5</th>
    <th>Max 10</th>
    <th>Max 20</th>
  </tr>
  <tr>
    <th class="alileft">Site Analysis</th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
  </tr>
  <tr>
    <td class="alileft">Rank report</td>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
  </tr>
  <tr>
    <th class="alileft">Keyword research</th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
  </tr>
  <tr>
    <td class="alileft">Competition Analysis</td>
    <th>3 websites</th>
    <th>5 websites</th>
    <th>8 websites</th>
  </tr>
  <tr>
    <th class="alileft">Back link analysis</th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
  </tr>

  <tr>
    <th>ON PAGE OPTIMIZATION</th>
    <th></th>
    <th></th>
    <th></th>
  </tr>


  <tr>
    <td class="alileft">Google Penalty Check</td>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
  </tr>
  <tr>
    <th class="alileft">Backlink Analysis</th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
  </tr>
  <tr>
    <td class="alileft">Website Page Load Optimization</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>
  <tr>
    <th class="alileft">Header Tags Optimization</th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
  </tr>
  <tr>
    <td class="alileft">HTML Code Cleanup & Optimization</td>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
  </tr>
  <tr>
    <th class="alileft">Image & Hyperlink Optimization</th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
    <th><i class="fa fa-check sitecolor"></i></th>
  </tr>
  <tr>
    <td class="alileft">Robots.txt Creation/Analysis</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>
  <tr>
    <th class="alileft">HTML & XML, GEO Sitemaps</th>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>
  <tr>
    <td class="alileft">Google & Bing Webmaster Tools</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>
  <tr>
    <th class="alileft">Google Analytics</th>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>
  <tr>
    <td class="alileft">Keyword Density Analysis & Content Optimization</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">External Links Optimization</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Website SEO Content Recommendations</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td>LOCAL SEARCH OPTIMIZATION</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>

  <tr>
    <td class="alileft">Google Local Places Setup & Verification</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

   <tr>
    <td class="alileft">Setting Geo Targeting in Google</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Classified Submissions</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Local Directory Submissions</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td>OFF PAGE + CONTENT MARKETING</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>

  <tr>
    <td class="alileft">Official Wordpress Blog SetUp</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Blog Creation, updation and promotion</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Guest Blog Post Writing</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

   <tr>
    <td>SOCIAL MEDIA OPTIMIZATION</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>

  <tr>
    <td class="alileft">Facebook & Twitter Account Setup</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Facebook cover Page Design</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Profile Content Writing</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Facebook Wall Updates</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Google + Update</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Facebook Timeline Design (6 Months)</td>
    <td><i class="fa fa-times gray"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

   <tr>
    <td>MONTHLY REPORTING</td>
    <td></td>
    <td></td>
    <td></td>
  </tr>


  <tr>
    <td class="alileft">SEO Reports</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  
  <tr>
    <td class="alileft">Search Engine Rank Report</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Google Analytics Report</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="alileft">Monthly Action Plan</td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
    <td><i class="fa fa-check sitecolor"></i></td>
  </tr>

  <tr>
    <td class="first rowfirst"></td>
    <td class="rowsremain">    
        <div class="prices">
            <strong>10K<i>/M</i></strong> <b>Regularly <em>$4.99</em></b>
            <a href="#">Buy Now</a>
        </div>
    </td>
    <td class="rowsremain">
        <div class="prices">
            <strong>15K<i>/M</i></strong> <b>Regularly <em>$8.99</em></b>
            <a href="#">Buy Now</a>
        </div>
    </td>
    <td class="rowsremain">
        <div class="prices">
            <strong>20K<i>/M</i></strong> <b>Regularly <em>$18.99</em></b>
            <a href="#">Buy Now</a>
        </div>
    </td>
  </tr>

</tbody></table>

</div>
</div>

<div class="clearfix"></div>

<div class="parallax_section2">
<div class="container">
    
    <div class="counters12">
    
        <div class="one_fifth_less"> <h5 class="light">Projects</h5> <h1 id="target31"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Clients</h5> <h1 id="target32"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Awards</h5> <h1 id="target33"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Domains</h5> <h1 id="target34"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less last"> <h5 class="light">Days</h5> <h1 id="target35"></h1> </div><!-- end section -->
     
    </div>

</div>
</div><!-- end parallax section 2 -->

<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Some <strong>Our Esteemed</strong><span> Clients</span></span></h2>
     
    </div><!-- end section title heading -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="../images/clients/adam-fabriwerk.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                 <h5><strong><a href="#">Adam Fabriwerk</a></strong></h5>
            <p>Manufactures of processing systems for pharma</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
       
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/ajfilms.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Aj Films</a></strong></h5>
            <p>Best Photography in Mumbai</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/agdigitas.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ag Digitas</a></strong></h5>
                <p>IT Solutions Company</p>
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/blacpeper.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Black Peper</a></strong></h5>
                <p>Black Pepper is an Exhibition Stall Design company</p>
            </div>
            
        </div>
    
    </div>
    
    
     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/enjay.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Enjay Solution</a></strong></h5>
                <p>Expert in CRM and Telephony Solutions across verticals</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/genesis.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Genesis Telesecure</a></strong></h5>
                <p>Genesis Telesecure LLP offers complete range of solutions</p>
            </div>
            
        </div>
    
    </div>
                      
    <div class="one_fourth">

    <div class="flips4">
    
        <div class="flips4_front flipscont4">
       <img src="../images/clients/grecelllogo.png" alt="" >
        </div>
        
        <div class="flips4_back flipscont4">
            <h5><strong><a href="#">Grecells</a></strong></h5>
            <p>Disaster recovery solutions for Windows server</p>
        </div>
        
    </div>

    </div>      
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/heminfotech.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hem Infotech</a></strong></h5>
                <p>IT Solutions and Services Company</p>

            </div>
            
        </div>
    
    </div>
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/hi-will-logo.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hi-Will Education</a></strong></h5>
                <p>Engineers classes</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/neotech.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/prarambh.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Prarambh Security</a></strong></h5>
                <p>Security Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/pratrikshainterior.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Pratiksha Interior</a></strong></h5>
                <p>Modular kitchen, Office Furniture and Roofing</p>

            </div>
            
        </div>
    
    </div>

 </div>
</div>

<div class="clearfix"></div>

<div class="feature_section8">
<div class="container">
    
    <h2 class="caps white"><strong>what our customers say</strong></h2>
    
    <div class="clearfix margin_bottom2"></div>
    
    <div id="owl-demo13" class="owl-carousel">
                    
        <div class="slidesec">
            <div class="imgbox one"><img src="../images/site-img20.jpg" alt="" /></div>
            <h4>- Mark Jones Computer Services</h4>
            <p class="bigtfont">Purchase the Template a more recently pagemaker include versions tortorhas survived not only five centuries making this the first true randomised words which generator on the tend to repeat predefined chunks as predefined chunks as randomised necessary.</p>
            <br />
            <strong>website:</strong> www.markjones.com
            <br />
            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
        </div><!-- end a slide -->
        
        
        <div class="slidesec">
            <div class="imgbox one"><img src="../images/site-img20.jpg" alt="" /></div>
            <h4>- Henry Clarence</h4>
            <p class="bigtfont">Purchase the Template a more recently pagemaker include versions tortorhas survived not only five centuries making this the first true randomised words which generator on the tend to repeat predefined chunks as predefined chunks as randomised necessary.</p>
            <br />
            <strong>website:</strong> www.henryclarence.us
            <br />
            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
        </div><!-- end a slide -->
        
        
        <div class="slidesec">
            <div class="imgbox one"><img src="../images/site-img20.jpg" alt="" /></div>
            <h4>- Maurice Jamar</h4>
            <p class="bigtfont">Purchase the Template a more recently pagemaker include versions tortorhas survived not only five centuries making this the first true randomised words which generator on the tend to repeat predefined chunks as predefined chunks as randomised necessary.</p>
            <br />
            <strong>website:</strong> www.mauricejamar.co.uk
            <br />
            <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i>
        </div><!-- end a slide -->
                            
    </div>

</div>
</div><!-- end featured section 8 -->

<div class="clearfix"></div>

<div class="feature_section9">
<div class="container">

    <h1 class="caps"><strong>FAQ - Your questions? We got answers!</strong></h1>
    
    <div class="clearfix margin_bottom3"></div>
    
    <div class="one_half">
    
        <div class="box">
            <h4 class="caps"><strong>WHAT'S A DOMAIN NAME?</strong></h4>
            <p class="bigtfont">Many desktop publishing packages and web page editors now use many web sites still in their versions have over the years.</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>What is a domain extension?</strong></h4>
            <p class="bigtfont">Desktop publishing packages and web page editors now use Lorem Ipsum as their default model text and a search</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>Can I use my domain for email?</strong></h4>
            <p class="bigtfont">Lorem Ipsum as their default model text and a search for will uncover many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>What can I do with a domain name?</strong></h4>
            <p class="bigtfont">Web page editors now use Lorem Ipsum as their default model text and a search for will uncover many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
    </div><!-- end section -->
    
    <div class="one_half last">
    
        <div class="box">
            <h4 class="caps"><strong>How long does it take to register my domain?</strong></h4>
            <p class="bigtfont">Lorem Ipsum as their default model many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>What are your Terms &amp; Conditions?</strong></h4>
            <p class="bigtfont">Desktop publishing packages and web page editors now use Lorem Ipsum as their default model text and a search</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>WHAT PAYMENT OPTIONS DO YOU OFFER?</strong></h4>
            <p class="bigtfont">Lorem Ipsum as their default model text and a search for will uncover many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>Can I sell or transfer the domain?</strong></h4>
            <p class="bigtfont">Web page editors now use Lorem Ipsum as web sites still in their infancy their default model text and a search for will uncover many versions have evolved over the years.</p>
        </div>
        
    </div><!-- end section -->

</div>
</div><!-- end featured section 9 -->

<div class="clearfix"></div>

<div class="container feature_section107"><br><h1 class="caps light">NEED HELP? Call our support at <b>(+91) 9833189090</b> or <a href="#">Request Quote</a></h1></div>

<div class="clearfix"></div>

<?php include '../includes/website-footer.php' ?>

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
 
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<?php include '../includes/website-js.php' ?>

</body>
</html>
