<!doctype html>

<html lang=en-gb class=no-js>
<
<head>
<title>Static Website Designing Company IN MUMBAI | 25+ services | 7 Years of Experience</title>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name=keywords content />
<meta name=description content="One of the most innovative Digital Marketing Agency in Mumbai for SEO Services, Digital Marketing Consulting, Social Media Consulting, Email Marketing, Lead Generation, Online promotion, Google Adwords, Facebook Advertising, PPC services " />
<?php include "includes/common-css.php" ?>
<link rel="stylesheet" href="form.css">
</head>
<body>
<div class=site_wrapper>
<?php include "../includes/menu-home.php" ?>
<div class=clearfix></div>

<div class="feature_section479">
<div class="container">

    <h1 class="less6">We Develop Websites that Ignite your Business.


<br> <em>Optron Web Design company that will guide you to your online success<br>
Because the way your site looks is our business</em> </h1>


    
    <a href="#">Get Started</a> 
    
</div>
</div>



<div class=clearfix></div>
<div class="feature_section80">
<div  class="container">
  <div class="arrow_box">
        <h1>We Develop Websites that Ignite your Business.</h1>
        <p class="big_text1 less1">Engaging | Powerful | Creative</p><br>
        <p class="bigtfont less10">


We fully acknowledge what are clients are looking for when it comes to a excellent website design company. This is why; ‘Trust’ and ‘Loyalty’ are two main aspects that are being exchanged between our clients and us for more than a decade, now and will always do in our future </p>
          
    
</div>
</div>
</div> 

<div class="clearfix"></div>
<div class="feature_section629">
    <div class="container">
      <h3 class="white caps">To find out if a static website would suit your requirements | Feel free to </h3>
      <a href="#" class="button one">Contact Us</a> </div>
  </div>



<div class="clearfix"></div>

<div class=divider_line23></div>

<div class="feature_section743">
<div class="container">

    <div class="one_half">
    
     <h2>Static Website is suitable for Small and Medium size Business</h2>   <p class="bigtfont">We take care of the smallest details while making your website and writing content to ensure that the above mentioned objectives are done with perfection.<br> <b>Advantages for Static Website are as follows :-</b></p>
 <div class="clearfix margin_top2"></div>
 <ul>
 <li><i class="fa fa-check"></i> Excellent for smaller companies</li>
 <li><i class="fa fa-check"></i> Simple to create and easy to Host</li>

 <li><i class="fa fa-check"></i> Complete control over the Content </li>

 <li><i class="fa fa-check"></i> Costs lesser than any Dynamic Website design </li>

 <li><i class="fa fa-check"></i> The page layout can be changed whenever desired</li>


            </ul>
        </div>

<div class="one_half last">
    	
<script type="text/javascript" src="//static.mailerlite.com/data/webforms/446663/u0p2r9.js?v3"></script>

    </div>
	

</div>
</div>

<div class="clearfix"></div>
<div class="feature_section255">
<div class="container">

      <h3>OUR CREDENTIAL
        </h3>

    
    <div class="clearfix margin_bottom3"></div>
    
   
    
    
    
    <div class="counters2">
    
        <div class="one_fourth"> <i class="fa fa-heart"></i> <span id="target6"> 15</span> 
        <h4>Static Website Design</h4> </div>
        
        <div class="one_fourth"> <i class="fa fa-star"></i> <span id="target7"> 32</span> 
        <h4>Dynamic website Design</h4> </div>
        
        <div class="one_fourth"> <i class="fa fa-thumbs-up"></i> <span id="target8"> 50</span>
        <h4>Customized website Design</h4> </div>
        
        <div class="one_fourth last"> <i class="fa fa-shopping-cart"></i> <span id="target9"> 40</span> <h4>E-commerce website Design</h4> </div>
        
       
    </div>
    
    
      
</div>
</div>


 <!-- <div class="feature_section329">
 <div  class="container">
 
    
        <h1 class="white less9">Your website creates the first impression for your Brand</h1>
        <p class="bigtfont less10">Static Web Designing is very simple which show's your Company's Profile or Information.<br> It is Designed in HTML Platform as it is easy to develop as compared to other Platforms.<br>Creates an attractive professional website which is easy to navigate. </p>
        <br>
        
   
  
 </div>
 </div>
 -->





<div class="feature_section873">

<div class="container">

    <div class="left">
    	<h1 class="caps"><strong>Why Choose OptronMarketing</strong> <br/>For Website Development</h1>
	</div>
    
    <div class="right">
       
        
        <ul>
            <li><i class="fa  fa-check"></i>10 + YEARS OF EXPERIENCE</li>
            <li><i class="fa  fa-check"></i>ETHICAL BUSINESS PRACTICES</li>
            <li><i class="fa  fa-check"></i>RESULT ORIENTED</li>
            <li><i class="fa  fa-check"></i>QUALITY FIRST</li>
            <li><i class="fa  fa-check"></i>OUR TALENTED TEAM</li>
            <li><i class="fa  fa-check"></i>OUR CORE VALUES</li>
    	</ul>
    </div>
    
</div>
    </div>

     <div class="clearfix"></div>
<div class="feature_section37">
<div class="container">

  <div class="one_full">
       
   
    <h2 class="caps"><strong>Technologies We Use</strong></h2>
        <div class="clearfix margin_bottom2"></div>
        <ul>
          <li><img src="../images/scripts-logo1.jpg" alt="" /></li>
            <li><img src="../images/scripts-logo2.jpg" alt="" /></li>
            <li><img src="../images/scripts-logo3.jpg" alt="" /></li>
            <li><img src="../images/scripts-logo4.jpg" alt="" /></li>
           <li><img src="../images/scripts-logo7.jpg" alt="" /></li>
            <li><img src="../images/scripts-logo6.jpg" alt="" /></li>
           
          
        </ul>
        
    </div>
    

</div>
</div>


<div class="clearfix"></div>
<div class="feature_section255">
<div class="container">

      <h3>OUR CREDENTIAL
        </h3>

    
    <div class="clearfix margin_bottom3"></div>
    
   
    
    
    
    <div class="counters2">
    
        <div class="one_fourth"> <i class="fa fa-heart"></i> <span id="target6"> 15</span> 
        <h4>Static Website Design</h4> </div>
        
        <div class="one_fourth"> <i class="fa fa-star"></i> <span id="target7"> 32</span> 
        <h4>Dynamic website Design</h4> </div>
        
        <div class="one_fourth"> <i class="fa fa-thumbs-up"></i> <span id="target8"> 50</span>
        <h4>Customized website Design</h4> </div>
        
        <div class="one_fourth last"> <i class="fa fa-shopping-cart"></i> <span id="target9"> 40</span> <h4>E-commerce website Design</h4> </div>
        
       
    </div>
    
    
      
</div>
</div>

<div class=clearfix></div>










<!-- 
<div class="feature_section62">
<div class="container">
  
  <h2>Static website Designing Packages</h2>
    
    <div class="one_third highlight">
    <div class="title">
      <h4>Plan 1</h4><h2>Silver </h2></div>
    <ul>
      <li><i class="fa  fa-check"></i>Upto 10 Pages</li>
      <li><i class="fa  fa-check"></i>SEO Ready</li>
      <li><i class="fa  fa-check"></i>Mobile Friendly Layout</li>
      <li><i class="fa  fa-check"></i>Dynamic Content</li>
      <li><i class="fa  fa-check"></i>Inquiry Page</li>
       <li><i class="fa  fa-check"></i>Contact Page</li>
       <li><i class="fa  fa-close"></i>Product Search</li>
        <li><i class="fa  fa-close"></i>Advanced Search</li>
        <li><i class="fa  fa-close"></i>Custom Home
Page</li>
<li><i class="fa  fa-check"></i>5 Menu Itemse</li>
<li><i class="fa  fa-close"></i>Custom Design</li>
       
    </ul>
    <div class="clearfix margin_bottom2"></div><a href="#" class="button_2">Request Quote</a>
    </div>
    
    <div class="one_third highlight ">
    <div class="title"><h4>Plan 2</h4><h2>Gold</h2></div>
    <ul>
      <li><i class="fa  fa-check"></i>Between 10-15 Pages</li>
      <li><i class="fa  fa-check"></i>SEO Ready</li>
      <li><i class="fa  fa-check"></i>Mobile Friendly Layout</li>
      <li><i class="fa  fa-check"></i>Dynamic Content</li>
      <li><i class="fa  fa-check"></i>Inquiry Page</li>
       <li><i class="fa  fa-check"></i>Contact Page</li>
       <li><i class="fa  fa-close"></i>Product Search</li>
        <li><i class="fa  fa-close"></i>Advanced Search</li>
        <li><i class="fa  fa-close"></i>Custom Home
Page</li>
       
       <li><i class="fa  fa-check"></i>7 Menu Items</li>
       <li><i class="fa  fa-close"></i>Custom Design</li>

    </ul>
    <div class="clearfix margin_bottom2"></div><a href="#" class="button_2">Request Quote</a>
    </div>
    
    <div class="one_third highlight last">
    <div class="title"><h4>Plan 3</h4><h2>Platinum</h2></div>
    <ul>
      <li><i class="fa  fa-check"></i>Between 15-20 Pages</li>
      <li><i class="fa  fa-check"></i>SEO Ready</li>
      <li><i class="fa  fa-check"></i>Mobile Friendly Layout</li>
      <li><i class="fa  fa-check"></i>Dynamic Content</li>
      <li><i class="fa  fa-check"></i>Inquiry Page</li>
       <li><i class="fa  fa-check"></i>Contact Page</li>
       <li><i class="fa  fa-check"></i>Product Search</li>
        <li><i class="fa  fa-check"></i>Advanced Search</li>
        <li><i class="fa  fa-check"></i>Custom Home
Page</li>
<li><i class="fa  fa-check"></i>10 Menu Items</li>
<li><i class="fa  fa-check"></i>Custom Design</li>

    </ul>
    <div class="clearfix margin_bottom2"></div><a href="#" class="button_2">Request Quote</a>
    </div>
</div>
</div>

 -->

<div class=clearfix></div>
<?php include "../includes/test.php" ?>
<div class=clearfix></div>

<div class=divider_line23></div>
<div class=clearfix></div>
<?php include "../includes/partner.php" ?>




<div class=clearfix></div>
<?php include "../includes/footer.php" ?>
<a href=# class=scrollup>Scroll</a>
</div>
<?php include "../includes/common-js.php" ?>
<?php include "../includes/ga.php" ?>

</body>
</html>