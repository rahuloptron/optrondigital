<!doctype html>
 <html lang="en-gb" class="no-js"> 
 <head>

	<title>Web Design Company in Mumbai</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <!-- Favicon --> 
	<link rel="shortcut icon" href="../images-3/favicon.png">
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
	
    <?php include '../includes-3/css.php' ?>
    
</head>

<body>

<div class="site_wrapper">

<header class="header">
 
	<div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="index.html" id="logo"></a></div>
		
	<!-- Navigation Menu -->
    <div class="menu_main">
    
      <div class="navbar yamm navbar-default">
        
          <div class="navbar-header">
            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  >
              <button type="button" > <i class="fa fa-bars"></i></button>
            </div>
          </div>
          
          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
          
            <?php include '../includes-3/menu.php' ?>
            
          </div>
        
      </div>
    </div>
    
	</div>
    
</header>


<div class="clearfix"></div>

<!-- Slider
======================================= -->

<div class="feature_section18" style="background-image: url(../images-3/bg1.jpg">
<div class="container">

    <h1 class="caps"><strong>We Are With You Every Step <br>Get Started Easily.</strong></h1>
    <h4>Make your Website Faster, Safer &amp; Better Support.</h4>
    <br><br>
    <a href="#">Get Started Now!</a>
    
</div>
</div><!-- end search section -->
<!-- end of masterslider -->
<div class="clearfix"></div>


<div class="feature_section106-1">
<div class="container">

    <h1 class="caps"><strong>Start a project</strong>
    <em>There are so many reasons to choose us for your website solutions, here are a few... </em></h1>
   

    <div class="one_fourth">

    <h3><a href="#">Corporate Website</a></h3>
    <p>We Create Beautiful, Unique and Responsive Website for all Type of Corporate Companies in just 3 Days.Real Estate and Holiday Package Portals at Affordable Price.</p></br><a href="#" class="but_small2"><i class="fa fa-paper-plane"></i>&nbsp; More</a>
   
    </div><!-- end section -->
    
    <div class="one_fourth">
    <h3><a href="#">Ecommerce Website</a></h3>
    <p>Get Responsive Ecommerce Site to Sell your Products Online to Worldwide Customers with Admin Panel.Real Estate and Holiday Package Portals at Affordable Price.</p></br><a href="#" class="but_small2"><i class="fa fa-paper-plane"></i>&nbsp; More</a>
    </div><!-- end section -->
    
    <div class="one_fourth">
    <h3><a href="#">Web Portal Design</a></h3>
    <p>We Provide Ready-made Portals like Job Portal, Real Estate and Holiday Package Portals at Affordable Price.Real Estate and Holiday Package Portals at Affordable Price. </p></br><a href="#" class="but_small2"><i class="fa fa-paper-plane"></i>&nbsp; More</a>
    </div><!-- end section -->
    
    <div class="one_fourth last">
    <h3><a href="#">Application Design</a></h3>
    <p>Get a Standard Mobile Application Developed to Target the Mobile Users and to Increase your Business Online.Real Estate and Holiday Package Portals at Affordable Price. </p></br><a href="#" class="but_small2"><i class="fa fa-paper-plane"></i>&nbsp; More</a>
    </div><!-- end section -->

    
</div>
</div>

<div class="clearfix"></div>

<div class="feature_section1">
                    <div class="container">
                        <h2 class="caps"><strong class="white">Industry Specific Web Design Solutions</strong></h2>
                        <div class="clearfix margin_bottom3"></div>
                        <div class="one_third"> <i class="fa fa-user-md "></i>
                            <h4>HEALTH CARE</h4>
                            <p>Website for doctors, clinics, hospitals and pharmaceutical companies</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-shopping-cart"></i>
                            <h4>E-COMMERCE</h4>
                            <p>Start selling online using our e-commerce website development services</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-tachometer"></i>
                            <h4>MANUFACTURING</h4>
                            <p>Corporate website and B2B marketing strategy for manufacturing companies</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third"> <i class="fa fa-home"></i>
                            <h4>REAL ESTATE</h4>
                            <p >Website development &amp; Digital marketing for real estate industry</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-bus"></i>
                            <h4>TOURS AND TRAVELS</h4>
                            <p>Static or dynamic website for travel agency and tours &amp; travel business</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-car"></i>
                            <h4>AUTOMOBILE</h4>
                            <p>Portal for cars and bikes, website for selling used cars, bikes etc</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third last"> <i class="fa fa-graduation-cap"></i>
                            <h4>EDUCATION</h4>
                            <p>Website for Schools, Colleges, Training institutes, coaching classes etc. </p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-coffee"></i>
                            <h4>Software &amp; IT</h4>
                            <p>Mobile friendly website, marketing solution for IT Industry</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-gift"></i>
                            <h4>FASHION AND GARMENT</h4>
                            <p>Website development &amp; promotion for fashion and garment industry</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                    </div>
                </div>


<div class="clearfix"></div>


<div class="feature_section17-1">
<div class="container">

      <h1 class="caps"><strong>Website Features</strong>
    <em>There are so many reasons to choose us for your website solutions, here are a few... </em></h1>

    <div class="one_third">
        <img src="../images-3/service-search.png" alt="">
        <h3 class="caps"><strong>SEO Friendly</strong></h3>
        <h5 class="light">We Provide SEO Friendly site, that improves visibility on various search engines like Google, Yahoo & more.</h5>
       
    </div><!-- end section -->
    
    <div class="one_third">
        <img src="../images-3/service-layers.png" alt="">
        <h3 class="caps"><strong>Payment Gateways</strong></h3>
        <h5 class="light">We Integrate payment gateway for Free. Accept payments via Debit Cards, Credit Cards, Net Banking, Paypal & COD.<br>
    </div><!-- end section -->
    
    <div class="one_third last">
        <img src="../images-3/service-devices.png" alt="">
        <h3 class="caps"><strong>Reponsive Design</strong></h3>
        <h5 class="light">An integrated responsive function that gives a beautiful interface on any devices like mobiles and tablets.</h5>
        
    </div><!-- end section -->
    
    
    <div class="clearfix margin_bottom5"></div>
    
    
    <div class="one_third">
        <img src="../images-3/service-3d-printer.png" alt="">
        <h3 class="caps"><strong>Shipping Method</strong></h3>
        <h5 class="light">We Integrate few available Logistics provider into the website. You have to signup and provide credentials.</h5>
        
    </div><!-- end section -->
    
    <div class="one_third">
         <img src="../images-3/sevice-startup.png" alt="">
        <h3 class="caps"><strong>Performance</strong></h3>
        <h5 class="light">We develop the website with speed & performance so your customer gets free speed browsing with latest technology.</h5>
        
    </div><!-- end section -->
    
    <div class="one_third last">
        <img src="../images-3/shield.png" alt="">
        <h3 class="caps"><strong>Backend Admin Panel</strong></h3>
        <h5 class="light">You can manage store from admin panel like logo, sliders, menu, products & categories without coding knowledge.</h5>
       
    </div><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>

<div class="parallax_section2">
<div class="container">
    
    <div class="counters12">
    
        <div class="one_fifth_less"> <h5 class="light">Projects</h5> <h1 id="target31"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Clients</h5> <h1 id="target32"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Awards</h5> <h1 id="target33"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Domains</h5> <h1 id="target34"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less last"> <h5 class="light">Days</h5> <h1 id="target35"></h1> </div><!-- end section -->
     
    </div>

</div>
</div><!-- end parallax section 2 -->


 <div class="clearfix"></div>

    <div class="feature_section3">
<div class="container">

    <div class="one_half">
    
        <h2 class="caps">Latest News / Blogs</h2>
        
        <div class="clearfix margin_bottom1"></div>
        
        
        <div id="owl-demo13" class="owl-carousel">
        
            <div class="lstblogs">
            <div>
                <a href="#"><img src="http://placehold.it/560x250" alt="" /></a>
                <a href="#" class="date"><strong>12</strong> Aug</a>
                <h4 class="white light"><a href="#" class="tcont">Galley of type and scrambled it to make typesets specimen book with the release sheets.</a> <div class="hline"></div></h4>
            </div>
            </div><!-- end this slide -->
            
            <div class="lstblogs">
            <div>
                <a href="#"><img src="http://placehold.it/560x250" alt="" /></a>
                <a href="#" class="date"><strong>11</strong> Aug</a>
                <h4 class="white light"><a href="#" class="tcont">Desktop type and scrambled it to make typesets specimen book with the release sheets.</a> <div class="hline"></div></h4>
            </div>
            </div><!-- end this slide -->
            
            <div class="lstblogs">
            <div>
                <a href="#"><img src="http://placehold.it/560x250" alt="" /></a>
                <a href="#" class="date"><strong>10</strong> Aug</a>
                <h4 class="white light"><a href="#" class="tcont">Packages of type and scrambled it to make specimen book with the release sheets.</a> <div class="hline"></div></h4>
            </div>
            </div><!-- end this slide -->
            
        </div>
            
    </div><!-- end latest news / blogs section -->
    
    
    <div class="one_half last">
    
        <h2 class="caps">Have Questions?</h2>
        
        <div class="clearfix margin_bottom1"></div>
        
        <div id="st-accordion-six" class="st-accordion-six">
        
            <ul>
            
                <li>
                    <a href="#">3 Diffrent Hosting Websites<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>She packed her seven versalia, put her into the belt and made on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her she had a last view back on the skyline of her  hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane over the years, sometimes by sometimes on purpose.</p>
                    </div>
                </li>
                
                <li>
                    <a href="#">Fully Responsive Well Structured<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                    </div>
                </li>
                
                <li>
                    <a href="#">Free Support Free Lifetime Updates<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>O my friend - but it is too much for my strength - I sink under the weight of the splendour of these visions!</p>
                        <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>
                    </div>
                </li>
                
                <li>
                    <a href="#">Premium Sliders Portfolios and Forms<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What's happened to me?"</p>
                        <p>He thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>
                    </div>
                </li>
                
            </ul>
    </div>
        
    </div>

</div>
</div><!-- end feature section 3 -->

      <div class="clearfix"></div>

<div class="client_logos">
                    <div class="container">
                        <h2 class="caps"><strong>Our Recent work</strong></h2>
                        <div class="margin_top1"></div>
                        <div class="clearfix"></div>
                        <div id="owl-demo" class="owl-carousel">
                            <div class="item"><img src="/images-3/sliders/newindexviral.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexgrecells.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexag.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexshakti.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexenjay.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexrohm.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexrevital.png" alt="" /></div>
                            <div class="item"><img src="/images-3/sliders/newindexaashrayaa.png" alt="" /></div>
                        </div>
                        <!-- end section -->
                    </div>
                </div>
<div class="clearfix"></div>


<div class="feature_section6">
<div class="container">
	
    <h1 class="caps"><strong>why customers <i class="fa fa-heart sitecolor"></i> us!</strong></h1>
    
    <div class="clearfix margin_bottom1"></div>
    
	<div class="less6">
    <div id="owl-demo20" class="owl-carousel">
    
        <div class="item">
        	<div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
        	<p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
			<br />
        	<strong>- Michile Johnson</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
        	<div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
        	<p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
			<br />
        	<strong>- Desirae Karla</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
        	<div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
        	<p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
			<br />
        	<strong>- Franklin Brice</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
                
    </div>
    </div>

</div>
</div><!-- end featured section 5-->


<div class="clearfix"></div>

<div class="host_plans">
<div class="container">

    <h1 class="caps"><strong>Pricing & Plans - Get 30% OFF!</strong>
    <em>We are Announcing Perfect Package for you</em></h1>
    
    <div class="clearfix margin_bottom3"></div>
    
    <div class="one_fourth_less">
    <div class="planbox">
        
        <div class="title"><h4 class="caps"><strong>CORPORATE</strong></h4></div>
        
        <div class="prices">
            <strong>2.99<i>/M</i></strong> <b>Regularly <em>4.99</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
            <li><strong>Unlimited</strong> Web Space</li>
            <li><strong>FREE</strong> Site Building Tools</li>
            <li><strong>FREE</strong> Domain Registar</li>
            <li><strong>24/7/365</strong> Support </li>
            <li><strong>FREE</strong> Marketing &amp; SEO Tools</li>
            <li><strong>99.9%</strong> Service Uptime</li>
            <li class="last"><strong>30 Day</strong> Money Back Guarantee</li>
        </ul>
        
    </div>
    </div><!-- end plan -->
    
    <div class="one_fourth_less">
    <div class="planbox highlight">
    
        <img src="../images-3/best-seller.png" alt="" class="hiimg" />
        
        <div class="title"><h4 class="caps"><strong>ECOMMERCE</strong></h4></div>
        
        <div class="prices">
            <strong>9.75<i>/M</i></strong> <b>Regularly <em>16.99</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
            <li><strong>Unlimited</strong> Web Space</li>
            <li><strong>FREE</strong> Site Building Tools</li>
            <li><strong>FREE</strong> Domain Registar</li>
            <li><strong>24/7/365</strong> Support </li>
            <li><strong>FREE</strong> Marketing &amp; SEO Tools</li>
            <li><strong>99.9%</strong> Service Uptime</li>
            <li class="last"><strong>30 Day</strong> Money Back Guarantee</li>
        </ul>
        
    </div>
    </div><!-- end plan -->
    
    <div class="one_fourth_less">
    <div class="planbox">
        
        <div class="title"><h4 class="caps"><strong>WEB PORTALS
</strong></h4></div>
        
        <div class="prices">
            <strong>45.50<i>/M</i></strong> <b>Regularly <em>90.50</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
            <li><strong>Unlimited</strong> Web Space</li>
            <li><strong>FREE</strong> Site Building Tools</li>
            <li><strong>FREE</strong> Domain Registar</li>
            <li><strong>24/7/365</strong> Support </li>
            <li><strong>FREE</strong> Marketing &amp; SEO Tools</li>
            <li><strong>99.9%</strong> Service Uptime</li>
            <li class="last"><strong>30 Day</strong> Money Back Guarantee</li>
        </ul>
        
    </div>
    </div><!-- end plan -->
    
    <div class="one_fourth_less last">
    <div class="planbox">
        
        <div class="title"><h4 class="caps"><strong>MOBILE APPS
</strong></h4></div>
        
        <div class="prices">
            <strong>100<i>/M</i></strong> <b>Regularly <em>200</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
            <li><strong>Unlimited</strong> Web Space</li>
            <li><strong>FREE</strong> Site Building Tools</li>
            <li><strong>FREE</strong> Domain Registar</li>
            <li><strong>24/7/365</strong> Support </li>
            <li><strong>FREE</strong> Marketing &amp; SEO Tools</li>
            <li><strong>99.9%</strong> Service Uptime</li>
            <li class="last"><strong>30 Day</strong> Money Back Guarantee</li>
        </ul>
        
    </div>
    </div><!-- end plan -->
    
</div>
</div><!-- end hosting plans -->

<div class="clearfix"></div>

<div class="feature_section5">
<div class="container">


    <center><h1 class="caps"><strong>TECHNOLOGIES WE USE</strong></h1></center>
    <div class="clearfix margin_bottom1"></div>

    <div class="content">
    
        
        
        <ul>
            <li><img src="../images-3/media/scripts-logo1.jpg" alt=""></li>
            <li><img src="../images-3/media/scripts-logo2.jpg" alt=""></li>
            <li><img src="../images-3/media/scripts-logo3.jpg" alt=""></li>
            <li><img src="../images-3/media/scripts-logo4.jpg" alt=""></li>
             <li><img src="../images-3/media/scripts-logo7.jpg" alt=""></li>
            <li><img src="../images-3/media/scripts-logo6.jpg" alt=""></li>
           
          
        </ul>
        
    </div>
    

</div>
</div><!-- end featured section 5-->
<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Some <strong>Our Esteemed</strong><span> Clients</span></span></h2>
     
    </div><!-- end section title heading -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="../images/clients/adam-fabriwerk.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                 <h5><strong><a href="#">Adam Fabriwerk</a></strong></h5>
            <p>Manufactures of processing systems for pharma</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
       
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/ajfilms.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Aj Films</a></strong></h5>
            <p>Best Photography in Mumbai</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/agdigitas.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ag Digitas</a></strong></h5>
                <p>IT Solutions Company</p>
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/blacpeper.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Black Peper</a></strong></h5>
                <p>Black Pepper is an Exhibition Stall Design company</p>
            </div>
            
        </div>
    
    </div>
    
    
     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/enjay.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Enjay Solution</a></strong></h5>
                <p>Expert in CRM and Telephony Solutions across verticals</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/genesis.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Genesis Telesecure</a></strong></h5>
                <p>Genesis Telesecure LLP offers complete range of solutions</p>
            </div>
            
        </div>
    
    </div>
                      
    <div class="one_fourth">

    <div class="flips4">
    
        <div class="flips4_front flipscont4">
       <img src="../images/clients/grecelllogo.png" alt="" >
        </div>
        
        <div class="flips4_back flipscont4">
            <h5><strong><a href="#">Grecells</a></strong></h5>
            <p>Disaster recovery solutions for Windows server</p>
        </div>
        
    </div>

    </div>      
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/heminfotech.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hem Infotech</a></strong></h5>
                <p>IT Solutions and Services Company</p>

            </div>
            
        </div>
    
    </div>
    
    
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/hi-will-logo.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hi-Will Education</a></strong></h5>
                <p>Engineers classes</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/neotech.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/prarambh.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Prarambh Security</a></strong></h5>
                <p>Security Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/pratrikshainterior.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Pratiksha Interior</a></strong></h5>
                <p>Modular kitchen, Office Furniture and Roofing</p>

            </div>
            
        </div>
    
    </div>


 </div>
</div>




<div class="clearfix"></div>

<?php include '../includes-3/footer.php' ?>

<!-- end footer -->

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
 
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<?php include '../includes-3/js.php' ?>
</body>
</html>
    