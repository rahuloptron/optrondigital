
<!doctype html>

<html lang="en-gb" class="no-js">
<!--<![endif]-->

<head>
    <title>Leading Website Design Company In Mumbai</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="/images/favicon.png">
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
    <!-- ######### CSS STYLES ######### -->
    <link rel="stylesheet" href="../css/reset1.css" type="text/css" />
	<link rel="stylesheet" href="../css/style1.css" type="text/css" />
    
    <!-- font awesome icons -->
    <link rel="stylesheet" href="../css/font-awesome/css/font-awesome.min.css">
	
    <!-- simple line icons -->
	<link rel="stylesheet" type="text/css" href="../css/simpleline-icons/simple-line-icons.css" media="screen" />
        
    <!-- animations -->
    <link href="../js/animations/css/animations.min.css" rel="stylesheet" type="text/css" media="all" />
    
    <!-- responsive devices styles -->
	<link rel="stylesheet" media="screen" href="../css/responsive-leyouts1.css" type="text/css" />
    
    <!-- shortcodes -->
    <link rel="stylesheet" media="screen" href="../css/shortcodes1.css" type="text/css" /> 
   
    <!-- mega menu -->
    <link href="../js/mainmenu/bootstrap.min.css" rel="stylesheet">
	<link href="../js/mainmenu/demo.css" rel="stylesheet">
	<link href="../js/mainmenu/menu3.css" rel="stylesheet">
	
    <!-- owl carousel -->
    <link href="../js/carouselowl/owl.transitions.css" rel="stylesheet">
    <link href="../js/carouselowl/owl.carousel.css" rel="stylesheet">
    
    <!-- accordion -->
    <link rel="stylesheet" type="text/css" href="../js/accordion/style.css" />
    

    </head>

<body>
    
    <div class="site_wrapper">
      
            <!-- end top navigation links -->
            <div class="clearfix"></div>
             <?php include "../includes/menu-home.php" ?>
                <div class="clearfix"></div>
             <!-- Slider
======================================= -->
<div class="feature_section18" style="background-image: url(../images/slider-web-design.jpg) ">
<div class="container">

<h1 class="caps"><strong>We Are With You Every Step <br>Get Started Easily.</strong></h1>

    <h4>Make your Website Faster, Safer &amp; Better Support.</h4>
    <br><br>
    <a href="#">Get Started Now!</a>
    
</div>
</div><!-- end search section -->

                <div class="clearfix"></div>
                <div class="content_fullwidth less3">
                    <div class="container">
                        <h2>Leading Website Design Company In Mumbai</h2>
                        <h3 class="blue"> Web designing is an art! Your website design shows your business insight. A well known saying is "First impression is the lasting one". In web technologies, your website is the first entity that interacts with the visitor, so your website should speak itself! So, If your company is considering to build a new website or updating an existing one, then should be one of the top priorities in mind.</h3>
                        <div class="clearfix"></div>
                    </div>
                    <div class="feature_section10">
                        <div class="container">
                            <div class="one_fourth_less">
                                <div class="box"> <i class="fa fa-user-md fati3"></i>
                                    <h3>Health Care Website</h3>
                                    <h3></h3> </div>
                            </div>
                            <div class="one_fourth_less">
                                <div class="box"> <i class="fa fa-mortar-board fati3"></i>
                                    <h3>Education Website</h3>
                                    <h3></h3> </div>
                            </div>
                            <div class="one_fourth_less">
                                <div class="box"> <i class="fa fa-bus fati3"></i>
                                    <h3>Travel Website</h3>
                                    <h3></h3> </div>
                            </div>
                            <div class="one_fourth_less last">
                                <div class="box"> <i class="fa fa-home fati3"></i>
                                    <h3>Real Estate Website</h3>
                                    <h3></h3> </div>
                            </div>
                        </div>
                    </div> 
                    <div class="clearfix"></div>
                    <div class="margin_top6"></div>
                    <div class="feature_section1">
                        <div class="container">
                            <h2 class="caps"><strong>Creative Webdesign Features</strong></h2>
                            <div class="clearfix margin_bottom3"></div>
                            <div class="one_third"> <i class="fa fa-pencil-square-o	"></i>
                                <h4>Web Design templates</h4>
                                <p>Ready web design templates to choose from variety of designs for every budget.</p>
                                <div class="clearfix margin_bottom5"></div> <i class="fa fa-search-plus"></i>
                                <h4>Wordpress Development</h4>
                                <p>Open source wordpress installation and customized web design solutions.</p>
                                <div class="clearfix margin_bottom5"></div>
                            </div>
                            <!-- end section -->
                            <div class="one_third"> <i class="fa fa-paint-brush"></i>
                                <h4>Custom Web Design</h4>
                                <p>Build your web brand with our customized web designing.</p>
                                <div class="clearfix margin_bottom5"></div> <i class="fa fa-refresh"></i>
                                <h4>Website Re-design</h4>
                                <p>We re-design, re-develop, and deliver websites that are efficient and effective.</p>
                                <div class="clearfix margin_bottom5"></div>
                            </div>
                            <!-- end section -->
                            <div class="one_third last"> <i class="fa fa-eye"></i>
                                <h4>Dynamic Websites</h4>
                                <p>Build Dynamic websites to manage content with our enterprise.</p>
                                <div class="clearfix margin_bottom5"></div> <i class="fa fa-wrench"></i>
                                <h4>Website Maintenance</h4>
                                <p>Upgrade your websites, content, images with our maintenance services.</p>
                                <div class="clearfix margin_bottom5"></div>
                            </div>
                            <!-- end section -->
                        </div>
                    </div>
                    <div class="clearfix"></div>

<div class="feature_section3">
<div class="container">

    <div class="one_half">
    
        <h2 class="caps">Latest News / Blogs</h2>
        
        <div class="clearfix margin_bottom1"></div>
        
        
        <div id="owl-demo13" class="owl-carousel">
        
            <div class="lstblogs">
            <div>
                <a href="#"><img src="http://placehold.it/560x250" alt="" /></a>
                <a href="#" class="date"><strong>12</strong> Aug</a>
                <h4 class="white light"><a href="#" class="tcont">Galley of type and scrambled it to make typesets specimen book with the release sheets.</a> <div class="hline"></div></h4>
            </div>
            </div><!-- end this slide -->
            
            <div class="lstblogs">
            <div>
                <a href="#"><img src="http://placehold.it/560x250" alt="" /></a>
                <a href="#" class="date"><strong>11</strong> Aug</a>
                <h4 class="white light"><a href="#" class="tcont">Desktop type and scrambled it to make typesets specimen book with the release sheets.</a> <div class="hline"></div></h4>
            </div>
            </div><!-- end this slide -->
            
            <div class="lstblogs">
            <div>
                <a href="#"><img src="http://placehold.it/560x250" alt="" /></a>
                <a href="#" class="date"><strong>10</strong> Aug</a>
                <h4 class="white light"><a href="#" class="tcont">Packages of type and scrambled it to make specimen book with the release sheets.</a> <div class="hline"></div></h4>
            </div>
            </div><!-- end this slide -->
            
        </div>
            
    </div><!-- end latest news / blogs section -->
    
    
    <div class="one_half last">
    
        <h2 class="caps">Have Questions?</h2>
        
        <div class="clearfix margin_bottom1"></div>
        
        <div id="st-accordion-six" class="st-accordion-six">
        
            <ul>
            
                <li>
                    <a href="#">3 Diffrent Hosting Websites<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>She packed her seven versalia, put her into the belt and made on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her she had a last view back on the skyline of her  hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane over the years, sometimes by sometimes on purpose.</p>
                    </div>
                </li>
                
                <li>
                    <a href="#">Fully Responsive Well Structured<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                    </div>
                </li>
                
                <li>
                    <a href="#">Free Support Free Lifetime Updates<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>O my friend - but it is too much for my strength - I sink under the weight of the splendour of these visions!</p>
                        <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>
                    </div>
                </li>
                
                <li>
                    <a href="#">Premium Sliders Portfolios and Forms<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What's happened to me?"</p>
                        <p>He thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>
                    </div>
                </li>
                
            </ul>
    </div>
        
    </div>

</div>
</div><!-- end feature section 3 -->

                  
      <div class="clearfix"></div>

                    
<div class="feature_section5">
<div class="container">

	<div class="one_full">
    	 
   
		<h2 class="caps"><strong>Technologies We Use</strong></h2>
        <div class="clearfix margin_bottom2"></div>
        <ul>
        	<li><img src="../images/media/scripts-logo1.jpg" alt="" /></li>
            <li><img src="../images/media/scripts-logo2.jpg" alt="" /></li>
            <li><img src="../images/media/scripts-logo3.jpg" alt="" /></li>
            <li><img src="../images/media/scripts-logo4.jpg" alt="" /></li>
        	 <li><img src="../images/media/scripts-logo7.jpg" alt="" /></li>
            <li><img src="../images/media/scripts-logo6.jpg" alt="" /></li>
           
          
        </ul>
        
    </div>
    

</div>
</div>
                    
             
                        <div class="client_logos">
                            <div class="container">
                                <h2 class="caps"><strong>Our portfolio</strong></h2>
                                <div class="margin_top1"></div>
                                <div class="clearfix"></div>
                                <div id="owl-demo" class="owl-carousel">
                                    <div class="item"><img src="/images/sliders/newindexviral.png" alt="" /></div>
                                    <div class="item"><img src="/images/sliders/newindexgrecells.png" alt="" /></div>
                                    <div class="item"><img src="/images/sliders/newindexag.png" alt="" /></div>
                                    <div class="item"><img src="/images/sliders/newindexshakti.png" alt="" /></div>
                                    <div class="item"><img src="/images/sliders/newindexenjay.png" alt="" /></div>
                                    <div class="item"><img src="/images/sliders/newindexrohm.png" alt="" /></div>
                                    <div class="item"><img src="/images/sliders/newindexrevital.png" alt="" /></div>
                                    <div class="item"><img src="/images/sliders/newindexaashrayaa.png" alt="" /></div>
                                </div>
                                <!-- end section -->
                            </div>
                        </div>
                </div>
    </div>
    <div class="clearfix"></div>

    <div class="feature_section6">
<div class="container">
    
    <h1 class="caps"><strong>why customers <i class="fa fa-heart sitecolor"></i> us!</strong></h1>
    
    <div class="clearfix margin_bottom1"></div>
    
    <div class="less6">
    <div id="owl-demo20" class="owl-carousel">
    
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Michile Johnson</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Desirae Karla</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Franklin Brice</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
                
    </div>
    </div>

</div>
</div><!-- end featured section 5-->

 <div class="clearfix"></div>

<footer>

<div class="footer">

<div class="clearfix"></div>

<div class="secarea">
<div class="container">
    
    <div class="one_fourth">
        <h4 class="white">Hosting Packages</h4>
        <ul class="foolist">
            <li><a href="#">Web Hosting</a></li>
            <li><a href="#">Reseller Hosting</a></li>
            <li><a href="#">VPS Hosting</a></li>
            <li><a href="#">Dedicated Servers</a></li>
            <li><a href="#">Windows Hosting</a></li>
            <li><a href="#">Cloud Hosting</a></li>
            <li><a href="#">Linux Servers</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Our Products</h4>
        <ul class="foolist">
            <li><a href="#">Website Builder</a></li>
            <li><a href="#">Web Design</a></li>
            <li><a href="#">Logo Design</a></li>
            <li><a href="#">Register Domains</a></li>
            <li><a href="#">Traffic Booster</a></li>
            <li><a href="#">Search Advertising</a></li>
            <li><a href="#">Email Marketing</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Company</h4>
        <ul class="foolist">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Press &amp; Media</a></li>
            <li><a href="#">News / Blogs</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Awards &amp; Reviews</a></li>
            <li><a href="#">Testimonials</a></li>
            <li><a href="#">Affiliate Program</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth last aliright">
        <div class="address">
        
           <h3 class="white">Optron Technologies</h3>
            217, Accord Classic, Station Road, Goregaon East 400063
            <div class="clearfix margin_bottom1"></div>
            <strong>Phone:</strong> <b>9833189090</b>
            <br />
            <strong>Mail:</strong> <a href="mailto:info@arkahost.com">info@optron.in</a>
            <br />
           <br />
           
            
        </div>
        
    </div><!-- end section -->
    
    
    <div class="clearfix margin_bottom3"></div>
    
    
    <div class="one_fourth">
        <h4 class="white">Follow Us</h4>
        <ul class="foosocial">
            <li class="faceboox"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
            <li class="linkdin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Resources</h4>
        <ul class="foolist">
            <li><a href="#">How to Create a Website</a></li>
            <li><a href="#">How to Transfer a Website</a></li>
            <li><a href="#">Start a Web Hosting Business</a></li>
            <li><a href="#">How to Start a Blog</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Support</h4>
        <ul class="foolist">
            <li><a href="#">Product Support</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Knowledge Base</a></li>
            <li><a href="#">Tutorials</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth last aliright">
        <p class="clearfix margin_bottom1"></p>
        <img src="http://placehold.it/120x50" alt="" /> &nbsp; <img src="http://placehold.it/120x50" alt="" />
    </div><!-- end section -->
    
</div>
</div>
    

<div class="clearfix"></div>


<div class="copyrights">
<div class="container">

    <div class="one_half">Copyright © 2018 Optron Digital. All rights reserved.</div>
    <div class="one_half last aliright"><a href="/about/terms.html">Terms of Service</a>|<a href="/about/privacy.html">Privacy Policy</a>|<a href="/about/sitemap.html">Site Map</a></div>

</div>
</div><!-- end copyrights -->


</div>

</footer>
        <!-- end footer -->
        <div class="clearfix"></div> 

        <a href="#" class="scrollup">Scroll</a>
        
        </div>
    
<script type="text/javascript" src="../js/universal/jquery.js"></script>
<script src="../js/animations/js/animations.min.js" type="text/javascript"></script>
<script src="../js/mainmenu/bootstrap.min.js"></script> 
<script src="../js/mainmenu/customeUI.js"></script>
<script src="../js/masterslider/jquery.easing.min.js"></script>

<script src="../js/scrolltotop/totop.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/mainmenu/sticky.js"></script>

<script src="../js/carouselowl/owl.carousel.js"></script>

<script type="text/javascript" src="../js/accordion/jquery.accordion.js"></script>
<script type="text/javascript" src="../js/accordion/custom.js"></script>

<script type="text/javascript" src="../js/universal/custom.js"></script>

</body>
</html>