<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]--><head>
	<title>FAQS</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

    
    <!-- Favicon --> 
	<link rel="shortcut icon" href="../images-3/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
   
    <?php include '../includes-3/css2.php' ?>
    
</head>

<body>

<div class="site_wrapper">

<div class="clearfix"></div>


<header class="header">
 
	<div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="index.html" id="logo"></a></div>
		
	<!-- Navigation Menu -->
    <div class="menu_main">
    
      <div class="navbar yamm navbar-default">
        
          <div class="navbar-header">
            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  >
              <button type="button" > <i class="fa fa-bars"></i></button>
            </div>
          </div>
          
          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
          
             <?php include '../includes-3/menu.php' ?>
            
          </div>
        
      </div>
    </div>
	<!-- end Navigation Menu -->
    
    
	</div>
    
</header>

<div class="clearfix"></div>
<div class="page_title1 sty8">
<div class="container">

    <h1>FAQS</h1>
 
</div>      
</div>


<div class="clearfix"></div>


<div class="feature_section9">
<div class="container">

	<h1 class="caps"><strong>FAQ - Your questions? We got answers!</strong></h1>
    
    <div class="clearfix margin_bottom3"></div>
	
    <div class="one_half">
    
        <div class="box">
        	<h4 class="caps"><strong>WHAT'S A DOMAIN NAME?</strong></h4>
        	<p class="bigtfont">Many desktop publishing packages and web page editors now use many web sites still in their versions have over the years.</p>
        </div>
    	
        <div class="box">
        	<h4 class="caps"><strong>What is a domain extension?</strong></h4>
        	<p class="bigtfont">Desktop publishing packages and web page editors now use Lorem Ipsum as their default model text and a search</p>
        </div>
        
        <div class="box">
        	<h4 class="caps"><strong>Can I use my domain for email?</strong></h4>
        	<p class="bigtfont">Lorem Ipsum as their default model text and a search for will uncover many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
        <div class="box">
        	<h4 class="caps"><strong>What can I do with a domain name?</strong></h4>
        	<p class="bigtfont">Web page editors now use Lorem Ipsum as their default model text and a search for will uncover many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
	</div><!-- end section -->
    
    <div class="one_half last">
    
        <div class="box">
        	<h4 class="caps"><strong>How long does it take to register my domain?</strong></h4>
        	<p class="bigtfont">Lorem Ipsum as their default model many web sites still in their infancy versions have evolved over the years.</p>
        </div>
    	
        <div class="box">
        	<h4 class="caps"><strong>What are your Terms &amp; Conditions?</strong></h4>
        	<p class="bigtfont">Desktop publishing packages and web page editors now use Lorem Ipsum as their default model text and a search</p>
        </div>
        
        <div class="box">
        	<h4 class="caps"><strong>WHAT PAYMENT OPTIONS DO YOU OFFER?</strong></h4>
        	<p class="bigtfont">Lorem Ipsum as their default model text and a search for will uncover many web sites still in their infancy versions have evolved over the years.</p>
        </div>
        
        <div class="box">
        	<h4 class="caps"><strong>Can I sell or transfer the domain?</strong></h4>
        	<p class="bigtfont">Web page editors now use Lorem Ipsum as web sites still in their infancy their default model text and a search for will uncover many versions have evolved over the years.</p>
        </div>
        
	</div><!-- end section -->

</div>
</div><!-- end featured section 9 -->


<div class="clearfix"></div>


 <?php include '../includes-3/footer.php' ?><!-- end footer -->


<div class="clearfix"></div>


<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>

    
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
 <?php include '../includes-3/js2.php' ?>

</body>
</html>
