<?php 

$productName = "Ecommence Woocommerce development";

?>

<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]--><head>
    <title>Ecommence Woocommerce development in mumbai</title>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />

    <!-- Favicon --> 
    <link rel="shortcut icon" href="../../images/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
    
<?php include '../../includes/website-css2.php' ?>

<?php include '../../includes/ga.php' ?>
    
</head>

<body>

    <?php include "../../includes/form.php" ?>

<div class="site_wrapper">

<?php include "../../includes/menu-home.php" ?>

<div class="clearfix"></div>

<div class="page_title1 sty7">

    <div class="container">

     <h1>Woocommerce Ecommerce website</h1>
    <h3 class="white">A beautiful, professional and SEO friendly website with chat, inquiry form, admin access, thank you pages and latest version of wordpress.</h3>

      <br>
     <a href="#new-message-popup" class="but_large4 transp2 open-new-message">Get Started</a>

     </div>
    
</div>

<div class="clearfix margin_bottom4"></div>

<div class="feature_section12-1">
<div class="container">
    
    <div class="one_half">

        <h2>WooCommerce Web Development Services</h2>
    
    <p class="bigtfont dark">WooCommerce is an Open Source, powerful, extendable eCommerce plug-in providing eCommerce solutions to small and medium scale online retailers with powerful yet easy to use features and intuitive interface. WooCommerce helps us deliver our clients with enterprise-level outcomes with high-level security standards, bird’s eye view of stores to check monthly sales and create reports, set taxes and calculation, manage the day-to-day store and many other facilities for vendor and customers. </p>

    <div class="clearfix margin_bottom2"></div>
   <ul class="list2">
          <li><i class="fa fa-long-arrow-right"></i>It’s Free and Familiar</li>
          <li><i class="fa fa-long-arrow-right"></i>It Looks Professional Yet Simple</li>
          <li><i class="fa fa-long-arrow-right"></i>It’s Very Secure</li>
          <li><i class="fa fa-long-arrow-right"></i>It Has A Lot Of Customization Options</li>
          <li><i class="fa fa-long-arrow-right"></i>Huge Flexibility</li>
         
          <li><i class="fa fa-long-arrow-right"></i>And More..</li>
     </ul>
    </div><!-- end section -->
    
    <div class="one_half last">

        <img src="../../images/woocommerce-web-design.jpg" alt="">
    
        
    </div><!-- end section -->
    
</div>
</div><!-- end featured section 12 -->

<div class="clearfix"></div>

<div class="feature_section401">
<div class="container">

    <div class="one_third">
    
        <span aria-hidden="true" class="icon-trophy"></span>
        <h4 class="white caps">4 Diffrent Themes</h4>
        <p>Many desktop publishi packages and web page editors use model web sites.</p>
    
    </div><!-- end section -->
    
    <div class="one_third">
    
        <span aria-hidden="true" class="icon-present"></span>
        <h4 class="white caps">Modern Design</h4>
        <p>Many desktop publishi packages and web page editors use model web sites.</p>
    
    </div><!-- end section -->
    
    <div class="one_third last">
    
        <span aria-hidden="true" class="icon-settings"></span>
        <h4 class="white caps">Easy to Use</h4>
        <p>Many desktop publishi packages and web page editors use model web sites.</p>
    
    </div><!-- end section -->

</div>
</div>

<div class="clearfix"></div>

<div class="feature_section12">
<div class="container">

    <h1>Features of WooCommerce</h1>

     <div class="clearfix margin_bottom3"></div>
    
    <div class="one_fourth">
    
        <i class="fa fa-check"></i>
        <h4 class="light">Modern & Clean Interface</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">WordPress Content Integration</h4>

         <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Pre-Installed Payment Gateways</h4>

         <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
         <h4 class="light">Set Default Currency</h4>    
        
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <i class="fa fa-check"></i>
        <h4 class="light">Geo-Location Support</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Add/Manage Products</h4>

         <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Stock level tracking</h4>

         <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Restrict Sales</h4>
        
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <i class="fa fa-check"></i>
        <h4 class="light">Multiple Shipping Methods</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Control Your Taxes</h4>

         <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Search Engine Optimization</h4>

         <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Discount Coupons & Codes</h4>
        
    </div><!-- end section -->
    
    <div class="one_fourth last">
    
        <i class="fa fa-check"></i>
        <h4 class="light">Product Reviews</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Dashboard Widgets & Reports</h4>

         <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Easily Upgradable</h4>

         <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Cross Browser Compatibility</h4>
        
    </div><!-- end section -->

</div>
</div>

<div class="clearfix"></div>


<div class="parallax_section2">
<div class="container">

    <h4 class="caps">More than 2,000 websites hosted</h4>

    <h1 class="caps"><strong>get your website online today</strong></h1>

    <div class="clearfix margin_bottom2"></div>
    
    <div class="counters12">
    
        <div class="one_fifth_less"> <h5 class="light">Customers</h5> <h1 id="target31"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Projects</h5> <h1 id="target32"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Developers</h5> <h1 id="target33"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Awards</h5> <h1 id="target34"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less last"> <h5 class="light">Years</h5> <h1 id="target35"></h1> </div><!-- end section -->
     
    </div>

</div>
</div><!-- end parallax section 2 -->

<div class="clearfix"></div>

<div class="feature_section17-1">
<div class="container">

      <h1 class="caps"><strong>WooCommerce Development Services</strong></h1>

    <div class="one_third">
        <img src="../../images/responsive.png" alt="">
        <h3 class="caps"><strong>Responsive Website</strong></h3>
        <h5 class="light">Never miss your mobile users as we offer Responsive WooCommerce development to help you serve your mobile users better.</h5>
       
    </div><!-- end section -->
    
    <div class="one_third">
        <img src="../../images/integration.png" alt="">
        <h3 class="caps"><strong>WooCommerce Integration</strong></h3>
        <h5 class="light">CMS Integration Multiple Payment Gateways Integration CRM and WordPress Website Integration<br>
    </h5></div><!-- end section -->
    
    <div class="one_third last">
        <img src="../../images/customization.png" alt="">
        <h3 class="caps"><strong>WooCommerce Customization</strong></h3>
        <h5 class="light">Customization of Existing WordPress Websites Plug-in Customization WooCommerce Shopping Cart Customization</h5>
        
    </div><!-- end section -->
    
    
    <div class="clearfix margin_bottom5"></div>
    
    
    <div class="one_third">
        <img src="../../images/seo-woocom.png" alt="">
        <h3 class="caps"><strong>WooCommerce SEO</strong></h3>
        <h5 class="light">Extensive Audit and Keyword Research Page Title Optimization Implementing XMLs Sitemaps & Google Analytics</h5>
        
    </div><!-- end section -->
    
    <div class="one_third">
         <img src="../../images/site-migration.png" alt="">
        <h3 class="caps"><strong>Site Migration</strong></h3>
        <h5 class="light">With the help of our developers, we also migrate your present website to robust and feature rich WooCommerce platform.</h5>
        
    </div><!-- end section -->
    
    <div class="one_third last">
        <img src="../../images/support-and-maintenance.png" alt="">
        <h3 class="caps"><strong>Support and Maintenance</strong></h3>
        <h5 class="light">When you choose us, rest assured that you will get the best support and maintenance from the experts.</h5>
       
    </div><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>


<div class="feature_section6">
<div class="container">
    
    <h1 class="caps"><strong>why customers <i class="fa fa-heart sitecolor"></i> us!</strong></h1>
    
    <div class="clearfix margin_bottom1"></div>
    
    <div class="less6">
    <div id="owl-demo20" class="owl-carousel">
    
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Michile Johnson</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Desirae Karla</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Franklin Brice</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
                
    </div>
    </div>

</div>
</div>

<div class="clearfix"></div>


<div class="content_fullwidth">
<div class="container">
    
     <h1 class="caps"><strong>Some Esteemed Clients</strong>
    </h1>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="../../images/clients/adam-fabriwerk.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                 <h5><strong><a href="#">Adam Fabriwerk</a></strong></h5>
            <p>Manufactures of processing systems for pharma</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
       
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../../images/clients/ajfilms.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Aj Films</a></strong></h5>
            <p>Best Photography in Mumbai</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../../images/clients/agdigitas.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ag Digitas</a></strong></h5>
                <p>IT Solutions Company</p>
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../../images/clients/blacpeper.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Black Peper</a></strong></h5>
                <p>Black Pepper is an Exhibition Stall Design company</p>
            </div>
            
        </div>
    
    </div>
    
    
     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../../images/clients/enjay.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Enjay Solution</a></strong></h5>
                <p>Expert in CRM and Telephony Solutions across verticals</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../../images/clients/genesis.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Genesis Telesecure</a></strong></h5>
                <p>Genesis Telesecure LLP offers complete range of solutions</p>
            </div>
            
        </div>
    
    </div>
                      
    <div class="one_fourth">

    <div class="flips4">
    
        <div class="flips4_front flipscont4">
       <img src="../../images/clients/grecelllogo.png" alt="" >
        </div>
        
        <div class="flips4_back flipscont4">
            <h5><strong><a href="#">Grecells</a></strong></h5>
            <p>Disaster recovery solutions for Windows server</p>
        </div>
        
    </div>

    </div>      
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../../images/clients/heminfotech.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hem Infotech</a></strong></h5>
                <p>IT Solutions and Services Company</p>

            </div>
            
        </div>
    
    </div>
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../../images/clients/hi-will-logo.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hi-Will Education</a></strong></h5>
                <p>Engineers classes</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../../images/clients/neotech.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../../images/clients/prarambh.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Prarambh Security</a></strong></h5>
                <p>Security Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../../images/clients/pratrikshainterior.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Pratiksha Interior</a></strong></h5>
                <p>Modular kitchen, Office Furniture and Roofing</p>

            </div>
            
        </div>
    
    </div>

 </div>
</div>

<div class="clearfix"></div>

<div class="page_title1 sty7">

    <div class="container">

    <h1>Request Quote</h1>
   
     <br><a href="#" class="but_medium1"><strong>&nbsp; Get Started</strong></a>

     </div>
    
</div>

<div class="clearfix"></div>


<?php include '../../includes/website-footer.php' ?>

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
 
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<?php include '../../includes/website-js2.php' ?>

</body>
</html>
