<?php 

$productName = "Business website development";

?>

<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]--><head>
	<title>Business website development in mumbai</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

    <!-- Favicon --> 
    <link rel="shortcut icon" href="../images/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
	
<?php include '../includes/website-css.php' ?>

<?php include '../includes/ga.php' ?>
    
</head>

<body>
    
<?php include "../includes/form.php" ?>

<div class="site_wrapper">

<?php include "../includes/menu-home.php" ?>

<div class="clearfix"></div>

<div class="page_title1 sty7">

    <div class="container">

    <h1>Business website for only Rs.9,500.00</h1>
    <h3>A beautiful, professional and SEO friendly website with chat, inquiry form, admin access, thank you pages and latest version of wordpress.</h3>

      <br>
     <a href="#new-message-popup" class="but_large4 transp2 open-new-message">Get Started</a>

     </div>
    
</div>

<div class="clearfix margin_bottom4"></div>

<div class="feature_section12-1">
<div class="container">
    
    <div class="one_half">

        <h2>WordPress is among the world’s most popular platform for creating dynamic website.</h2>
    
    <p class="bigtfont dark">Wordpress is flexible, scalable, secure and easy to use content management platform for creating website for personal use, business use and also for selling online. </p>

    <div class="clearfix margin_bottom2"></div>
     <p class="bigtfont dark">At Optron, we have the right expertise and experience in developing wordpress websites. Our team of wordpress website developers are well trained and capable of creating wordpress website for all kinds of requirements.</p>
    	
	</div><!-- end section -->
    
    <div class="one_half last">

        <img src="../images/wp-design-onehalf.jpg" alt="">
    
        
	</div><!-- end section -->
    
</div>
</div><!-- end featured section 12 -->

<div class="clearfix"></div>

<div class="feature_section401">
<div class="container">

    <div class="one_third">
    
        <span aria-hidden="true" class="icon-trophy"></span>
        <h4 class="white caps">4 Diffrent Themes</h4>
        <p>Many desktop publishi packages and web page editors use model web sites.</p>
    
    </div><!-- end section -->
    
    <div class="one_third">
    
        <span aria-hidden="true" class="icon-present"></span>
        <h4 class="white caps">Modern Design</h4>
        <p>Many desktop publishi packages and web page editors use model web sites.</p>
    
    </div><!-- end section -->
    
    <div class="one_third last">
    
        <span aria-hidden="true" class="icon-settings"></span>
        <h4 class="white caps">Easy to Use</h4>
        <p>Many desktop publishi packages and web page editors use model web sites.</p>
    
    </div><!-- end section -->

</div>
</div>

<div class="clearfix"></div>

<div class="feature_section17-1">
<div class="container">

      <h1 class="caps"><strong>Our Commitment</strong>

         <div class="clearfix margin_bottom7"></div>

    <div class="one_third">
        <img src="../images/service-search.png" alt="">
        <h3 class="caps"><strong>Proven Methodology</strong></h3>
        <p class="light">Our process driven approach allows us to complete every project on time.</p>
       
    </div><!-- end section -->
    
    <div class="one_third">
        <img src="../images/service-layers.png" alt="">
        <h3 class="caps"><strong>Affordable Pricing</strong></h3>
        <p class="light">We are not cheap, we are reasonable and delivering value for money.</p>
    </div><!-- end section -->
    
    <div class="one_third last">
        <img src="../images/service-devices.png" alt="">
        <h3 class="caps"><strong>Skilled Developers</strong></h3>
        <p class="light">Team of skilled and developers with multiple years of experience.</p>
        
    </div><!-- end section -->
    
    
</div>
</div>

<div class="clearfix"></div>

<div class="feature_section12">
<div class="container">

    <h1>Wordpress website Features </h1>

     <div class="clearfix margin_bottom3"></div>
    
    <div class="one_fourth">
    
        <i class="fa fa-check"></i>
        <h4 class="light">Attractive and user friendly design</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">Clean HTML and CSS code</h4>
        
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <i class="fa fa-check"></i>
        <h4 class="light">Modify your content as per need</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">SEO friendly design </h4>
        
    </div><!-- end section -->
    
    <div class="one_fourth">
    
        <i class="fa fa-check"></i>
        <h4 class="light">Responsive layout</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">User friendly admin panel</h4>
        
    </div><!-- end section -->
    
    <div class="one_fourth last">
    
        <i class="fa fa-check"></i>
        <h4 class="light">Secure and stable platform</h4>
        
        <div class="clearfix margin_bottom4"></div>
        
        <i class="fa fa-check"></i>
        <h4 class="light">100% responsive </h4>
        
    </div><!-- end section -->

</div>
</div>

<div class="clearfix"></div>


<div class="parallax_section2">
<div class="container">

    <h4 class="caps">More than 2,000 websites hosted</h4>

    <h1 class="caps"><strong>get your website online today</strong></h1>

    <div class="clearfix margin_bottom2"></div>
    
    <div class="counters12">
    
        <div class="one_fifth_less"> <h5 class="light">Customers</h5> <h1 id="target31"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Projects</h5> <h1 id="target32"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Developers</h5> <h1 id="target33"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Awards</h5> <h1 id="target34"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less last"> <h5 class="light">Years</h5> <h1 id="target35"></h1> </div><!-- end section -->
     
    </div>

</div>
</div><!-- end parallax section 2 -->

<div class="clearfix"></div>

<div class="feature_section17">
<div class="container">

     <h1 class="caps"><strong>Wordpress Website Services</strong>

         <div class="clearfix margin_bottom7"></div>

    <div class="one_third">
        <i class="fa fa-wordpress"></i>
        <h3 class="caps"><strong>Wordpress Development</strong></h3>
        
    </div><!-- end section -->
    
    <div class="one_third">
        <i class="fa fa-download"></i>
        <h3 class="caps"><strong>Installation and Configuration</strong></h3>
       
    </div><!-- end section -->
    
    <div class="one_third last">
        <i class="fa fa-database"></i>
        <h3 class="caps"><strong>Wordpress Migration</strong></h3>
        
    </div><!-- end section -->
    
    
    <div class="clearfix margin_bottom5"></div>
    
    
    <div class="one_third">
        <i class="fa fa-wrench"></i>
        <h3 class="caps"><strong>Wordpress Maintenance</strong></h3>
       
    </div><!-- end section -->
    
    <div class="one_third">
        <i class="fa fa-connectdevelop"></i>
        <h3 class="caps"><strong>Wordpress Theme Development</strong></h3>
        
    </div><!-- end section -->
    
    <div class="one_third last">
        <i class="fa fa-support"></i>
        <h3 class="caps"><strong>Wordpress<br> Support</strong></h3>
        
    </div><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>

<div class="feature_section1">
<div class="container">

    <h1 class="caps"><strong>Why WordPress for your website development?</strong></h1>

    <div class="clearfix margin_bottom3"></div>
    
    <div class="one_third">
       
        <h4>Social Media Integration</h4>
        <p>Link your Facebook page, Twitter and Instagram accounts with your website to increase your online visibility. Get more likes on your Facebook page.</p>
        
        <div class="clearfix margin_bottom5"></div>
        
       
        <h4>SEO friendly pages</h4>
        <p>Get SEO friendly URL, SEO software included to increase website visibility in Google search results with minimum efforts.</p>
        
        <div class="clearfix margin_bottom5"></div>
        
       
        <h4>Free Domain for 12 Months</h4>
        <p>Get Domain name for 12 months for free with every WordPress website services you buy from Optron. </p>
        
    </div><!-- end section -->
    
    
    <div class="one_third">

        <h4>Free Google Site Console</h4>
        <p>We will setup free Google webmasters tools to improve your SEO ranking in Google with latest algorithm</p>
       
        <div class="clearfix margin_bottom5"></div>
        
        <h4>Free Google Analytics</h4>
        <p>Get details of your website visitors, source and how much time they have spent on your website. This will allow you to optimize your website for SEO</p>
        
        <div class="clearfix margin_bottom5"></div>
        
        <h4>Free Installation and Setup</h4>
        <p>We offer free wordpress installation on our secure and fast server. Don’t worry about setup and installation</p>
        
    </div><!-- end section -->
    
    
    <div class="one_third last">
       
        <h4>Free Hosting and SSL</h4>
        <p>Don’t waste money on expensive hosting, we offer FREE 6 months hosting with latest technology servers and secure data centers located in USA and India</p>
        
        <div class="clearfix margin_bottom5"></div>
        
       
        <h4>5 Business Email Address</h4>
        <p>Get 5 business email addresses free with website to communicate with your customers, vendors and partners. </p>
        
        <div class="clearfix margin_bottom5"></div>
        
       
        <h4>Free SEO Training Session</h4>
        <p>Get Free access pass for 3 hrs SEO training worth Rs.3,500.00 absolutely free</p>
        
    </div><!-- end section -->

</div>
</div>


<div class="clearfix"></div>

<div class="feature_section9">
<div class="container">

    <h1 class="caps"><strong>Why Choose Wordpress Website Design</strong></h1>
    
    <div class="clearfix margin_bottom3"></div>
    
    <div class="one_half">
    
        <div class="box">
            <h4 class="caps"><strong>Save Money</strong></h4>
            <p class="bigtfont">Website designing is expensive, we offer affordable solution with latest features that allows you to start your website and save money.</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>Easy to Manage</strong></h4>
            <p class="bigtfont">Managing wordpress website is very easy. With few clicks you can add, remove or edit pages without having HTML and coding knowledge</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>No Software Required</strong></h4>
            <p class="bigtfont">You dont need any software to make wordpress website. Wordpress is browser based application</p>
        </div>
       
    </div><!-- end section -->
    
    <div class="one_half last">
    
        <div class="box">
            <h4 class="caps"><strong>Save Time</strong></h4>
            <p class="bigtfont">Save time of development, design and coding. Get started with your website in just 2 hrs and your website will be ready in less than 7 days. (Depending on plan)</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>SEO Friendly Platform</strong></h4>
            <p class="bigtfont">Wordpress is SEO friendly CMS. You can optimize Wordpress website with basic SEO add-ons and manage Title, H1, H2, and image alt tags also</p>
        </div>
        
        <div class="box">
            <h4 class="caps"><strong>No Software Required</strong></h4>
            <p class="bigtfont">You dont need any software to make wordpress website. Wordpress is browser based application</p>
        </div>
      
    </div><!-- end section -->

</div>
</div><!-- end featured section 9 -->


<div class="clearfix"></div>


<div class="feature_section6">
<div class="container">
    
    <h1 class="caps"><strong>why customers <i class="fa fa-heart sitecolor"></i> us!</strong></h1>
    
    <div class="clearfix margin_bottom1"></div>
    
    <div class="less6">
    <div id="owl-demo20" class="owl-carousel">
    
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Michile Johnson</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Desirae Karla</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Franklin Brice</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
                
    </div>
    </div>

</div>
</div>

<div class="clearfix"></div>


<div class="content_fullwidth">
<div class="container">
    
     <h1 class="caps"><strong>Some Esteemed Clients</strong>
    </h1>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="../images/clients/adam-fabriwerk.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                 <h5><strong><a href="#">Adam Fabriwerk</a></strong></h5>
            <p>Manufactures of processing systems for pharma</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
       
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/ajfilms.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Aj Films</a></strong></h5>
            <p>Best Photography in Mumbai</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/agdigitas.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ag Digitas</a></strong></h5>
                <p>IT Solutions Company</p>
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/blacpeper.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Black Peper</a></strong></h5>
                <p>Black Pepper is an Exhibition Stall Design company</p>
            </div>
            
        </div>
    
    </div>
    
    
     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/enjay.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Enjay Solution</a></strong></h5>
                <p>Expert in CRM and Telephony Solutions across verticals</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/genesis.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Genesis Telesecure</a></strong></h5>
                <p>Genesis Telesecure LLP offers complete range of solutions</p>
            </div>
            
        </div>
    
    </div>
                      
    <div class="one_fourth">

    <div class="flips4">
    
        <div class="flips4_front flipscont4">
       <img src="../images/clients/grecelllogo.png" alt="" >
        </div>
        
        <div class="flips4_back flipscont4">
            <h5><strong><a href="#">Grecells</a></strong></h5>
            <p>Disaster recovery solutions for Windows server</p>
        </div>
        
    </div>

    </div>      
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/heminfotech.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hem Infotech</a></strong></h5>
                <p>IT Solutions and Services Company</p>

            </div>
            
        </div>
    
    </div>
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/hi-will-logo.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hi-Will Education</a></strong></h5>
                <p>Engineers classes</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/neotech.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/prarambh.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Prarambh Security</a></strong></h5>
                <p>Security Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/pratrikshainterior.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Pratiksha Interior</a></strong></h5>
                <p>Modular kitchen, Office Furniture and Roofing</p>

            </div>
            
        </div>
    
    </div>

 </div>
</div>

<div class="clearfix"></div>

<div class="page_title1 sty7">

    <div class="container">

    <h1>Beautiful and Professional website for only Rs.9,500.00</h1>
    <h3>A beautiful, professional and SEO friendly website with chat, inquiry form, admin access, thank you pages and latest version of wordpress.</h3>

     <br><a href="#" class="but_medium1"><strong>&nbsp; Get Started</strong></a>

     </div>
    
</div>

<div class="clearfix"></div>

<div class="feature_section2">

<div class="title"><h1 class="caps sitecolor"><strong>Why Optron</strong></h1></div>

<div class="twoboxes">
<div class="container">

    <div class="left">
    
       
        <ul>
               
            <li><i class="fa fa-long-arrow-right"></i>10+ years of experience in Industry</li>
            
            <li><i class="fa fa-long-arrow-right"></i>More than 500 Customers</li>
            
            <li><i class="fa fa-long-arrow-right"></i>More than 20 full-time developers</li>

            <li><i class="fa fa-long-arrow-right"></i>In-house project management tools </li>
            
            <li><i class="fa fa-long-arrow-right"></i>Latest technologies and tools</li>
            
           
        </ul>
        
    </div><!-- end left section -->
    
    
    <div class="right">
    
      
        <ul>
            <li><i class="fa fa-long-arrow-right"></i>State of the art offices</li>
            
            <li><i class="fa fa-long-arrow-right"></i>More than 1500 websites made</li>
            
            <li><i class="fa fa-long-arrow-right"></i>97% client retention ratio </li>

            <li><i class="fa fa-long-arrow-right"></i>Support CRM for customer support</li>
            
            <li><i class="fa fa-long-arrow-right"></i>Process driven </li>
            
         
        </ul>
        
    </div><!-- end right section -->

</div>
</div>

</div>

<div class="clearfix"></div>

<div class="host_plans">
<div class="container">

    <h1 class="caps"><strong>Pricing & Plans - Get 30% OFF!</strong>
    </h1>
    
    <div class="clearfix margin_bottom3"></div>
    
    <div class="one_third_less">
    <div class="planbox">
        
        <div class="title"><h4 class="caps"><strong>CORPORATE</strong></h4></div>
        
        <div class="prices">
            <strong>2.99<i>/M</i></strong> <b>Regularly <em>4.99</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
            <li><strong>Unlimited</strong> Web Space</li>
            <li><strong>FREE</strong> Site Building Tools</li>
            <li><strong>FREE</strong> Domain Registar</li>
            <li><strong>24/7/365</strong> Support </li>
            <li><strong>FREE</strong> Marketing &amp; SEO Tools</li>
            <li><strong>99.9%</strong> Service Uptime</li>
            <li class="last"><strong>30 Day</strong> Money Back Guarantee</li>
        </ul>
        
    </div>
    </div><!-- end plan -->
    
    
    
    <div class="one_third_less">
    <div class="planbox">
        
        <div class="title"><h4 class="caps"><strong>WEB PORTALS
</strong></h4></div>
        
        <div class="prices">
            <strong>45.50<i>/M</i></strong> <b>Regularly <em>90.50</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
            <li><strong>Unlimited</strong> Web Space</li>
            <li><strong>FREE</strong> Site Building Tools</li>
            <li><strong>FREE</strong> Domain Registar</li>
            <li><strong>24/7/365</strong> Support </li>
            <li><strong>FREE</strong> Marketing &amp; SEO Tools</li>
            <li><strong>99.9%</strong> Service Uptime</li>
            <li class="last"><strong>30 Day</strong> Money Back Guarantee</li>
        </ul>
        
    </div>
    </div><!-- end plan -->
    
    <div class="one_third_less last">
    <div class="planbox">
        
        <div class="title"><h4 class="caps"><strong>MOBILE APPS
</strong></h4></div>
        
        <div class="prices">
            <strong>100<i>/M</i></strong> <b>Regularly <em>200</em></b>
            <a href="#">Sign Up</a>
        </div>

        <ul>
            <li><strong>Unlimited</strong> Web Space</li>
            <li><strong>FREE</strong> Site Building Tools</li>
            <li><strong>FREE</strong> Domain Registar</li>
            <li><strong>24/7/365</strong> Support </li>
            <li><strong>FREE</strong> Marketing &amp; SEO Tools</li>
            <li><strong>99.9%</strong> Service Uptime</li>
            <li class="last"><strong>30 Day</strong> Money Back Guarantee</li>
        </ul>
        
    </div>
    </div><!-- end plan -->
    
</div>
</div><!-- end hosting plans -->

<div class="clearfix"></div>




<div class="container feature_section107"><br><h1 class="caps light">NEED HELP? <a href="#">Request Quote</a></h1></div>

<div class="clearfix"></div>


<?php include '../includes/website-footer.php' ?>

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
 
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<?php include '../includes/website-js.php' ?>

</body>
</html>
