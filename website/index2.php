<!doctype html>

<html lang=en-gb class=no-js>

<head>
<title>#1 Website Development Company in Mumbai</title>
<meta charset=utf-8>
<meta http-equiv=X-UA-Compatible content="IE=edge" />
<meta name=keywords content />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name=description content="Optron is genuine website design company in Mumbai " />
<?php include "../includes/common-css.php" ?>

</head>
<body>

<div class="site_wrapper">
<?php include "../includes/menu-home.php" ?>
<div class="clearfix"></div>


<div class="feature_section479">
<div class="container">

    <h1 class="less6">LEADING WEBSITE DEVELOPMENT COMPANY IN MUMBAI


<br> <em>Optron Web Design company that will guide you to your online success<br>
Because the way your site looks is our business</em> </h1>


    
    <a href="/inquiry.html">Request Proposal</a> 
    
</div>
</div>

   

<div class="feature_section455">
<div class="container">
 
 
<div class="onecol_sixty">



  <p>Web Designing is one of our core service. Whether you are looking for a website for your company or digital marketing services to grow your business, we can provide you complete solutions from website designing to business promotion, lead generation services under one roof.</p>
   
   
   

  <p class="big_text1">Whether you are looking for a website for your company or digital marketing services to grow your business; we can provide you complete solutions from website development to business promotion, lead generation services under one roof. We can help you grow your business online by building websites. We can customise, upgrade and promote your Business to achieve your Business Goals.</p>
   
  
  
</div>



 <div class="onecol_thirtyfive last">
 

 <img src="../images/web-mobile2.jpg"  class="rimg3" alt=""/>
 
  </div>

</div>
</div>
  

<div class="clearfix"></div>
  <div class="feature_section191">
                    <div class="container">
                        <h2 class="caps"><strong>Industry Specific Web Design Solutions</strong></h2>
                        <div class="clearfix margin_bottom3"></div>
                        <div class="one_third"> <i class="fa fa-user-md "></i>
                            <h4>HEALTHCARE</h4>
                            <p>Website for doctors, clinics, hospitals and pharmaceutical companies</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-shopping-cart"></i>
                            <h4>E-COMMERCE</h4>
                            <p>Start selling online using our e-commerce website development services</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-tachometer"></i>
                            <h4>MANUFACTURING</h4>
                            <p>Corporate website and B2B marketing strategy for manufacturing companies</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third"> <i class="fa fa-home"></i>
                            <h4>REAL ESTATE</h4>
                            <p>Website development & Digital marketing for real estate industry</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-bus"></i>
                            <h4>TOURS AND TRAVELS</h4>
                            <p>Static or dynamic website for travel agency and tours & travel business</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-car"></i>
                            <h4>AUTOMOBILE</h4>
                            <p>Portal for cars and bikes, website for selling used cars, bikes etc</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                        <div class="one_third last"> <i class="fa fa-graduation-cap"></i>
                            <h4>EDUCATION</h4>
                            <p>Website for Schools, Colleges, Training institutes, coaching classes etc. </p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-desktop"></i>
                            <h4>SOFTWARE & IT</h4>
                            <p>Mobile friendly website, marketing solution for IT Industry</p>
                            <div class="clearfix margin_bottom5"></div> <i class="fa fa-gift"></i>
                            <h4>FASHION AND GARMENT</h4>
                            <p>Website development & promotion for fashion and garment industry</p>
                            <div class="clearfix margin_bottom5"></div>
                        </div>
                        <!-- end section -->
                    </div>
                </div>
    
 <!--  <div class="feature_section3">
 <div class="container">
 
  <h3>Web Design Services for Industrial Verticals</h3>
 
 
 <div class="one_third_less">
   <div class="box"> <i class="fa fa-laptop"></i><h5 class="caps">Multipurpose Templates</h5></div>
 </div>
           
 <div class="one_third_less">
   <div class="box"> <i class="fa fa-tablet"></i><h5 class="caps">Mobile Friendly Designs</h5></div>
 </div>
           
 <div class="one_third_less last">
   <div class="box"> <i class="fa fa-paint-brush"></i><h5 class="caps">Corporate and Creative</h5></div>
 </div>
 
 <div class="clearfix margin_bottom2"></div>
 
 <div class="one_third_less">
   <div class="box"> <i class="fa fa-bed"></i><h5 class="caps">Restaurant and Hotels</h5></div>
 </div>
 
 <div class="one_third_less">
   <div class="box"> <i class="fa fa-medkit"></i><h5 class="caps">Hospitals</h5></div>
 </div>
 
 <div class="one_third_less last">
   <div class="box"> <i class="fa fa-plane"></i><h5 class="caps">Travel and Hosting</h5></div>
 </div>
 
 </div>
 </div> -->

<div class="feature_section338">
<div class="container">

 <div class="one_full stcode_title7">
   <h2>Our Projects<br><span class="line"></span> </h2> 
        
       
    
    </div>

  <div class="one_third">

<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work1.png" alt="">
                </div>
                <h6 class="case-item__title">Revital Trichology </h6>
              </div>
               
</div>


<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/enjay-1.png" alt="">
                </div>
                <h6 class="case-item__title">Enjay World</h6>
              </div>
           
</div>


<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work2.png" alt="">
                </div>
                <h6 class="case-item__title">Rohm Computers</h6>
              </div>
            
</div>



<div class="clearfix margin_bottom4"></div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work3.png" alt="">
                </div>
                <h6 class="case-item__title">Grecells</h6>
              </div>
               
</div>


<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/bla.png" alt="">
                </div>
                <h6 class="case-item__title">Black Pepper Exhibitions</h6>
              </div>
             
</div>


<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work5.png" alt="">
                </div>
                <h6 class="case-item__title">Genesis Telecom</h6>
              </div>
            
</div>



<div class="clearfix margin_bottom4"></div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work6.png" alt="">
                </div>
                <h6 class="case-item__title">AG Digitas</h6>
              </div>
              
</div>


<div class="one_third">
 


<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/work10.png" alt="">
                  </div>
                  <h6 class="case-item__title">Sastadeals </h6>
                </div>
             
</div>


<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/vir.png" alt="">
                </div>
                <h6 class="case-item__title">Dr Viral Gada</h6>
              </div>
             
</div>
 

<div class="clearfix margin_bottom4"></div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="../images/aj.png" alt="">
                  </div>
                  <h6 class="case-item__title">AJ Films</h6>
                </div>
               
</div>


<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/tech.png" alt="">
                  </div>
                  <h6 class="case-item__title">Techsofya</h6>
                </div>
             
</div>


<div class="one_third last">

<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work7.png" alt="">
                </div>
                <h6 class="case-item__title">Shakti Enterprises</h6>
              </div>
           
</div>

<div class="clearfix margin_bottom4"></div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="../images/par1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Prarambh Securites</h6>
                </div>
                
</div>


<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/wc.png" alt="">
                  </div>
                  <h6 class="case-item__title">Welcome Cineplex</h6>
                </div>
              
</div>


<div class="one_third last">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/work8.png" alt="">
                  </div>
                  <h6 class="case-item__title">920pm</h6>
                </div>
             
</div>

<div class="clearfix margin_bottom4"></div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="../images/rev.png" alt="">
                  </div>
                  <h6 class="case-item__title">Revelation events</h6>
                </div>
                
</div>


<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/lap.png" alt="">
                  </div>
                  <h6 class="case-item__title">Latest Movies</h6>
                </div>
              
</div>


<div class="one_third last">
<a href="website-designing/inquiry">
<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/a.png" alt="">
                  </div>
                  <h6 class="case-item__title">Make your Website here</h6>
                </div>
              </a>
             
</div>








</div>
</div>



<div class="feature_section8">

<div class="container">

    <div class="left">
      <h1 class="caps"><strong>Why Choose OptronMarketing</strong> <br/>For Website Development</h1>
  </div>
    
    <div class="right">
        <h4 class="caps white">Being honest to our customers</h4>
        
        <ul>
            <li><i class="fa  fa-check"></i>10 + YEARS OF EXPERIENCE</li>
            <li><i class="fa  fa-check"></i>ETHICAL BUSINESS PRACTICES</li>
            <li><i class="fa  fa-check"></i>RESULT ORIENTED</li>
            <li><i class="fa  fa-check"></i>QUALITY FIRST</li>
            <li><i class="fa  fa-check"></i>OUR TALENTED TEAM</li>
            <li><i class="fa  fa-check"></i>OUR CORE VALUES</li>
      </ul>
    </div>
    
</div>
    </div>
<div class="feature_section629">
    <div class="container">
      <h3 class="white caps">To find out what type of website would suit your business | Feel free to </h3>
      <a href="inquiry" class="button one">Contact Us</a> </div>
  </div>
  <div class="clearfix"></div>



  <div class="feature_section62">
<div class="container">
  
  <h2>our pricing</h2>
    
    <div class="one_third">
    <div class="title">
      <h4>Silver </h4></div>
    <ul>
      <li><i class="fa  fa-check"></i>Up to 15 Pages</li>
       <li><i class="fa  fa-check"></i>Home Page</li>
      <li><i class="fa  fa-check"></i>10 Main Pages</li>
      <li><i class="fa  fa-check"></i>7 Menu Items</li>
      <li><i class="fa  fa-check"></i>1 Contact Page</li>
      <li><i class="fa  fa-check"></i>Mobile Friendly Design</li>
      <li><i class="fa  fa-close"></i>Dynamic Content</li>
      <li><i class="fa  fa-check"></i>Images</li>
      <li><i class="fa  fa-close"></i>Content</li>
      <li><i class="fa  fa-check"></i>SEO Ready</li>
      <li><i class="fa  fa-check"></i>Revision</li>
       <li><i class="fa  fa-close"></i>Additional Pages</li>
      <li><i class="fa  fa-close"></i>E-Commerce</li>
    
      <li><i class="fa  fa-check"></i>Inquiry Pages & Credits</li>
      <li><i class="fa  fa-check"></i>Product Search</li>
        <li><i class="fa  fa-close"></i>Custom Design</li>
      
      <li><i class="fa  fa-close"></i>Custom Home Design</li>



    </ul>
    <div class="clearfix margin_bottom2"></div><a href="inquiry" class="button_2">Request Proposal</a>
    </div>
    
    <div class="one_third">
    <div class="title"><h4>Gold</h4></div>
    <ul>
       <li><i class="fa  fa-check"></i>Up to 25 Pages</li>
       <li><i class="fa  fa-check"></i>Home Page</li>
      <li><i class="fa  fa-check"></i>15 Main Page</li>
      <li><i class="fa  fa-check"></i>10 Menu Items</li>
      <li><i class="fa   fa-check"></i>More than 1 contact Page</li>
      <li><i class="fa   fa-check"></i>Mobile Friendly Design</li>
      <li><i class="fa  fa-check"></i>Dynamic Content</li>
      <li><i class="fa  fa-check"></i>Images</li>
      <li><i class="fa   fa-check"></i>Content</li>
      <li><i class="fa   fa-check"></i>SEO Ready</li>
      <li><i class="fa  fa-check"></i>Revision</li>
      <li><i class="fa   fa-close"></i>Additional Pages</li>
       <li><i class="fa  fa-close"></i>E-commerce</li>
      <li><i class="fa  fa-check"></i>Inquiry Pages & Credits</li>
      <li><i class="fa  fa-check"></i>Product Search</li>
      
      <li><i class="fa   fa-close"></i>Custom Design</li>
      <li><i class="fa   fa-close"></i>Custom Home Design</li>

    </ul>
    <div class="clearfix margin_bottom2"></div><a href="inquiry" class="button_2">Request Proposal</a>
    </div>
    
    <div class="one_third last">
    <div class="title"><h4>Platinum</h4></div>
    <ul>
       <li><i class="fa  fa-check"></i>above 25 Pages</li>
       <li><i class="fa  fa-check"></i>Home Page</li>
      <li><i class="fa  fa-check"></i>Depends</li>
      <li><i class="fa  fa-check"></i>12 Menu Items</li>
      <li><i class="fa  fa-check"></i>More than 1 Contact Page</li>
      <li><i class="fa  fa-check"></i>Mobile Friendly Design</li>
      <li><i class="fa  fa-check"></i>Dynamic Content</li>
      <li><i class="fa  fa-check"></i>Images</li>
      <li><i class="fa  fa-check"></i>Content</li>
      <li><i class="fa  fa-check"></i>SEO Ready</li>
      <li><i class="fa  fa-check"></i>Revision</li>
       <li><i class="fa  fa-check"></i>Additional Pages</li>
      <li><i class="fa  fa-check"></i>E-Commerce</li>
    
      <li><i class="fa  fa-check"></i>Inquiry Pages & Credits</li>
      <li><i class="fa  fa-check"></i>Product Search</li>
        <li><i class="fa  fa-check"></i>Custom Design</li>
      
      <li><i class="fa  fa-check"></i>Custom Home Design</li>
    </ul>
    <div class="clearfix margin_bottom2"></div><a href="inquiry" class="button_2">Request Proposal</a>
    </div>

</div>
</div>

<!-- <div class="feature_section665">
<div class="container">

<h1>Choose your Website</h1>


    <div class="clearfix margin_bottom6"></div>

    <div class="one_fourth_less">
       <div class="box">
        <i class="fa fa-tablet"></i>
            <h2>Static Website</h2>
        <div class="bgline"></div><p> The main purpose of our static website design is it to provide you with an online presence. </p>
        <a class="btn linebtn one" href="static-website">Read more </a>
      </div>
    </div>
      
      

      
      <div class="one_fourth_less">
       <div class="box">
         <i class="fa fa-paper-plane-o"></i>
          <h2>Dynamic Websites</h2>
        <div class="bgline"></div><p>Database driven websites and portals for medium to large companies and startup</p>
        <a class="btn linebtn one" href="dynamic-website">Read more</a>
      </div>
    </div>
      
      
      
     <div class="one_fourth_less">
       <div class="box">
         <i class="fa fa-shopping-cart"></i>
          <h2>E-commerce websites</h2>
        <div class="bgline"></div><p>Complete E-commerce solution with payment gateway & mobile friendly design</p>
        <a class="btn linebtn one" href="e-commerce-website">Read more</a>
      </div>
    </div>
      
      
      
     <div class="one_fourth_less last">
       <div class="box">
         <i class="fa fa-star-o"></i>
          <h2>Customized websites</h2>
        <div class="bgline"></div><p>Lorem Ipsum gen erators the Internet tend to repeat pre</p>
        <a class="btn linebtn one" href="#">Read more</a>
      </div>
    </div>
          
</div>
</div> -->



  <div class="clearfix"></div>

 



<div class=divider_line23></div>

<div class=clearfix></div>


 
 

    
 

<div class="feature_section629">
    <div class="container">
      <h3 class="white caps">Grow  Your Business to next Level  </h3>
      <a href="inquiry" class="button one">Let's Start</a> </div>
  </div>

<div class=clearfix></div>
<?php include "../includes/test.php" ?>
<div class=clearfix></div>
<div class=divider_line23></div>


<div class=clearfix></div>
<div class="feature_section37">
<div class="container">

  <div class="one_full">
       
   
    <h2 class="caps"><strong>Technologies We Use</strong></h2>
        <div class="clearfix margin_bottom2"></div>
        <ul>
          <li><img src="../images/scripts-logo1.jpg" alt="" /></li>
            <li><img src="../images/scripts-logo2.jpg" alt="" /></li>
            <li><img src="../images/scripts-logo3.jpg" alt="" /></li>
            <li><img src="../images/scripts-logo4.jpg" alt="" /></li>
           <li><img src="../images/scripts-logo7.jpg" alt="" /></li>
            <li><img src="../images/scripts-logo6.jpg" alt="" /></li>
           
        </ul>
        
    </div>
    
</div>
</div>

<div class=clearfix></div>
<?php include "../includes/footer.php" ?>
<a href=# class=scrollup>Scroll</a>
</div>
<?php include "../includes/common-js.php" ?>

<?php include "../includes/ga.php" ?>

</body>
</html>