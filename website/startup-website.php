<?php 

$productName = "Startup website development";

?>

<!doctype html>
 <html lang="en-gb" class="no-js"> <!--<![endif]--><head>
	<title>Startup website development in mumbai</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />

    <!-- Favicon --> 
    <link rel="shortcut icon" href="../images/favicon.png">
    
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    
	
<?php include '../includes/website-css.php' ?>

<?php include '../includes/ga.php' ?>
    
</head>

<body>

    <?php include "../includes/form.php" ?>

<div class="site_wrapper">

<?php include "../includes/menu-home.php" ?>

<div class="clearfix"></div>

<div class="page_title1 sty7">

    <div class="container">

    <h1>Leading Startup Website Design Company In Mumbai</h1>
    <h3 class="white">Web designing is an art! Your website design shows your business insight. A well known saying is "First impression is the lasting one". In web technologies, your website is the first entity that interacts with the visitor, so your website should speak itself! So, If your company is considering to build a new website or updating an existing one, then should be one of the top priorities in mind.</h3>

      <br>
     <a href="#new-message-popup" class="but_large4 transp2 open-new-message">Get Started</a>

     </div>
    
</div>

<div class="clearfix margin_bottom4"></div>

<div class="feature_section12-1">
<div class="container">
    
    <div class="one_half">

        <h2>Welcome to Optron Digital – Website Designing & Web Development Company</h2>
    
    <p class="bigtfont dark">Web designing is an art! Your website design shows your business insight. A well known saying is "First impression is the lasting one". In web technologies, your website is the first entity that interacts with the visitor, so your website should speak itself! So, If your company is considering to build a new website or updating an existing one, then should be one of the top priorities in mind.</p>

    <div class="clearfix margin_bottom2"></div>
     <p class="bigtfont dark">Web designing is an art! Your website design shows your business insight. A well known saying is "First impression is the lasting one". In web technologies, your website is the first entity that interacts with the visitor.</p>
    	
	</div><!-- end section -->
    
    <div class="one_half last">

        <img src="../images/web-design-development.png" alt="">
    
        
	</div><!-- end section -->
    
</div>
</div><!-- end featured section 12 -->

<div class="clearfix"></div>

<div class="feature_section7">
<div class="container">

    <h1 class="caps"><strong>All Our Services include</strong></h1>
    
    <div class="clearfix margin_bottom2"></div>

    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-globe"></i> Domain<br> Registration</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-database"></i> Web<br> Hosting</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-wordpress"></i> Website<br> Designing</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-rocket"></i> Mobile<br>Website Design</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less last">
        <h5 class="caps"><i class="fa fa-coffee"></i> Website<br> Development</h5>
    </div><!-- end -->
    

    <div class="clearfix margin_bottom3"></div>
    
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-building-o"></i> CMS<br> Web Development</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-lock"></i> WordPress<br> Web Development</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-paper-plane-o"></i> E-Commerce<br> Web Development</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less">
        <h5 class="caps"><i class="fa fa-money"></i> PHP<br> Web Development</h5>
    </div><!-- end -->
    
    <div class="one_fifth_less last">
        <h5 class="caps"><i class="fa fa-headphones"></i> Search<br> Engine Optimization</h5>
    </div><!-- end -->
    
</div>
</div>

<div class="clearfix margin_bottom9"></div>

<div class="container alicent">
    
    <h2 class="caps"><strong>Why Choose Us?</strong></h2>
    
    <div class="clearfix margin_bottom5"></div>
    
    <ul class="pop-wrapper">
    
        <li> <img src="../images/award-1.png" alt=""> <h6>Award<br> Winning Firm</h6></li>
        
        <li> <img src="../images/proffesional-team.png" alt=""> <h6>Certified <br>Experts Team</h6></li>
        
        <li> <img src="../images/projects-done.png" alt=""> <h6>200+<br> Projects Done</h6></li>
        
        <li> <img src="../images/happy-clients.png" alt=""> <h6>150+<br> Happy Clients</h6></li>
        
        <li> <img src="../images/focus-onresult.png" alt=""> <h6>Focused<br> on Results</h6></li>
        
        <li> <img src="../images/support.jpg" alt=""> <h6>1yr<br> Technical Support</h6></li>
        
    </ul>
    
</div>


    <div class="clearfix margin_bottom9"></div>

<div class="parallax_section2">
<div class="container">

    <h4 class="caps">More than 2,000 websites hosted</h4>

    <h1 class="caps"><strong>get your website online today</strong></h1>

    <div class="clearfix margin_bottom2"></div>
    
    <div class="counters12">
    
        <div class="one_fifth_less"> <h5 class="light">Customers</h5> <h1 id="target31"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Projects</h5> <h1 id="target32"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Developers</h5> <h1 id="target33"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Awards</h5> <h1 id="target34"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less last"> <h5 class="light">Years</h5> <h1 id="target35"></h1> </div><!-- end section -->
     
    </div>

</div>
</div><!-- end parallax section 2 -->


<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
     <h1 class="caps"><strong>Some Esteemed Clients</strong>
    </h1>
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="../images/clients/adam-fabriwerk.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                 <h5><strong><a href="#">Adam Fabriwerk</a></strong></h5>
            <p>Manufactures of processing systems for pharma</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
       
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/ajfilms.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Aj Films</a></strong></h5>
            <p>Best Photography in Mumbai</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/agdigitas.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ag Digitas</a></strong></h5>
                <p>IT Solutions Company</p>
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/blacpeper.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Black Peper</a></strong></h5>
                <p>Black Pepper is an Exhibition Stall Design company</p>
            </div>
            
        </div>
    
    </div>
    
    
     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/enjay.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Enjay Solution</a></strong></h5>
                <p>Expert in CRM and Telephony Solutions across verticals</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/genesis.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Genesis Telesecure</a></strong></h5>
                <p>Genesis Telesecure LLP offers complete range of solutions</p>
            </div>
            
        </div>
    
    </div>
                      
    <div class="one_fourth">

    <div class="flips4">
    
        <div class="flips4_front flipscont4">
       <img src="../images/clients/grecelllogo.png" alt="" >
        </div>
        
        <div class="flips4_back flipscont4">
            <h5><strong><a href="#">Grecells</a></strong></h5>
            <p>Disaster recovery solutions for Windows server</p>
        </div>
        
    </div>

    </div>      
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/heminfotech.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hem Infotech</a></strong></h5>
                <p>IT Solutions and Services Company</p>

            </div>
            
        </div>
    
    </div>
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/hi-will-logo.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hi-Will Education</a></strong></h5>
                <p>Engineers classes</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/neotech.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/prarambh.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Prarambh Security</a></strong></h5>
                <p>Security Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/pratrikshainterior.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Pratiksha Interior</a></strong></h5>
                <p>Modular kitchen, Office Furniture and Roofing</p>

            </div>
            
        </div>
    
    </div>

 </div>
</div>


<div class="clearfix"></div>


<div class="feature_section6">
<div class="container">
    
    <h1 class="caps"><strong>why customers <i class="fa fa-heart sitecolor"></i> us!</strong></h1>
    
    <div class="clearfix margin_bottom1"></div>
    
    <div class="less6">
    <div id="owl-demo20" class="owl-carousel">
    
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Michile Johnson</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Desirae Karla</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Franklin Brice</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
                
    </div>
    </div>

</div>
</div>

<div class="clearfix"></div>


<div class="container feature_section107"><br><h1 class="caps light">NEED HELP? <a href="#">Request Quote</a></h1></div>

<div class="clearfix"></div>


<?php include '../includes/website-footer.php' ?>

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
 
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<?php include '../includes/website-js.php' ?>

</body>
</html>
