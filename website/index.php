<?php 

$productName = "Website Design";

?>

<!doctype html>
 <html lang="en-gb" class="no-js"> 
 <head>

	<title>Web Design Company in Mumbai</title>
	
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
    <!-- Favicon --> 
	<link rel="shortcut icon" href="../images/favicon.png">
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
   	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>

<?php include '../includes/website-css.php' ?>


<?php include '../includes/ga.php' ?>
    
</head>

<body>

<?php include "../includes/form.php" ?>

<div class="site_wrapper">

<?php include "../includes/menu-home.php" ?>

<div class="clearfix"></div>

<!-- Slider
======================================= -->


<div class="page_title1 sty7">

    <div class="container">

    <h1>We are revolutionizing the way websites are Made</h1>
    <h3 class="white">A beautiful, professional and SEO friendly website with chat, inquiry form, admin access, thank you pages and latest version of wordpress.</h3>

     <br>
     <a href="#new-message-popup" class="but_large4 transp2 open-new-message">Get Started</a>
  
      </div>
    
</div>
<!-- end of masterslider -->
<div class="clearfix"></div>


<div class="content_fullwidth less3">
<div class="container">
    <h2>Leading Website Design Company In Mumbai</h2>
    <h4> Web designing is an art! Your website design shows your business insight. A well known saying is "First impression is the lasting one". In web technologies, your website is the first entity that interacts with the visitor, so your website should speak itself! So, If your company is considering to build a new website or updating an existing one, then should be one of the top priorities in mind.</h4>
    <div class="clearfix"></div>
</div>
<div class="feature_section10">
    <div class="container">
        <div class="one_fourth_less">
            <div class="box"> <i class="fa fa-user-md fati3" color="red" style="color: #e64a19;"></i>
                <h3>Health Care Website</h3>
                <h3></h3> </div>
        </div>
        <div class="one_fourth_less">
            <div class="box"> <i class="fa fa-mortar-board fati3" style="color: #e64a19;"></i>
                <h3>Education Website</h3>
                <h3></h3> </div>
        </div>
        <div class="one_fourth_less">
            <div class="box"> <i class="fa fa-bus fati3" style="color: #e64a19;"></i>
                <h3>Travel Website</h3>
                <h3></h3> </div>
        </div>
        <div class="one_fourth_less last">
            <div class="box"> <i class="fa fa-home fati3" style="color: #e64a19;"></i>
                <h3>Real Estate Website</h3>
                <h3></h3> </div>
        </div>
    </div>
</div> 
<div class="clearfix"></div>

<div class="feature_section103">
<div class="container">

    <h1 class="caps white"><strong>What We Do</strong></h1>
    
    <div class="clearfix margin_bottom2"></div>

    <div class="box">
        <i class="fa fa-pencil-square-o"></i>
        <h4>Web Design<div class="line"></div></h4>
        <p class="white">THIS IS WHERE WE ENSURE YOUR SITE LOOKS GORGEOUS AND FUNCTIONS PERFECTLY.</p>
    </div><!-- end box -->
    
    <div class="box two">
        <i class="fa fa-connectdevelop"></i>
        <h4>Development<div class="line"></div></h4>
        <p class="white">TURNING DESIGNS INTO CODE THAT WE CAN THEN POWER WITH SOME CODING MAGIC.</p>
    </div><!-- end box -->
    
    <div class="box two">
        <i class="fa fa-paper-plane"></i>
        <h4>Strategy<div class="line"></div></h4>
        <p class="white">FROM IDEATION TO MARKET RESEARCH: DISCOVER, DEFINE, AND PLAN YOUR PRODUCT.</p>
    </div><!-- end box -->
    
    <div class="box two last">
        <i class="fa fa-sticky-note-o"></i>
        <h4>User Experience<div class="line"></div></h4>
        <p class="white">UX IS A CRUCIAL PART OF ALL OUR PROJECTS - ENSURING A NATURAL FLOW THAT EVERYONE CAN USE AND ENJOY.</p>
    </div><!-- end box -->
    
    <div class="clearfix"></div>
    
    <div class="box three">
        <i class="fa fa-hourglass-start"></i>
        <h4>Startup<div class="line"></div></h4>
        <p class="white">WE HELP ENTREPRENEURS BUILD BUSINESSES. USING DATA-DRIVEN DESIGN AND THE LEAN-STARTUP APPROACH, WE’LL HELP BRING YOUR IDEAS TO LIFE.</p>
    </div><!-- end box -->
    
    <div class="box four">
        <i class="fa fa-shopping-cart"></i>
        <h4>E-commerce<div class="line"></div></h4>
        <p class="white">OUR FAVOURITE! WE CAN HELP WITH ALL ASPECT FROM PAYMENTS TO FULFILMENT.</p>
    </div><!-- end box -->
    
    <div class="box four">
        <i class="fa fa-mobile"></i>
        <h4>Mobile App<div class="line"></div></h4>
        <p class="white">SMART, INTUITIVE AND BEAUTIFUL EXPERIENCES BUILT FOR MOBILE AND TABLET DEVICES.</p>
    </div><!-- end box -->
    
    <div class="box four last">
        <i class="fa fa-navicon"></i>
        <h4>Optimization<div class="line"></div></h4>
        <p class="white">OUR ONGOING SUPPORT AND OPTIMIZATION FUSES AN ANALYTICAL APPROACH WITH A RANGE OF CREATIVE IMPROVEMENT OPTIONS</p>
    </div><!-- end box -->
    
   
    
</div>
</div>

</div>
<div class="clearfix"></div>

<div class="feature_section17-1">
<div class="container">

      <h1 class="caps"><strong>Website Features</strong>
    <em>There are so many reasons to choose us for your website solutions, here are a few... </em></h1>

    <div class="one_third">
        <img src="../images/service-search.png" alt="">
        <h3 class="caps"><strong>SEO Friendly</strong></h3>
        <h5 class="light">We Provide SEO Friendly site, that improves visibility on various search engines like Google, Yahoo &amp; more.</h5>
       
    </div><!-- end section -->
    
    <div class="one_third">
        <img src="../images/service-layers.png" alt="">
        <h3 class="caps"><strong>Payment Gateways</strong></h3>
        <h5 class="light">We Integrate payment gateway for Free. Accept payments via Debit Cards, Credit Cards, Net Banking, Paypal &amp; COD.<br>
    </h5></div><!-- end section -->
    
    <div class="one_third last">
        <img src="../images/service-devices.png" alt="">
        <h3 class="caps"><strong>Reponsive Design</strong></h3>
        <h5 class="light">An integrated responsive function that gives a beautiful interface on any devices like mobiles and tablets.</h5>
        
    </div><!-- end section -->
    
    
    <div class="clearfix margin_bottom5"></div>
    
    
    <div class="one_third">
        <img src="../images/service-3d-printer.png" alt="">
        <h3 class="caps"><strong>Shipping Method</strong></h3>
        <h5 class="light">We Integrate few available Logistics provider into the website. You have to signup and provide credentials.</h5>
        
    </div><!-- end section -->
    
    <div class="one_third">
         <img src="../images/sevice-startup.png" alt="">
        <h3 class="caps"><strong>Performance</strong></h3>
        <h5 class="light">We develop the website with speed &amp; performance so your customer gets free speed browsing with latest technology.</h5>
        
    </div><!-- end section -->
    
    <div class="one_third last">
        <img src="../images/shield.png" alt="">
        <h3 class="caps"><strong>Backend Admin Panel</strong></h3>
        <h5 class="light">You can manage store from admin panel like logo, sliders, menu, products &amp; categories without coding knowledge.</h5>
       
    </div><!-- end section -->
    

</div>
</div>

<div class="clearfix"></div>

<div class="parallax_section2">
<div class="container">
    
    <div class="counters12">
    
        <div class="one_fifth_less"> <h5 class="light">Projects</h5> <h1 id="target31"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Clients</h5> <h1 id="target32"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Awards</h5> <h1 id="target33"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less"> <h5 class="light">Domains</h5> <h1 id="target34"></h1> </div><!-- end section -->
        
        <div class="one_fifth_less last"> <h5 class="light">Days</h5> <h1 id="target35"></h1> </div><!-- end section -->
     
    </div>

</div>
</div><!-- end parallax section 2 -->


 <div class="clearfix"></div>

    <div class="feature_section3">
<div class="container">

    <div class="one_half">
    
        <h2 class="caps">Latest News / Blogs</h2>
        
        <div class="clearfix margin_bottom1"></div>
        
        
        <iframe width="560" height="350" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
            
    </div><!-- end latest news / blogs section -->
    
    
    <div class="one_half last">
    
        <h2 class="caps">Types of Website</h2>
        
        <div class="clearfix margin_bottom1"></div>
        
        <div id="st-accordion-six" class="st-accordion-six">
        
            <ul>
            
                <li>
                    <a href="#">3 Diffrent Hosting Websites<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>She packed her seven versalia, put her into the belt and made on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her she had a last view back on the skyline of her  hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane over the years, sometimes by sometimes on purpose.</p>
                    </div>
                </li>
                
                <li>
                    <a href="#">Fully Responsive Well Structured<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
                    </div>
                </li>
                
                <li>
                    <a href="#">Free Support Free Lifetime Updates<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>O my friend - but it is too much for my strength - I sink under the weight of the splendour of these visions!</p>
                        <p>A wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart.</p>
                        <p>I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine.</p>
                    </div>
                </li>
                
                <li>
                    <a href="#">Premium Sliders Portfolios and Forms<span class="st-arrow">Open or Close</span></a>
                    <div class="st-content">
                        <p>The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. "What's happened to me?"</p>
                        <p>He thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls.</p>
                    </div>
                </li>
                
            </ul>
    </div>
        
    </div>

</div>
</div><!-- end feature section 3 -->


<div class="clearfix"></div>

<div class="feature_section5">
<div class="container">


    <center><h1 class="caps"><strong>TECHNOLOGIES WE USE</strong></h1></center>
    <div class="clearfix margin_bottom1"></div>

    <div class="content">
    
        
        
        <ul>
            <li><img src="../images/media/scripts-logo1.jpg" alt=""></li>
            <li><img src="../images/media/scripts-logo2.jpg" alt=""></li>
            <li><img src="../images/media/scripts-logo3.jpg" alt=""></li>
            <li><img src="../images/media/scripts-logo4.jpg" alt=""></li>
             <li><img src="../images/media/scripts-logo7.jpg" alt=""></li>
            <li><img src="../images/media/scripts-logo6.jpg" alt=""></li>
           
          
        </ul>
        
    </div>
    

</div>
</div><!-- end featured section 5-->

      <div class="clearfix"></div>

<div class="feature_section338">
<div class="container">

 <div class="one_full stcode_title7">
   <h2>Recent Projects<br><span class="line"></span> </h2> 
        
    </div>

     <div class="one_third">

<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/heminfotechportfolio.png" alt="">
                </div>
                <h6 class="case-item__title">Hem Infotech</h6>
              </div>
               
</div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/hiwillportfolio.png" alt="">
                </div>
                <h6 class="case-item__title">Hi-Will Education</h6>
              </div>
           
</div>

<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/ajfilms.png" alt="">
                </div>
                <h6 class="case-item__title">Aj Films</h6>
              </div>
            
</div>

<div class="clearfix margin_bottom4"></div>

  <div class="one_third">

<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work1.png" alt="">
                </div>
                <h6 class="case-item__title">Revital Trichology </h6>
              </div>
               
</div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/enjay-1.png" alt="">
                </div>
                <h6 class="case-item__title">Enjay World</h6>
              </div>
           
</div>

<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work2.png" alt="">
                </div>
                <h6 class="case-item__title">Rohm Computers</h6>
              </div>
            
</div>

<div class="clearfix margin_bottom4"></div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work3.png" alt="">
                </div>
                <h6 class="case-item__title">Grecells</h6>
              </div>
               
</div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/bla.png" alt="">
                </div>
                <h6 class="case-item__title">Black Pepper Exhibitions</h6>
              </div>
             
</div>

<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/work5.png" alt="">
                </div>
                <h6 class="case-item__title">Genesis Telecom</h6>
              </div>
            
</div>

<div class="clearfix margin_bottom4"></div>

<div class="one_third">
 
<div class="case-item">
                <div class="case-item__thumb" data-offset="5">
                  <img src="../images/work6.png" alt="">
                </div>
                <h6 class="case-item__title">AG Digitas</h6>
              </div>
              
</div>

<div class="one_third">
 


<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/work10.png" alt="">
                  </div>
                  <h6 class="case-item__title">Sastadeals </h6>
                </div>
             
</div>

<div class="one_third last">
 
<div class="case-item">
                <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                  <img src="../images/vir.png" alt="">
                </div>
                <h6 class="case-item__title">Dr Viral Gada</h6>
              </div>
             
</div>


<div class="clearfix margin_bottom4"></div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb" data-offset="5">
                    <img src="../images/par1.png" alt="">
                  </div>
                  <h6 class="case-item__title">Prarambh Securites</h6>
                </div>
                
</div>

<div class="one_third">

<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/wc.png" alt="">
                  </div>
                  <h6 class="case-item__title">Welcome Cineplex</h6>
                </div>
              
</div>

<div class="one_third last">

<a href="website-designing/inquiry">
<div class="case-item">
                  <div class="case-item__thumb mouseover poster-3d lightbox shadow animation-disabled" data-offset="5">
                    <img src="../images/a.png" alt="">
                  </div>
                    <h6 class="case-item__title">Make your Website here</h6>
                  </div>
                    </a>
             
</div>

</div>
</div>

<div class="clearfix"></div>


<div class="feature_section6">
<div class="container">
    
    <h1 class="caps"><strong>why customers <i class="fa fa-heart sitecolor"></i> us!</strong></h1>
    
    <div class="clearfix margin_bottom1"></div>
    
    <div class="less6">
    <div id="owl-demo20" class="owl-carousel">
    
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Michile Johnson</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Desirae Karla</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /></div>
            <p class="bigtfont dark">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
            <strong>- Franklin Brice</strong> &nbsp;<em>-website</em>
            <p class="clearfix margin_bottom1"></p>
        </div><!-- end slide -->
                
    </div>
    </div>

</div>
</div>
<div class="clearfix"></div>

<div class="content_fullwidth">
<div class="container">
    
    <div class="stcode_title8">
    
        <h2><span class="line"></span><span class="text">Some <strong>Our Esteemed</strong><span> Clients</span></span></h2>
     
    </div><!-- end section title heading -->
    
    <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
                <img src="../images/clients/adam-fabriwerk.png" alt="">
            </div>
            
            <div class="flips4_back flipscont4">
                 <h5><strong><a href="#">Adam Fabriwerk</a></strong></h5>
            <p>Manufactures of processing systems for pharma</p>

            </div>
            
        </div>
    
    </div><!-- end section -->
       
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/ajfilms.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Aj Films</a></strong></h5>
            <p>Best Photography in Mumbai</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/agdigitas.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Ag Digitas</a></strong></h5>
                <p>IT Solutions Company</p>
            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/blacpeper.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Black Peper</a></strong></h5>
                <p>Black Pepper is an Exhibition Stall Design company</p>
            </div>
            
        </div>
    
    </div>
    
    
     <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/enjay.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Enjay Solution</a></strong></h5>
                <p>Expert in CRM and Telephony Solutions across verticals</p>
            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/genesis.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Genesis Telesecure</a></strong></h5>
                <p>Genesis Telesecure LLP offers complete range of solutions</p>
            </div>
            
        </div>
    
    </div>
                      
    <div class="one_fourth">

    <div class="flips4">
    
        <div class="flips4_front flipscont4">
       <img src="../images/clients/grecelllogo.png" alt="" >
        </div>
        
        <div class="flips4_back flipscont4">
            <h5><strong><a href="#">Grecells</a></strong></h5>
            <p>Disaster recovery solutions for Windows server</p>
        </div>
        
    </div>

    </div>      
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/heminfotech.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hem Infotech</a></strong></h5>
                <p>IT Solutions and Services Company</p>

            </div>
            
        </div>
    
    </div>
    
    
    
      <div class="margin_top3"></div><div class="clearfix"></div>
    
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
            <img src="../images/clients/hi-will-logo.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Hi-Will Education</a></strong></h5>
                <p>Engineers classes</p>

            </div>
            
        </div>
    
    </div>

    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/neotech.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
                <h5><strong><a href="#">Neotech</a></strong></h5>
                <p>Professional and Reliable IT Services</p>

            </div>
            
        </div>
    
    </div>
                            
    <div class="one_fourth">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/prarambh.jpg" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Prarambh Security</a></strong></h5>
                <p>Security Solutions</p>

            </div>
            
        </div>
    
    </div>
    
    
    <div class="one_fourth last">
    
        <div class="flips4">
        
            <div class="flips4_front flipscont4">
           <img src="../images/clients/pratrikshainterior.png" alt="" >
            </div>
            
            <div class="flips4_back flipscont4">
              <h5><strong><a href="#">Pratiksha Interior</a></strong></h5>
                <p>Modular kitchen, Office Furniture and Roofing</p>

            </div>
            
        </div>
    
    </div>


 </div>
</div>

<div class="clearfix"></div>

<?php include '../includes/website-footer.php' ?>

<!-- end footer -->

<div class="clearfix"></div>

<a href="#" class="scrollup">Scroll</a><!-- end scroll to top of the page-->

</div>
 
<!-- ######### JS FILES ######### -->
<!-- get jQuery used for the theme -->
<?php include '../includes/website-js.php' ?>

</body>
</html>
    