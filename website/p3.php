
<!doctype html>

<html lang="en-gb" class="">
<!--<![endif]-->

<head>
    <title>Website Development company in Mumbai | Goregaon</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <!-- Favicon -->
    <link rel="shortcut icon" href="/images/favicon.png">
    <!-- this styles only adds some repairs on idevices  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Google fonts - witch you want to use - (rest you can just remove) -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,300italic,400,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,  100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Dancing+Script:400,700' rel='stylesheet' type='text/css'>
    <!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.../js-3"></script>
	<![endif]-->
    <!-- ######### CSS STYLES ######### -->
    <?php include '../includes-3/css.php' ?>
    
   
    </head>

<body>
    
    <div class="site_wrapper">

        <header class="header">
 
    <div class="container">
    
    <!-- Logo -->
    <div class="logo"><a href="index.html" id="logo"></a></div>
        
    <!-- Navigation Menu -->
    <div class="menu_main">
    
      <div class="navbar yamm navbar-default">
        
          <div class="navbar-header">
            <div class="navbar-toggle .navbar-collapse .pull-right " data-toggle="collapse" data-target="#navbar-collapse-1"  >
              <button type="button" > <i class="fa fa-bars"></i></button>
            </div>
          </div>
          
          <div id="navbar-collapse-1" class="navbar-collapse collapse pull-right">
          
            <?php include '../includes-3/menu.php' ?>
            
          </div>
        
      </div>
    </div>
    
    </div>
    
</header>
       
            <!-- end top navigation links -->
<div class="clearfix"></div>


<div class="feature_section18" style="background-image: url(../images/slider-web-design.jpg) ">
<div class="container">

    <h1 class="caps"><strong>We Are With You Every Step <br />Get Started Easily.</strong></h1>
    <h4>Make your Website Faster, Safer &amp; Better Support.</h4>
    <br /><br />
    <a href="#">Get Started Now!</a>
    
</div>
</div>

          
<div class="clearfix"></div>


<div class="feature_section102">

    <h1 class="caps"><strong>Choose Perfect Package for you</strong>
    <em>ARKAHOST providing best and cheap web hosting packges</em></h1>
    
    <div class="clearfix margin_bottom5"></div>
    
    <div class="plan">
    <div class="container">
        
        <div class="onecol_sixty">
            <h1 class="caps"><b>Basic</b> Website</h1>
            <h2 class="light">Many web sites are still in infancy various have versions evolved over the years.</h2>
            <ul>
                <li>- Free Domain name</li>
                <li>- Unlimited Web Space </li>
                <li>- Free SSL Certificate</li>
                <li>- Unlimited FTP Accounts</li>
                <li>- Unlimited Sub Domains</li>
            </ul>
            <h2 class="caps light">Starting at just <strong>1.75</strong></h2>
            <div class="clearfix margin_bottom3"></div>
         
        </div><!-- end section -->
        
        <div class="onecol_forty last"><img src="../images/site-img106.png" alt="" /></div><!-- end img -->
        
    </div>    
    </div><!-- end plan 1 -->
    
    
    <div class="clearfix"></div>
    
    
    <div class="plan two">
    <div class="container">
        
        <div class="onecol_forty"><img src="../images/site-img107.png" alt="" /></div><!-- end img -->
        
        <div class="onecol_sixty last">
            <h1 class="caps"><b>Ecommerce</b> Website</h1>
            <h2 class="light">Many web sites are still in infancy various have versions evolved over the years.</h2>
            <ul>
                <li>- Free Domain name</li>
                <li>- Unlimited Web Space </li>
                <li>- Free SSL Certificate</li>
                <li>- Unlimited FTP Accounts</li>
                <li>- Unlimited Sub Domains</li>
            </ul>
            <h2 class="caps light">Starting at just <strong>27</strong></h2>
            <div class="clearfix margin_bottom3"></div>
         
        </div><!-- end section -->
        
    </div>    
    </div><!-- end plan 2 -->
    
    
    <div class="clearfix"></div>
    
    
    <div class="plan three">
    <div class="container">
        
        <div class="onecol_sixty">
            <h1 class="caps"><b>Application</b> Website</h1>
            <h2 class="light">Many web sites are still in infancy various have versions evolved over the years.</h2>
            <ul>
                <li>- Free Domain name</li>
                <li>- Unlimited Web Space </li>
                <li>- Free SSL Certificate</li>
                <li>- Unlimited FTP Accounts</li>
                <li>- Unlimited Sub Domains</li>
            </ul>
            <h2 class="caps light">Starting at just <strong>4.95</strong></h2>
            <div class="clearfix margin_bottom3"></div>
         
        </div><!-- end section -->
        
        <div class="onecol_forty last"><img src="../images/site-img108.png" alt="" /></div><!-- end img -->
        
    </div>    
    </div><!-- end plan 3 -->
    
</div><!-- end featured section 102 -->


<div class="clearfix"></div>

<div class="feature_section103">
<div class="container">

    <h1 class="caps white"><strong>Why choose our web hosting?</strong>
    <em>More than 200,000 websites hosted</em></h1>
    
    <div class="clearfix margin_bottom2"></div>

    <div class="box">
        <i class="fa fa-rocket"></i>
        <h4>99.9% Uptime Guarantee <div class="line"></div></h4>
        <p class="gray">Desktop publishing packages web page model and a for many web sites.</p>
    </div><!-- end box -->
    
    <div class="box two">
        <i class="fa fa-lightbulb-o"></i>
        <h4>One FREE Domain for Life <div class="line"></div></h4>
        <p class="gray">Desktop publishing packages web page model and a for many web sites.</p>
    </div><!-- end box -->
    
    <div class="box two">
        <i class="fa fa-database"></i>
        <h4>Unlimited Disk Space <div class="line"></div></h4>
        <p class="gray">Desktop publishing packages web page model and a for many web sites.</p>
    </div><!-- end box -->
    
    <div class="box two last">
        <i class="fa fa-sliders"></i>
        <h4>Advanced Security Scan <div class="line"></div></h4>
        <p class="gray">Desktop publishing packages web page model and a for many web sites.</p>
    </div><!-- end box -->
    
    <div class="clearfix"></div>
    
    <div class="box three">
        <i class="fa fa-sitemap"></i>
        <h4>Host Unlimited Domains <div class="line"></div></h4>
        <p class="gray">Desktop publishing packages web page model and a for many web sites.</p>
    </div><!-- end box -->
    
    <div class="box four">
        <i class="fa fa-clock-o"></i>
        <h4>Free 24×7/365 Support <div class="line"></div></h4>
        <p class="gray">Desktop publishing packages web page model and a for many web sites.</p>
    </div><!-- end box -->
    
    <div class="box four">
        <i class="fa fa-tachometer"></i>
        <h4>cPanel Control Panel <div class="line"></div></h4>
        <p class="gray">Desktop publishing packages web page model and a for many web sites.</p>
    </div><!-- end box -->
    
    <div class="box four last">
        <i class="fa fa-usd"></i>
        <h4>30 Day Money-back Guarantee <div class="line"></div></h4>
        <p class="gray">Desktop publishing packages web page model and a for many web sites.</p>
    </div><!-- end box -->
    
    <div class="clearfix margin_bottom7"></div>
    
    <a href="#" class="button five">View All Features</a>
    
</div>
</div><!-- end featured section 103 -->           
    
<div class="clearfix"></div>


<div class="feature_section106">
    
    <div class="left">
        <h2 class="caps"><strong>What our customers Say!</strong></h2>
        <div class="clearfix margin_bottom2"></div>
        
        <div id="owl-demo20" class="owl-carousel">
    
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /> <strong>Michile Johnson</strong> <em>websitename.com</em></div>
            <p class="bigtfont">" Sydney College in virginia looked up one the more obscure latin words consectetur from a rem Ipsum  passage, and going through the cites of the word in classical literature as discovered the undoubtable source lorem Ipsum comes from treatise on the theory sections. "</p>
            <br />
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /> <strong>Desirae Karla</strong> <em>websitename.com</em></div>
            <p class="bigtfont">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
        </div><!-- end slide -->
        
        <div class="item">
            <div class="climg"><img src="http://placehold.it/250x250" alt="" /> <strong>Franklin Brice</strong> <em>websitename.com</em></div>
            <p class="bigtfont">Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using content here now use Lorem Ipsum as their default model text, and a search for lorem ipsum will uncover many web sites still in their infancy various versions have evolved over the years.</p>
            <br />
        </div><!-- end slide -->


        </div>
    </div><!-- end left section -->
    
    
    <div class="right">
        <h2 class="caps white"><strong>Our Trusted Clients</strong></h2>
        <div class="clearfix margin_bottom2"></div>
        
        <ul>
            <li><img src="http://placehold.it/180x80" alt="" /></li>
            <li><img src="http://placehold.it/180x80" alt="" /></li>
            <li><img src="http://placehold.it/180x80" alt="" /></li>
            <li><img src="http://placehold.it/180x80" alt="" /></li>
            <li><img src="http://placehold.it/180x80" alt="" /></li>
            <li><img src="http://placehold.it/180x80" alt="" /></li>
            <li><img src="http://placehold.it/180x80" alt="" /></li>
            <li><img src="http://placehold.it/180x80" alt="" /></li>
            <li><img src="http://placehold.it/180x80" alt="" /></li>
        </ul>
        
    </div>
    
</div><!-- end featured section 106 -->


<div class="clearfix"></div>        
  <div class="clearfix margin_bottom3"></div>

<!-- end featured section 5-->
        
        <div class="feature_section5">
<div class="container">

	<div class="one_full">
    	 
   
		<h2 class="caps"><strong>Technologies We Use</strong></h2>
        <div class="clearfix margin_bottom2"></div>
        <ul>
        	<li><img src="../images/media/scripts-logo1.jpg" alt="" /></li>
            <li><img src="../images/media/scripts-logo2.jpg" alt="" /></li>
            <li><img src="../images/media/scripts-logo3.jpg" alt="" /></li>
            <li><img src="../images/media/scripts-logo4.jpg" alt="" /></li>
        	 <li><img src="../images/media/scripts-logo7.jpg" alt="" /></li>
            <li><img src="../images/media/scripts-logo6.jpg" alt="" /></li>
           
          
        </ul>
        
    </div>
    

</div>
</div>
      <div class="clearfix"></div>
                  
<div class="clearfix"></div>

<footer>

<div class="footer">

<div class="clearfix"></div>

<div class="secarea">
<div class="container">
    
    <div class="one_fourth">
        <h4 class="white">Hosting Packages</h4>
        <ul class="foolist">
            <li><a href="#">Web Hosting</a></li>
            <li><a href="#">Reseller Hosting</a></li>
            <li><a href="#">VPS Hosting</a></li>
            <li><a href="#">Dedicated Servers</a></li>
            <li><a href="#">Windows Hosting</a></li>
            <li><a href="#">Cloud Hosting</a></li>
            <li><a href="#">Linux Servers</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Our Products</h4>
        <ul class="foolist">
            <li><a href="#">Website Builder</a></li>
            <li><a href="#">Web Design</a></li>
            <li><a href="#">Logo Design</a></li>
            <li><a href="#">Register Domains</a></li>
            <li><a href="#">Traffic Booster</a></li>
            <li><a href="#">Search Advertising</a></li>
            <li><a href="#">Email Marketing</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Company</h4>
        <ul class="foolist">
            <li><a href="#">About Us</a></li>
            <li><a href="#">Press &amp; Media</a></li>
            <li><a href="#">News / Blogs</a></li>
            <li><a href="#">Careers</a></li>
            <li><a href="#">Awards &amp; Reviews</a></li>
            <li><a href="#">Testimonials</a></li>
            <li><a href="#">Affiliate Program</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth last aliright">
        <div class="address">
        
           <h3 class="white">Optron Technologies</h3>
            217, Accord Classic, Station Road, Goregaon East 400063
            <div class="clearfix margin_bottom1"></div>
            <strong>Phone:</strong> <b>9833189090</b>
            <br />
            <strong>Mail:</strong> <a href="mailto:info@arkahost.com">info@optron.in</a>
            <br />
           <br />
           
            
        </div>
        
    </div><!-- end section -->
    
    
    <div class="clearfix margin_bottom3"></div>
    
    
    <div class="one_fourth">
        <h4 class="white">Follow Us</h4>
        <ul class="foosocial">
            <li class="faceboox"><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li class="twitter"><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li class="gplus"><a href="#"><i class="fa fa-google-plus"></i></a></li>
            <li class="youtube"><a href="#"><i class="fa fa-youtube-play"></i></a></li>
            <li class="linkdin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Resources</h4>
        <ul class="foolist">
            <li><a href="#">How to Create a Website</a></li>
            <li><a href="#">How to Transfer a Website</a></li>
            <li><a href="#">Start a Web Hosting Business</a></li>
            <li><a href="#">How to Start a Blog</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth">
        <h4 class="white">Support</h4>
        <ul class="foolist">
            <li><a href="#">Product Support</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Knowledge Base</a></li>
            <li><a href="#">Tutorials</a></li>
        </ul>
    </div><!-- end section -->
    
    <div class="one_fourth last aliright">
        <p class="clearfix margin_bottom1"></p>
        <img src="http://placehold.it/120x50" alt="" /> &nbsp; <img src="http://placehold.it/120x50" alt="" />
    </div><!-- end section -->
    
</div>
</div>
    

<div class="clearfix"></div>


<div class="copyrights">
<div class="container">

    <div class="one_half">Copyright © 2018 Optron Digital. All rights reserved.</div>
    <div class="one_half last aliright"><a href="/about/terms.html">Terms of Service</a>|<a href="/about/privacy.html">Privacy Policy</a>|<a href="/about/sitemap.html">Site Map</a></div>

</div>
</div><!-- end copyrights -->


</div>

</footer>

<div class="clearfix"></div> 

<a href="#" class="scrollup">Scroll</a>
                         
    </div>
    <!-- ######### ../JS-3 FILES ######### -->
    <!-- get jQuery used for the theme -->
    <script type="text/javascript" src="../js-3/universal/jquery.js"></script>
    <script src="../js-3/animations/js/animations.min.js" type="text/javascript"></script>
    <script src="../js-3/mainmenu/bootstrap.min.js"></script>
    <script src="../js-3/mainmenu/customeUI.js"></script>
    <script src="../js-3/masterslider/jquery.easing.min.js"></script>
    <script src="../js-3/masterslider/masterslider.min.js"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            var slider = new MasterSlider();
            // adds Arrows navigation control to the slider.
          
            slider.setup('masterslider', {
                width: 1400, // slider standard width
                height: 580, // slider standard height
                space: 0
                , speed: 45
                , layout: 'fullwidth'
                , loop: true
                , preload: 0
                , overPause: true
                , autoplay: true
                , view: "fade"
            });
        })(jQuery);
    </script>
  
    <script src="../js-3/scrolltotop/totop.js" type="text/javascript"></script>
  
    <script src="../js-3/carouselowl/owl.carousel.js"></script>  
    <script type="text/javascript" src="../js-3/universal/custom.js"></script>
     
</body>

</html>