﻿<!doctype html>
<html lang="en-gb" class="no-js">
<!--<![endif]-->
<head>
<title>Optron Academy Address in Goregaon</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="keywords" content="" />
<meta name="description" content="" />

 <?php include "includes/common-css.php" ?>
</head>
<body>
<div class="site_wrapper">

<div class="clearfix"></div>

<div class="feature_section89">
<div class="container">

    <div class="one_half">
    
     <div class="inquiry-form">

<h2>Request Quote</h2>


       <form method="Get"  onsubmit="return validateForm()" action="contact-back.php" name="MyForm" >
       <label><span style="color:red">*</span> Name:</label>
<input type="text" placeholder="Enter Your Name" name="txtname" required><br>

 <label><span style="color:red">*</span> Email:</label>
 <input type="Email" placeholder="Enter Your Email" name="txtemail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required><br>

 <label><span style="color:red">*</span> Mobile Number:</label>
<input type="tel" placeholder="Enter Your Number" name="txtnumber" pattern="^\+{0,2}([\-\. ])?(\(?\d{0,3}\))?([\-\. ])?\(?\d{0,3}\)?([\-\. ])?\d{3}([\-\. ])?\d{4}" required><br>


<label><span style="color:red">*</span> Type of Website</label>
<select name="txtway">
<option value="">Select</option>

  <option value="call">Static Website</option>
  <option value="message">Dynamic Website</option>
  <option value="whatsapp">E-commerce Website</option>
  
</select>


<br>

<br>
<!-- <input type="submit"> -->
<button type='submit' name='submit'>Submit</button>
</form>
        
      </div>  
<div class="margin_bottom2"></div>

            
        </div>


	<div class="one_half last">
    <?php include "../includes/address.php" ?>
    	
    </div>

</div>
</div><!-- end feature_section 7 -->


<div class="clearfix"></div>
<!-- <div class="feature_section8">

<div class="container">

    <div class="left">
        <h1 class="caps"><strong>#1 Digital Marketing Course</strong> <br/>Practical Training on live projects</h1>
       
    </div>
    
    <div class="right">
       
        <h3 class="white">We are dedicated to providing excellent learning experience and bridging skills gap in India.
 </h3><p class="white">Our Unique training methodology is developed by industry experts with multiple years of experience. We focus on <strong>learning by doing methodology</strong>. Here learning is combined with fundamentals, interactive learning sessions, workshops and case studies to  help you understand topics and improve your skills. 
</p>
      
    </div>
    
</div> -->

<div class="clearfix margin_bottom8"></div>



</div>
    

    
<div class="clearfix"></div>
    

    

<div class="clearfix"></div>



<?php include "../includes/footer.php" ?>
</div>
<!-- ######### JS FILES ######### --> 
<script type="text/javascript" src="../js/universal/jquery.js"></script>
<script src="../js/mainmenu/bootstrap.min.js"></script> 
<script src="../js/mainmenu/customeUI.js"></script> 
<script type="text/javascript" src="../js/mainmenu/modernizr.custom.75180.js"></script>
<script type="text/javascript" src="../js/cform/form-validate.js"></script>
<?php include "../includes/ga.php" ?>


</body>
</html>
